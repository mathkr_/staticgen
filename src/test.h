#ifdef RUN_TESTS

#define CONCAT2(a, b)    a##b
#define CONCAT(a, b)     CONCAT2(a, b)

#define TEST_FUNC(name) static bool CONCAT(test_, name)(void)

#define test_assert(expr) do { if (!_test_assert(expr, #expr, __FILE__, __LINE__)) return false; } while(0)
static inline bool
_test_assert(bool result, const char* expr, const char* file, i32 line)
{
    if (!result)
        fprintf(stderr, "    test assertion failed (%s:%d): %s\n", file, line, expr);
    return result;
}

#ifdef _MSC_VER

#include <io.h>

#define DUP(fd)             _dup(fd)
#define DUP2(fd1, fd2)      _dup2(fd1, fd2)
#define DEV_NULL            "NUL"
#define FILENO(fp)          _fileno(fp)

#else

#include <unistd.h>

#define DUP(fd)             dup(fd)
#define DUP2(fd1, fd2)      dup2(fd1, fd2)
#define DEV_NULL            "/dev/null"
#define FILENO(fp)          fileno(fp)

#endif


#define TOGGLE_OFF_OUT_DISABLE 0
#if !TOGGLE_OFF_OUT_DISABLE

#define TEST_DISABLE_OUT_STREAM(dup_name, stream) \
    fflush(stream); \
    i32 dup_name = DUP(FILENO(stream)); \
    freopen(DEV_NULL, "w", stream)

#define TEST_RESTORE_OUT_STREAM(dup_name, stream) \
    fflush(stream); \
    DUP2(dup_name, FILENO(stream));

#else // !TOGGLE_OFF_OUT_DISABLE

#define TEST_DISABLE_OUT_STREAM(dup_name, stream)
#define TEST_RESTORE_OUT_STREAM(dup_name, stream)

#endif // else !TOGGLE_OFF_OUT_DISABLE


//##########################//
// PSEUDORANDOM FOR TESTING
//##########################//

static inline u64 rndu64(u64* state);
static inline u32 rndu32(u64* state);
static inline f32 rndf32(u64* state);
static inline i32 rndi32(u64* state);

static i32  rndi32between(u64* state, i32 lower, i32 upper);
static bool rndbool(u64* state);
static i32  rndsign(u64* state);

static inline u64
xorshift64(u64* state)
{
    assert(state && *state);

    // NOTE(mk): from wikipedia de.wikipedia.org/wiki/Xorshift
    (*state) ^= (*state) << 13;
    (*state) ^= (*state) >> 7;
    (*state) ^= (*state) << 17;

    return *state;
}

static inline u64
xorshift64star(u64* state)
{
    assert(state && *state);

    // NOTE(mk): from https://en.wikipedia.org/wiki/Xorshift#Variations
    (*state) ^= (*state) >> 12;
    (*state) ^= (*state) << 25;
    (*state) ^= (*state) >> 27;

    return (*state) * 2685821657736338717ULL;
}

static inline u64
rndu64(u64* state)
{
    return xorshift64star(state);
}

static inline u32
rndu32(u64* state)
{
    return (u32)xorshift64star(state);
}

static inline f32
rndf32(u64* state)
{
    f32 res = (f32)((f64)rndu64(state) / (f64)UINT64_MAX);
    return res;
}

static inline i32
rndi32(u64* state)
{
    return (i32)xorshift64star(state);
}

static i32
rndi32between(u64* state, i32 lower, i32 upper)
{
    assert(lower < upper);
    assert(((i64)upper - (i64)lower) < (i64)INT32_MAX);

    u32 range = (u32)(upper - lower);
    i32 res = ((i32)(rndu32(state) % range)) + lower;

    return res;
}

static bool
rndbool(u64* state)
{
    u32 n = rndu32(state);
    bool res = n >= (UINT32_MAX / 2);

    return res;
}

static i32
rndsign(u64* state)
{
    i32 res = rndbool(state) ? -1 : 1;
    return res;
}


#else // RUN_TESTS

#define test_assert(expr)

#endif // else RUN_TESTS
