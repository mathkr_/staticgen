#define KILOBYTES(n)    ((n) * (1024ULL))
#define MEGABYTES(n)    ((n) * (1024ULL * 1024ULL))

typedef enum
{
    LogSeverityTrace,
    LogSeverityLog,
    LogSeverityWarn,
    LogSeverityWarnNote,
    LogSeverityError,
    LogSeverityErrorNote,

} LogSeverity;

static const char* log_severity_strings[] = {
    "trace",
    "log",
    "warning",
    "note",
    "error",
    "note",
};

static void io_log(const char* format, ...);
static void io_verbose_log(const char* format, ...);
static void io_wrn(const char* format, ...);
static void io_wrn_note(const char* format, ...);
static void io_err(const char* format, ...);
static void io_err_note(const char* format, ...);

#define IO_FATAL(...) io_fatal(__FILE__, __LINE__, __VA_ARGS__)
#define INVALID_CODE_PATH() io_fatal(__FILE__, __LINE__, "invalid code path, yet we're here")

static void io_fatal(const char* file, i32 line, const char* format, ...);

typedef struct {
    const char* path;
    char*       buf;

} File;

// TAG: Array_File Definition
#define ARRAY_ELEMENT_TYPE  File
#define ARRAY_FUNC_PREFIX   file
#include "array_template.h"

typedef enum {
    IOStatNoExist = 0,
    IOStatOther,
    IOStatFile,
    IOStatDir,
    IOStatError,

} IOStatFileType;

typedef struct {
    size_t size;
    u32 type        : 3;
    u32 exists      : 1;
    u32 stat_error  : 1;

} IOStats;

static IOStats      io_stat(const char* path);
static inline bool  io_mkdir(const char* path);

/**
 * Removes all the files and directories under path recursively.
 *
 * @return          true if the operation succeeded, otherwise false
 * @param path      directory whose subfiles and subdirectories get removed
 */
static bool io_rm_dir_contents(const char* path);

/**
 * Copies all the directory and all of its files and subdirectories at path into dest.
 *
 * @return          true if the operation succeeded, otherwise false
 * @param path      directory whose subfiles and subdirectories get copied
 * @param dest      destination directory
 */
static void io_copy_dir(const char* path, const char* dest, JobQueue* jobqueue);
