static inline void
tokenlist_expect(jmp_buf* eenv, TokenList* list, TokenType_ type)
{
    if (list->t->type == type)
    {
        tokenlist_inc(list);
        return;
    }

    token_msg(list->t,
              LogSeverityError,
              "expected '%s', found '%s'",
              tokentype_get_desc(type),
              tokentype_get_desc(list->t->type));
    longjmp(*eenv, 1);
}

static inline void
tokenlist_expect_value(jmp_buf* eenv, TokenList* list, TokenType_ type, const char* value)
{
    bool type_match = (list->t->type == type);
    bool value_match = (strncmp(list->t->value, value, list->t->len) == 0);
    if (type_match && value_match)
    {
        tokenlist_inc(list);
        return;
    }

    if (!type_match)
    {
        token_msg(list->t,
                  LogSeverityError,
                  "expected '%s', found '%s'",
                  tokentype_get_desc(type),
                  tokentype_get_desc(list->t->type));
    }
    else
    {
        token_msg(list->t,
                  LogSeverityError,
                  "expected '%s', found '%.*s'",
                  value,
                  (i32)MIN(32, list->t->len),
                  list->t->value);
    }
    longjmp(*eenv, 1);
}

static inline void
tokenlist_expect_ident(jmp_buf* eenv, TokenList* list, InternedString intern)
{
    bool type_match = (list->t->type == TokenIdent);
    bool value_match = list->t->intern == intern;
    if (type_match && value_match)
    {
        tokenlist_inc(list);
        return;
    }

    if (type_match)
    {
        token_msg(list->t,
                  LogSeverityError,
                  "expected '%s', found '%s'",
                  tokentype_get_desc(TokenIdent),
                  tokentype_get_desc(list->t->type));
    }
    else
    {
        token_msg(list->t,
                  LogSeverityError,
                  "expected '%s', found '%s'",
                  intern,
                  list->t->intern);
    }
    longjmp(*eenv, 1);
}

static inline bool
ident_is_uppercase(Token* ident)
{
    assert(ident->type == TokenIdent);
    const char* p = ident->intern;
    while (p[0])
    {
        if (p[0] >= 'a' && p[0] <= 'z')
            return false;
        ++p;
    }
    return true;
}

static inline bool
ident_is_builtin_param(Token* ident)
{
    assert(ident->type == TokenIdent);
    for (i32 i = NumLowerCaseKeywords; i < NumKeywords; ++i)
    {
        if (ident->intern == g_keywords[i])
            return true;
    }
    return false;
}

static inline ASTNode*
alloc_node(PageAlloc* allocator, ASTNode node)
{
    ASTNode* res = page_alloc(allocator);
    *res = node;
    return res;
}

static void
ast_trim_text_children(ASTNode* node)
{
    ASTNode* prev = NULL;
    for (ASTNode* child = node->first_child; child != NULL; prev = child, child = child->next)
    {
        if (child->type == ASTNodeText)
        {
            ASTNode* next = child->next;

            // Don't trim text nodes before and after text generating nodes

            if (!(prev && (prev->type == ASTNodeParamAcc || prev->type == ASTNodeInst)))
            {
                trim_token_start(&child->text.text);
            }

            if (!(next && (next->type == ASTNodeParamAcc || next->type == ASTNodeInst)))
            {
                trim_token_end(&child->text.text);
            }

            if (!child->text.text.len)
            {
                // Unlink empty text node
                if (!prev)
                {
                    node->first_child = next;
                }
                else
                {
                    prev->next = next;
                }
            }
        }
    }
}

// Parse function forward declarations:
static ASTNode* parse_inst(jmp_buf* eenv, PageAlloc* allocator, TokenList* list, bool top_level_inst);
static ASTNode* parse_ref(jmp_buf* eenv, PageAlloc* allocator, TokenList* list);
static ASTNode* parse_param_acc(jmp_buf* eenv, PageAlloc* allocator, TokenList* list);
static ASTNode* parse_inst_child(jmp_buf* eenv, PageAlloc* allocator, TokenList* list, bool top_level_inst);

static ASTNode*
parse_text_node(jmp_buf* eenv, PageAlloc* allocator, TokenList* list)
{
    ASTNode node = {0};
    node.type = ASTNodeText;
    node.text.text = *list->t;
    tokenlist_expect(eenv, list, TokenTextNode);

    return alloc_node(allocator, node);
}

static ASTNode*
parse_param_def_child(jmp_buf* eenv, PageAlloc* allocator, TokenList* list)
{
    switch (list->t->type)
    {
        case TokenTagOpen:
        {
            if (tokenlist_match(list, 1, TokenIdent) &&
                !tokenlist_match(list, 2, TokenColon))
            {
                return parse_param_acc(eenv, allocator, list);
            }

            token_msg(list->t, LogSeverityError, "expected param access or text node");
            longjmp(*eenv, 1);
        } break;

        case TokenTextNode:
        {
            return parse_text_node(eenv, allocator, list);
        } break;

        default:
        {
            token_msg(list->t, LogSeverityError, "expected one of: param access or text node");
            longjmp(*eenv, 1);
        } break;
    }
}

static ASTNode*
parse_if(jmp_buf* eenv, PageAlloc* allocator, TokenList* list)
{
    ASTNode node = {0};
    node.type = ASTNodeIf;

    tokenlist_expect(eenv, list, TokenTagOpen);
    tokenlist_expect_ident(eenv, list, g_keywords[KeywordIf]);
    tokenlist_expect(eenv, list, TokenColon);

    if (tokenlist_match_ident(list, 0, g_keywords[KeywordNot]))
    {
        tokenlist_inc(list);
        node.if_clause.not_operator = true;
    }

    if (tokenlist_match(list, 0, TokenIdent) &&
        tokenlist_match(list, 1, TokenPeriod))
    {
        node.if_clause.param_namespace = list->t->intern;
        assert(node.if_clause.param_namespace);
        tokenlist_advance(list, 2);
    }

    node.if_clause.param_ident = *list->t;
    tokenlist_expect(eenv, list, TokenIdent);

    if (tokenlist_match(list, 0, TokenEquals))
    {
        tokenlist_inc(list);
        node.if_clause.match_string = strdup_len(list->t->value + 1, list->t->len - 2);
        tokenlist_expect(eenv, list, TokenString);
    }

    ASTNode* if_block = alloc_node(allocator, (ASTNode){0});
    if_block->type = ASTNodeBlock;
    if_block->block.begin = *list->t;

    tokenlist_expect(eenv, list, TokenTagClose);

    node.first_child = if_block;

    ASTNode** append = &if_block->first_child;
    while (!(tokenlist_match(list, 0, TokenTagOpen) &&
             tokenlist_match(list, 1, TokenForwardSlash) &&
             tokenlist_match_ident(list, 2, g_keywords[KeywordIf])) &&
           !(tokenlist_match(list, 0, TokenTagOpen) &&
             tokenlist_match_ident(list, 1, g_keywords[KeywordElse])))
    {
        ASTNode* next = parse_inst_child(eenv, allocator, list, false);
        *append = next;
        append = &next->next;
    }

    if (tokenlist_match(list, 1, TokenForwardSlash))
    {
        tokenlist_advance(list, 3);
        tokenlist_expect(eenv, list, TokenTagClose);
        ast_trim_text_children(if_block);
    }
    else
    {
        tokenlist_advance(list, 2);

        ASTNode* else_block = alloc_node(allocator, (ASTNode){0});
        else_block->type = ASTNodeBlock;
        else_block->block.begin = *list->t;

        tokenlist_expect(eenv, list, TokenTagClose);

        if_block->next = else_block;

        ASTNode** append = &else_block->first_child;
        while (!(tokenlist_match(list, 0, TokenTagOpen) &&
                 tokenlist_match(list, 1, TokenForwardSlash) &&
                 tokenlist_match_ident(list, 2, g_keywords[KeywordIf])))
        {
            ASTNode* next = parse_inst_child(eenv, allocator, list, false);
            *append = next;
            append = &next->next;
        }

        tokenlist_advance(list, 3);
        tokenlist_expect(eenv, list, TokenTagClose);
        ast_trim_text_children(else_block);
    }

    return alloc_node(allocator, node);
}

static ASTNode*
parse_markdown(jmp_buf* eenv, PageAlloc* allocator, TokenList* list)
{
    ASTNode node = {0};
    node.type = ASTNodeMarkdown;

    tokenlist_expect(eenv, list, TokenTagOpen);
    node.markdown.tag = *list->t;
    tokenlist_expect_ident(eenv, list, g_keywords[KeywordMarkdown]);
    tokenlist_expect(eenv, list, TokenTagClose);

    ASTNode** append = &node.first_child;
    while (!(tokenlist_match(list, 0, TokenTagOpen) &&
             tokenlist_match(list, 1, TokenForwardSlash) &&
             tokenlist_match_ident(list, 2, g_keywords[KeywordMarkdown])))
    {
        ASTNode* next = parse_inst_child(eenv, allocator, list, false);
        *append = next;
        append = &next->next;
    }

    tokenlist_advance(list, 3);
    tokenlist_expect(eenv, list, TokenTagClose);

    return alloc_node(allocator, node);
}

static ASTNode*
parse_param_def(jmp_buf* eenv, PageAlloc* allocator, TokenList* list, ParamDefLocation location)
{
    ASTNode node = {0};
    node.type = ASTNodeParamDef;

    tokenlist_expect(eenv, list, TokenTagOpen);
    tokenlist_expect_ident(eenv, list, g_keywords[KeywordDef]);
    tokenlist_expect(eenv, list, TokenColon);
    node.param_def.ident = *list->t;
    tokenlist_expect(eenv, list, TokenIdent);


    assert(node.param_def.ident.intern);
    for (i32 i = 0; i < NumLowerCaseKeywords; ++i)
    {
        if (node.param_def.ident.intern == g_keywords[i])
        {
            token_msg(&node.param_def.ident,
                      LogSeverityError,
                      "reserved keywords may not be used as parameter names");
            longjmp(*eenv, 1);
        }
    }

    if (ident_is_uppercase(&node.param_def.ident))
    {
        if (node.param_def.ident.intern == g_keywords[KeywordCONTENT])
        {
            token_msg(&node.param_def.ident,
                      LogSeverityError,
                      "the parameter CONTENT may not be defined explicitly");
            longjmp(*eenv, 1);
        }

        if (location != ParamDefLocationTopLevel &&
            (node.param_def.ident.intern == g_keywords[KeywordOUT_DIR] ||
             node.param_def.ident.intern == g_keywords[KeywordSTATIC_DIR]))
        {
            token_msg(&node.param_def.ident,
                      LogSeverityError,
                      "builtin parameters OUT_DIR and STATIC_DIR may only be defined in the global scope");
            longjmp(*eenv, 1);
        }

        if (location != ParamDefLocationTopLevelInst &&
            (node.param_def.ident.intern == g_keywords[KeywordFILE] ||
             node.param_def.ident.intern == g_keywords[KeywordID]))
        {
            token_msg(&node.param_def.ident,
                      LogSeverityError,
                      "builtin parameters FILE and ID may only be defined in top-level instantiations");
            longjmp(*eenv, 1);
        }

        if (!ident_is_builtin_param(&node.param_def.ident))
        {
            token_msg(&node.param_def.ident,
                      LogSeverityError,
                      "all-uppercase names are reserved for built-in parameters");
            longjmp(*eenv, 1);
        }
    }

    if (tokenlist_match(list, 0, TokenForwardSlash))
    {
        tokenlist_inc(list);
        tokenlist_expect(eenv, list, TokenTagClose);
    }
    else
    {
        tokenlist_expect(eenv, list, TokenTagClose);

        ASTNode** append = &node.first_child;
        while (!(tokenlist_match(list, 0, TokenTagOpen) &&
                 tokenlist_match(list, 1, TokenForwardSlash) &&
                 tokenlist_match_ident(list, 2, g_keywords[KeywordDef])))
        {
            ASTNode* next = parse_param_def_child(eenv, allocator, list);

            if (location != ParamDefLocationDefault &&
                next->type == ASTNodeParamAcc &&
                next->param_acc.namespace)
            {
                // No ref.param or ID.param accessors allowed
                token_msg(&next->param_acc.ident,
                          LogSeverityError,
                          "namespace-prefixed param access not allowed in top-level "
                          "(or top-level inst) param definitions");
                longjmp(*eenv, 1);
            }

            *append = next;
            append = &next->next;
        }

        tokenlist_advance(list, 3);
        tokenlist_expect(eenv, list, TokenTagClose);

        ast_trim_text_children(&node);
    }

    return alloc_node(allocator, node);
}

static ASTNode*
parse_param_acc(jmp_buf* eenv, PageAlloc* allocator, TokenList* list)
{
    ASTNode node = {0};
    node.type = ASTNodeParamAcc;

    tokenlist_expect(eenv, list, TokenTagOpen);

    if (tokenlist_match(list, 0, TokenIdent) &&
        tokenlist_match(list, 1, TokenPeriod))
    {
        node.param_acc.namespace = list->t->intern;
        assert(node.param_acc.namespace);
        tokenlist_advance(list, 2);
    }

    node.param_acc.ident = *list->t;
    tokenlist_expect(eenv, list, TokenIdent);

    if (tokenlist_match(list, 0, TokenTagClose))
    {
        // Param acc default value
        tokenlist_inc(list);
        node.param_acc.default_value = true;

        ASTNode** append = &node.first_child;
        while (!(tokenlist_match(list, 0, TokenTagOpen) &&
                 tokenlist_match(list, 1, TokenForwardSlash) &&
                 tokenlist_match_ident(list, 2, node.param_acc.ident.intern)))
        {
            ASTNode* next = parse_param_def_child(eenv, allocator, list);
            *append = next;
            append = &next->next;
        }

        tokenlist_advance(list, 3);
        tokenlist_expect(eenv, list, TokenTagClose);
    }
    else
    {
        tokenlist_expect(eenv, list, TokenForwardSlash);
        tokenlist_expect(eenv, list, TokenTagClose);
    }

    return alloc_node(allocator, node);
}

static ASTNode*
parse_ref(jmp_buf* eenv, PageAlloc* allocator, TokenList* list)
{
    ASTNode node = {0};
    node.type = ASTNodeRef;

    tokenlist_expect(eenv, list, TokenTagOpen);
    tokenlist_expect_ident(eenv, list, g_keywords[KeywordRef]);
    tokenlist_expect(eenv, list, TokenColon);

    node.ref.param_ident = *list->t;
    tokenlist_expect(eenv, list, TokenIdent);

    if (tokenlist_match(list, 0, TokenEquals))
    {
        tokenlist_inc(list);
        node.ref.match_string = strdup_len(list->t->value + 1, list->t->len - 2);
        tokenlist_expect(eenv, list, TokenString);
    }

    tokenlist_expect(eenv, list, TokenTagClose);

    ASTNode** append = &node.first_child;
    while (!(tokenlist_match(list, 0, TokenTagOpen) &&
             tokenlist_match(list, 1, TokenForwardSlash) &&
             tokenlist_match_ident(list, 2, g_keywords[KeywordRef])))
    {
        ASTNode* next = parse_inst_child(eenv, allocator, list, false);
        *append = next;
        append = &next->next;
    }

    tokenlist_advance(list, 3);
    tokenlist_expect(eenv, list, TokenTagClose);

    ast_trim_text_children(&node);

    return alloc_node(allocator, node);
}

static ASTNode*
parse_inst_child(jmp_buf* eenv, PageAlloc* allocator, TokenList* list, bool top_level_inst)
{
    switch (list->t->type)
    {
        case TokenTagOpen:
        {
            if (tokenlist_match_ident(list, 1, g_keywords[KeywordDef]))
            {
                return parse_param_def(eenv, allocator, list, top_level_inst ? ParamDefLocationTopLevelInst : ParamDefLocationDefault);
            }
            else if (tokenlist_match_ident(list, 1, g_keywords[KeywordInst]))
            {
                return parse_inst(eenv, allocator, list, false);
            }
            else if (tokenlist_match_ident(list, 1, g_keywords[KeywordMarkdown]))
            {
                return parse_markdown(eenv, allocator, list);
            }
            else if (tokenlist_match_ident(list, 1, g_keywords[KeywordRef]) &&
                     tokenlist_match(list, 2, TokenColon))
            {
                return parse_ref(eenv, allocator, list);
            }
            else if (tokenlist_match_ident(list, 1, g_keywords[KeywordIf]))
            {
                return parse_if(eenv, allocator, list);
            }
            else if (tokenlist_match(list, 1, TokenIdent))
            {
                return parse_param_acc(eenv, allocator, list);
            }

            token_msg(list->t,
                      LogSeverityError,
                      "expected keyword (param, in, ref, markdown, if) or param identifier");
            longjmp(*eenv, 1);
        } break;

        case TokenTextNode:
        {
            return parse_text_node(eenv, allocator, list);
        } break;

        default:
        {
            token_msg(list->t,
                      LogSeverityError,
                      "expected one of: param access, param def, instantiation, ref, markdown or text node");
            longjmp(*eenv, 1);
        } break;
    }
}

static ASTNode*
parse_template(jmp_buf* eenv, PageAlloc* allocator, TokenList* list)
{
    ASTNode node = {0};
    node.type = ASTNodeTemplate;

    tokenlist_expect(eenv, list, TokenTagOpen);
    tokenlist_expect_ident(eenv, list, g_keywords[KeywordTemplate]);
    tokenlist_expect(eenv, list, TokenColon);

    node.template.ident = *list->t;
    tokenlist_expect(eenv, list, TokenIdent);
    tokenlist_expect(eenv, list, TokenTagClose);

    ASTNode** append = &node.first_child;
    while (!(tokenlist_match(list, 0, TokenTagOpen) &&
             tokenlist_match(list, 1, TokenForwardSlash) &&
             tokenlist_match_ident(list, 2, g_keywords[KeywordTemplate])))
    {
        ASTNode* next = parse_inst_child(eenv, allocator, list, false);
        *append = next;
        append = &next->next;
    }

    tokenlist_advance(list, 3);
    tokenlist_expect(eenv, list, TokenTagClose);

    ast_trim_text_children(&node);

    return alloc_node(allocator, node);
}

static ASTNode*
parse_inst(jmp_buf* eenv, PageAlloc* allocator, TokenList* list, bool top_level_inst)
{
    ASTNode node = {0};
    node.type = ASTNodeInst;

    tokenlist_expect(eenv, list, TokenTagOpen);
    tokenlist_expect_ident(eenv, list, g_keywords[KeywordInst]);
    tokenlist_expect(eenv, list, TokenColon);
    node.inst.template_ident = *list->t;
    tokenlist_expect(eenv, list, TokenIdent);

    if (tokenlist_match(list, 0, TokenForwardSlash))
    {
        tokenlist_inc(list);
        tokenlist_expect(eenv, list, TokenTagClose);
    }
    else
    {
        tokenlist_expect(eenv, list, TokenTagClose);

        ASTNode** append = &node.first_child;
        while (!(tokenlist_match(list, 0, TokenTagOpen) &&
                 tokenlist_match(list, 1, TokenForwardSlash) &&
                 tokenlist_match_ident(list, 2, g_keywords[KeywordInst])))
        {
            ASTNode* next = parse_inst_child(eenv, allocator, list, top_level_inst);
            *append = next;
            append = &next->next;
        }

        tokenlist_advance(list, 3);
        tokenlist_expect(eenv, list, TokenTagClose);
    }

    ast_trim_text_children(&node);

    return alloc_node(allocator, node);
}

static ASTNode*
parse_top_level_child(jmp_buf* eenv, PageAlloc* allocator, TokenList* list)
{
    switch (list->t->type)
    {
        case TokenTextNode:
        {
            // Ignoring top level text nodes
            tokenlist_inc(list);
            return NULL;
        } break;

        case TokenTagOpen:
        {
            if (tokenlist_match_ident(list, 1, g_keywords[KeywordTemplate]))
            {
                return parse_template(eenv, allocator, list);
            }
            else if (tokenlist_match_ident(list, 1, g_keywords[KeywordInst]))
            {
                return parse_inst(eenv, allocator, list, true);
            }
            else if (tokenlist_match_ident(list, 1, g_keywords[KeywordDef]))
            {
                return parse_param_def(eenv, allocator, list, ParamDefLocationTopLevel);
            }
        }
        // SWITCH-CASE FALLTHROUGH
        default:
        {
            token_msg(list->t, LogSeverityError,
                      "top level: expected template definition, instantiation or global parameter definition");
            longjmp(*eenv, 1);
        } break;
    }
}

static ASTNode*
ast_construct(TokenList list, PageAlloc* node_allocator)
{
    assert(list.num_tokens); // Empty input file should generate TokenEof token

    ASTNode* root = NULL;
    ASTNode** n = &root;

    jmp_buf eenv;
    if (setjmp(eenv) == 0)
    {
        while (list.t->type != TokenEof)
        {
            ASTNode* next = parse_top_level_child(&eenv, node_allocator, &list);
            if (next)
            {
                *n = next;
                n = &next->next;
            }
        }
    }
    else
    {
        // Caught an exception
        return NULL;
    }

    return root;
}

#ifdef RUN_TESTS
TEST_FUNC(parser)
{
    PageAlloc alloc = page_alloc_create(sizeof(ASTNode), 256);
    jmp_buf eenv;

    {
        TokenList tokens = tokenize_string("Some text.");
        if (setjmp(eenv) == 0)
            test_assert(parse_text_node(&eenv, &alloc, &tokens));
        else
            test_assert(false);
        tokenlist_free(&tokens);
    }

    {
        TokenList tokens = tokenize_string("");
        bool err = true;
        TEST_DISABLE_OUT_STREAM(err_dup, stderr);
        if (setjmp(eenv) == 0)
        {
            parse_text_node(&eenv, &alloc, &tokens);
            err = false;
        }
        TEST_RESTORE_OUT_STREAM(err_dup, stderr);
        test_assert(err);
        tokenlist_free(&tokens);
    }

    {
        const char* str =
            "<@def:\n"
            "test />\n";
        TokenList tokens = tokenize_string(str);
        bool err = true;
        TEST_DISABLE_OUT_STREAM(err_dup, stderr);
        if (setjmp(eenv) == 0)
        {
            parse_param_def(&eenv, &alloc, &tokens, ParamDefLocationDefault);
            err = false;
        }
        TEST_RESTORE_OUT_STREAM(err_dup, stderr);
        test_assert(err);
        tokenlist_free(&tokens);
    }

    {
        const char* str =
            "<@template: template1>\n"
            "   <@CONTENT />\n"
            "<@/template>\n"
            "<@in: template1>\n"
            "   <@def: test1>TEST1<@/def>\n"
            "   <@if: test1>                A <@/if>\n"
            "   <@if: not test1>            B <@/if>\n"
            "   <@if: not test1=\"TEST2\">  C <@/if>\n"
            "   <@if: test1>                D <@else>   E <@/if>\n"
            "   <@if: undefined>            F <@else>   G <@/if>\n"
            "   <@if: not undefined>        H <@else>   I <@/if>\n"
            "   <@if: global1=\"GLOB*\">    J <@/if>\n"
            "   <@ref: ID=\"*\">\n"
            "       <@if: ref.test2>        K <@/if>\n"
            "       <@if: not ref.undefined>L <@/if>\n"
            "   <@/ref>\n"
            "   <@if: otherInst.test2=\"TEST*\">\n"
            "                               M\n"
            "   <@/if>\n"
            "   <@if: otherInst.undefined>  N <@else>   O <@/if>\n"
            "<@/in>\n"
            "<@in: template1>\n"
            "   <@def: ID>otherInst<@/def>\n"
            "   <@def: test2>TEST2<@/def>\n"
            "<@/in>\n"
            "<@def: global1>GLOBAL1<@/def>";

        TokenList tokens = tokenize_string(str);
        ASTNode* root = ast_construct(tokens, &alloc);
        test_assert(root);
        tokenlist_free(&tokens);
    }

    {
        const char* str =
            "<@template: nav>"
            "    <@ref: addToNav>"
            "        <@if: ref.dir>"
            "            <a href=\"<@siteDomain />/<@ref.dir />/\"><@ref.title /></a>"
            "        <@else>"
            "            <@if: ref.FILE=\"index.html\">"
            "                <a href=\"<@siteDomain />/\"><@ref.title /></a>"
            "            <@else>"
            "                <a href=\"<@siteDomain />/<@ref.FILE />\"><@ref.title /></a>"
            "            <@/if>"
            "        <@/if>"
            "    <@/ref>"
            "<@/template>";
        TokenList tokens = tokenize_string(str);
        ASTNode* root = ast_construct(tokens, &alloc);
        test_assert(root);
        tokenlist_free(&tokens);
    }

    {
        const char* str =
            "<@template: nav>"
            "    <@ref: addToNav>"
            "        <@if: ref.dir>"
            "            <a href=\"<@siteDomain />/<@ref.dir />/\"><@ref.title /></a>"
            "        <@else>"
            "            <@if: ref.FILE=\"index.html\">"
            "                <a href=\"<@siteDomain />/\"><@ref.title /></a>"
            "            <@else>"
            "                <a href=\"<@siteDomain />/<@ref.FILE />\"><@ref.title /></a>"
            "              " // missing /if
            "        <@/if>"
            "    <@/ref>"
            "<@/template>";
        TokenList tokens = tokenize_string(str);
        TEST_DISABLE_OUT_STREAM(errdup, stderr);
        ASTNode* root = ast_construct(tokens, &alloc);
        TEST_RESTORE_OUT_STREAM(errdup, stderr);
        test_assert(!root);
        tokenlist_free(&tokens);
    }

    page_alloc_free(&alloc);
    return true;
}
#endif
