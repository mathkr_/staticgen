typedef struct {
    /**
     * Base directory for all output file paths
     */
    const char*     out_dir;

    /**
     * Directory containing static files that get copied to the output directory before generating
     */
    const char*     static_dir;

    /**
     * Whether to preserve comments from input files or strip them
     */
    bool            preserve_comments;

    /**
     * Whether to generate verbose, detailed output about the program operation.
     */
    bool            verbose;

    /**
     * Whether to remove all files and directories recursively inside the output directory
     * before generating any output.
     */
    bool            clean_out_dir;

    /**
     * Whether to copy contents of static directory into output directory.
     */
    bool            copy_static_files;

} Config;
