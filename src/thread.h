#ifdef _WIN32

typedef CRITICAL_SECTION*   Mutex;
typedef HANDLE              Semaphore;

#else
#endif

static inline Mutex     mutex_create();
static inline void      mutex_destroy(Mutex* mutex);
static inline void      mutex_lock(Mutex* mutex);
static inline bool      mutex_try_lock(Mutex* mutex);
static inline void      mutex_unlock(Mutex* mutex);

static inline Semaphore semaphore_create(i32 initial_count);
static inline void      semaphore_destroy(Semaphore* sem);
static inline void      semaphore_wait(Semaphore* sem);
static inline bool      semaphore_try_wait(Semaphore* sem); // Returns true if the semaphore was signalled
static inline void      semaphore_signal(Semaphore* sem, i32 count);

typedef void (*JobFuncPtr)(void*);
typedef struct {
    JobFuncPtr  func;
    void*       userptr;
} Job;

// TAG: Array_Job Definition
#define ARRAY_ELEMENT_TYPE  Job
#define ARRAY_FUNC_PREFIX   job
#include "array_template.h"

/**
 * A single-producer job queue.
 */
#ifdef _WIN32

typedef struct {
    Mutex               mutex;

    Semaphore           jobs_available;
    Semaphore           jobs_finished;
    i32                 num_jobs_pushed;
    Array_Job           jobs;

    uintptr_t*          thread_handles;
    i32                 num_threads;

} JobQueueWin32;

#else
#endif

typedef JobQueueWin32 JobQueue;

static JobQueue*    jobqueue_create();
static void         jobqueue_destroy(JobQueue* queue);

// IMPORTANT: Since this is a single-producer-queue, there should only ever be one thread that is
// pushing jobs and waiting on them.
static void         jobqueue_push_jobs(JobQueue* queue, Job* jobs, i32 num_jobs);
static void         jobqueue_wait_until_finished(JobQueue* queue);
