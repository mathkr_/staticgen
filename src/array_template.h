#ifndef ARRAY_ELEMENT_TYPE
#error "Define ARRAY_ELEMENT_TYPE before including array.h"
#endif

#ifndef ARRAY_FUNC_PREFIX
#error "Define ARRAY_FUNC_PREFIX before including array.h (for example: 'v2' to make array_v2_create etc.)"
#endif

#ifndef ARR_FATAL
#define ARR_FATAL(...) arr_fatal(__FILE__, __LINE__, __VA_ARGS__)

#if defined(__clang__) || defined(__GNUC__)
#define ARR_PRINTF_FORMAT_ATT_NORETURN(format_idx, format_args) __attribute__((format(printf, format_idx, format_args), noreturn))
#elif defined(_MSC_VER) && !defined(__INTEL_COMPILER)
#define ARR_PRINTF_FORMAT_ATT_NORETURN(format_idx, format_args) __declspec(noreturn)
#endif

static void ARR_PRINTF_FORMAT_ATT_NORETURN(3, 4)
arr_fatal(const char* file, i32 line, const char* format, ...)
{
    fprintf(stderr, "fatal error in %s:%d: ", file, line);

    va_list args;
    va_start(args, format);
    vfprintf(stderr, format, args);
    va_end(args);

    fputc('\n', stderr);
    fflush(stderr);

    // "Invoke" the debugger :-)
    i32 x = *(volatile i32*)(0);
    fprintf(stderr, "%d", x); // Making sure the read isn't optimized away
    abort();
}
#endif

#define AR_IDX  u32

#ifndef AR_CONCAT

#define AR_CONCAT2(a, b)    a##b
#define AR_CONCAT(a, b)     AR_CONCAT2(a, b)

#define AR_STRINGIFY2(s)    #s
#define AR_STRINGIFY(s)     AR_STRINGIFY2(s)

#endif // AR_CONCAT

#define ARRAY               AR_CONCAT(Array_, ARRAY_ELEMENT_TYPE)
#define ARRAY_FUNC(name)    AR_CONCAT(AR_CONCAT(array_, ARRAY_FUNC_PREFIX), AR_CONCAT(_, name))

typedef struct {
    ARRAY_ELEMENT_TYPE* data;
    AR_IDX              cap;
    AR_IDX              len;

} ARRAY;

static inline ARRAY
ARRAY_FUNC(create)(AR_IDX init_capacity)
{
    assert(init_capacity);
    ARRAY res;
    res.cap = init_capacity;
    res.len = 0;
    res.data = malloc(sizeof(ARRAY_ELEMENT_TYPE) * res.cap);
    return res;
}

static inline ARRAY
ARRAY_FUNC(copy)(ARRAY* old)
{
    assert(old->cap);
    ARRAY res = *old;
    res.data = malloc(sizeof(ARRAY_ELEMENT_TYPE) * res.cap);
    memcpy(res.data, old->data, sizeof(ARRAY_ELEMENT_TYPE) * res.len);
    return res;
}

static inline ARRAY
ARRAY_FUNC(copy_trim)(ARRAY* old)
{
    ARRAY res = *old;
    res.cap = MAX(res.len, 1);
    res.data = malloc(sizeof(ARRAY_ELEMENT_TYPE) * res.cap);
    memcpy(res.data, old->data, sizeof(ARRAY_ELEMENT_TYPE) * res.len);
    return res;
}

static inline void
ARRAY_FUNC(free)(ARRAY* array)
{
    free(array->data);
    array->data = NULL;
    array->cap = array->len = 0;
}

static inline void
ARRAY_FUNC(clear)(ARRAY* array)
{
    array->len = 0;
}

static inline ARRAY_ELEMENT_TYPE
ARRAY_FUNC(get_safe)(ARRAY* array, AR_IDX idx)
{
    if (idx >= array->len)
        ARR_FATAL("out of bounds access in " AR_STRINGIFY(ARRAY) " (idx: %" PRIu64 ", len: %" PRIu64 ")", (u64)idx, (u64)array->len);

    return array->data[idx];
}

static inline ARRAY_ELEMENT_TYPE*
ARRAY_FUNC(ptr_safe)(ARRAY* array, AR_IDX idx)
{
    if (idx >= array->len)
        ARR_FATAL("out of bounds access in " AR_STRINGIFY(ARRAY) " (idx: %" PRIu64 ", len: %" PRIu64 ")", (u64)idx, (u64)array->len);

    return array->data + idx;
}

static inline ARRAY_ELEMENT_TYPE*
ARRAY_FUNC(push)(ARRAY* array, ARRAY_ELEMENT_TYPE elem)
{
    if (array->len >= array->cap)
    {
        array->cap *= 2;
        array->data = realloc(array->data, sizeof(ARRAY_ELEMENT_TYPE) * array->cap);
    }

    ARRAY_ELEMENT_TYPE* res = array->data + array->len++;
    *res = elem;
    return res;
}

static inline void
ARRAY_FUNC(append)(ARRAY* array, ARRAY_ELEMENT_TYPE* elems, AR_IDX num_elems)
{
    if (array->len + num_elems > array->cap)
    {
        AR_IDX new_cap = array->cap * 2;
        while (new_cap < array->len + num_elems)
        {
            new_cap *= 2;
        }
        array->cap = new_cap;
        array->data = realloc(array->data, sizeof(ARRAY_ELEMENT_TYPE) * array->cap);
    }

    memcpy(array->data + array->len, elems, num_elems * sizeof(ARRAY_ELEMENT_TYPE));
    array->len += num_elems;
}

static inline void
ARRAY_FUNC(remove_unordered)(ARRAY* array, AR_IDX idx)
{
    if (idx >= array->len)
        ARR_FATAL("out of bounds access in " AR_STRINGIFY(ARRAY) " (idx: %" PRIu64 ", len: %" PRIu64 ")", (u64)idx, (u64)array->len);

    array->data[idx] = array->data[--array->len];
}

/**
 * Pops the last element in the array if array.len is at least 1.
 *
 * @param out_element   May be NULL. If not NULL and array.len > 0, then the popped element is written into *out_element
 * @return              RESULT_SUCCESS if at least one element was present, RESULT_FAILURE otherwise
 *
 */
static inline i32
ARRAY_FUNC(pop_safe)(ARRAY* array, ARRAY_ELEMENT_TYPE* out_element)
{
    if (!array->len)
        return RESULT_FAILURE;

    if (out_element)
        *out_element = array->data[array->len - 1];

    --array->len;
    return RESULT_SUCCESS;
}


#undef ARRAY
#undef ARRAY_FUNC
#undef ARRAY_ELEMENT_TYPE
#undef AR_IDX
#undef AR_OUTOFBOUNDS_FMT
#undef ARRAY_FUNC_PREFIX
#undef ARRAY_INDEX_TYPE
