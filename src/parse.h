typedef enum {
    KeywordTemplate,
    KeywordInst,
    KeywordRef,
    KeywordDef,
    KeywordMarkdown,
    KeywordIf,
    KeywordElse,
    KeywordNot,

    NumLowerCaseKeywords,

    KeywordCONTENT = NumLowerCaseKeywords,
    KeywordFILE,
    KeywordOUT_DIR,
    KeywordSTATIC_DIR,
    KeywordID,

    NumKeywords

} Keywords;

typedef enum {
    ASTNodeText,
    ASTNodeTemplate,
    ASTNodeParamDef,
    ASTNodeParamAcc,
    ASTNodeRef,
    ASTNodeInst,
    ASTNodeMarkdown,
    ASTNodeIf,
    ASTNodeBlock,       // Used for if/else blocks

} ASTNodeType;

typedef struct ASTNode ASTNode;
struct ASTNode {
    ASTNodeType type;
    ASTNode*    next;
    ASTNode*    first_child;

    union {
        struct {
            Token text;

        } text;

        struct {
            Token ident;

        } template;

        struct {
            Token ident;

        } param_def;

        struct {
            Token               ident;
            InternedString      namespace;
            bool                default_value;

        } param_acc;

        struct {
            Token param_ident;
            const char* match_string; // NULL-terminated without delimiters

        } ref;

        struct {
            Token template_ident;

        } inst;

        struct {
            Token tag;

        } markdown;

        struct {
            Token           param_ident;
            InternedString  param_namespace;
            const char*     match_string;
            bool            not_operator;

        } if_clause;

        struct {
            Token   begin;

        } block;
    };
};

typedef ASTNode* ASTNodePtr;

// TAG: Array_ASTNodePtr Definition
#define ARRAY_ELEMENT_TYPE  ASTNodePtr
#define ARRAY_FUNC_PREFIX   astnodeptr
#include "array_template.h"

typedef enum {
    ParamDefLocationDefault,
    ParamDefLocationTopLevel,
    ParamDefLocationTopLevelInst,

} ParamDefLocation;
