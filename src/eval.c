static void
eval_stack_trace_print(FILE* out, Array_ASTNodePtr* stack)
{
    for (u32 i = 0; i < stack->len; ++i)
    {
        ASTNode* node = array_astnodeptr_get_safe(stack, i);
        fprintf(out, "%*s > ", 4 * i, "");
        switch (node->type)
        {
            case ASTNodeText: {
                fprintf(out, "text node (%s:%d:%d)\n",
                        node->text.text.file.path,
                        node->text.text.line,
                        node->text.text.column);
            } break;
            case ASTNodeTemplate: {
                fprintf(out, "template definition '%s' (%s:%d:%d)\n",
                        node->template.ident.intern,
                        node->template.ident.file.path,
                        node->template.ident.line,
                        node->template.ident.column);
            } break;
            case ASTNodeMarkdown: {
                fprintf(out, "markdown block (%s:%d:%d)\n",
                        node->markdown.tag.file.path,
                        node->markdown.tag.line,
                        node->markdown.tag.column);
            } break;
            case ASTNodeParamDef: {
                fprintf(out, "param definition '%s' (%s:%d:%d)\n",
                        node->param_def.ident.intern,
                        node->param_def.ident.file.path,
                        node->param_def.ident.line,
                        node->param_def.ident.column);
            } break;
            case ASTNodeParamAcc: {
                fprintf(out, "param access '%s%s%s' (%s:%d:%d)\n",
                        (node->param_acc.namespace ? node->param_acc.namespace : ""),
                        (node->param_acc.namespace ? "." : ""),
                        node->param_acc.ident.intern,
                        node->param_acc.ident.file.path,
                        node->param_acc.ident.line,
                        node->param_acc.ident.column);
            } break;
            case ASTNodeRef: {
                fprintf(out, "ref '%s=\"%s\"' (%s:%d:%d)\n",
                        node->ref.param_ident.intern,
                        node->ref.match_string,
                        node->ref.param_ident.file.path,
                        node->ref.param_ident.line,
                        node->ref.param_ident.column);
            } break;
            case ASTNodeIf: {
                fprintf(out, "if '%s%s%s=\"%s\"' (%s:%d:%d)\n",
                        (node->if_clause.param_namespace ? node->if_clause.param_namespace : ""),
                        (node->if_clause.param_namespace ? "." : ""),
                        node->if_clause.param_ident.intern,
                        node->if_clause.match_string,
                        node->if_clause.param_ident.file.path,
                        node->if_clause.param_ident.line,
                        node->if_clause.param_ident.column);
            } break;
            case ASTNodeBlock: {
                fprintf(out, "block (%s:%d:%d)\n",
                        node->block.begin.file.path,
                        node->block.begin.line,
                        node->block.begin.column);
            } break;
            case ASTNodeInst: {
                fprintf(out, "template instantiation '%s' (%s:%d:%d)\n",
                        node->inst.template_ident.intern,
                        node->inst.template_ident.file.path,
                        node->inst.template_ident.line,
                        node->inst.template_ident.column);
            } break;
        }
    }
}

static const char*
evalstrings_expand(EvalString* strings, u32 num, size_t* out_len)
{
    size_t len = 0;
    for (u32 i = 0; i < num; ++i)
    {
        len += strings[i].len;
    }

    char* res = malloc(len + 1);
    char* p = res;
    for (u32 i = 0; i < num; ++i)
    {
        EvalString* s = strings + i;
        memcpy(p, s->val, s->len);
        p += s->len;
    }
    p[0] = '\0';
    assert(p == res + len);
    if (out_len)
        *out_len = len;
    return res;
}

static void
evalparam_free(EvalParam* p)
{
    array_evalstr_free(&p->strings);
    if (p->expanded_result)
        free((void*)p->expanded_result);
}

static void
evalparam_expand(EvalParam* param)
{
    assert(!param->expanded_result && !param->trimmed_result);
    param->expanded_result = evalstrings_expand(param->strings.data, param->strings.len, NULL);
    param->trimmed_result = str_trim_in_place((char*)param->expanded_result);
}

static void
evalparamstack_set_sp(Array_EvalParam* stack, u32 sp)
{
    for (u32 i = sp; i < stack->len; ++i)
    {
        EvalParam* p = array_evalparam_ptr_safe(stack, i);
        evalparam_free(p);
    }
    stack->len = sp;
}

static void
evalparamstack_free(Array_EvalParam* stack)
{
    for (u32 i = 0; i < stack->len; ++i)
    {
        EvalParam* p = array_evalparam_ptr_safe(stack, i);
        evalparam_free(p);
    }
    array_evalparam_free(stack);
}

static EvalParam*
evalparamstack_search(Array_EvalParam* stack, InternedString ident_intern)
{
    for (u32 i = stack->len - 1; i < stack->len; --i) // u32 wrap
    {
        EvalParam* p = array_evalparam_ptr_safe(stack, i);
        if (p->intern == ident_intern)
            return p;
    }
    return NULL;
}

static Array_EvalParam
evalparamstack_copy(Array_EvalParam* old)
{
    Array_EvalParam res = array_evalparam_copy(old);
    for (u32 i = 0; i < res.len; ++i)
    {
        EvalParam* p = array_evalparam_ptr_safe(&res, i);
        p->strings = array_evalstr_copy_trim(&p->strings);
        if (p->expanded_result)
        {
            p->expanded_result = STRDUP(p->trimmed_result);
            p->trimmed_result = p->expanded_result;
        }
    }
    return res;
}

static EvalParam*
evalparamstack_closest_match(Array_EvalParam* stack, InternedString ident_intern)
{
    i32 min_dist = INT32_MAX;
    EvalParam* closest = NULL;

    for (u32 i = stack->len - 1; i < stack->len; --i) // u32 wrap
    {
        EvalParam* p = array_evalparam_ptr_safe(stack, i);
        i32 dist = str_levenshtein_ignorecase(p->intern, ident_intern);
        if (dist < min_dist)
        {
            min_dist = dist;
            closest = p;
        }
    }

    if (min_dist <= 3)
        return closest;
    else
        return NULL;
}

static void
evalcontext_init(EvalContext* ctx)
{
    memset(ctx, 0, sizeof *ctx);
    ctx->templates = dict_create(128);
    ctx->inst_ids = dict_create(64);
    ctx->toplvlinsts = array_evaltoplvlinst_create(32);
    ctx->global_param_stack = array_evalparam_create(32);
    ctx->allocated_strings = array_evalstr_create(32);
    ctx->stringbuf = stringbuf_create(MEGABYTES(1));
}

static void
evalcontext_deinit(EvalContext* ctx)
{
    evalparamstack_free(&ctx->global_param_stack);
    for (u32 i = 0; i < ctx->toplvlinsts.len; ++i)
    {
        EvalTopLvlInst* inst = array_evaltoplvlinst_ptr_safe(&ctx->toplvlinsts, i);
        evalparamstack_free(&inst->param_stack);
    }
    array_evaltoplvlinst_free(&ctx->toplvlinsts);
    dict_free(&ctx->templates);
    dict_free(&ctx->inst_ids);
    for (u32 i = 0; i < ctx->allocated_strings.len; ++i)
    {
        EvalString* str = array_evalstr_ptr_safe(&ctx->allocated_strings, i);
        free((void*)str->val);
    }
    array_evalstr_free(&ctx->allocated_strings);
    stringbuf_free(&ctx->stringbuf);
}

static inline void
eval_text(Array_EvalString* res_stack,
          ASTNode*          node)
{
    assert(node->type == ASTNodeText);
    array_evalstr_push(res_stack, (EvalString){
        .val        = node->text.text.value,
        .len        = node->text.text.len,
    });
}

static EvalResult eval_param_def_child(EvalContext*, Array_ASTNodePtr*, Array_EvalString*, Array_EvalParam*, EvalTopLvlInst*, ASTNode*);

static EvalResult
eval_param_acc(EvalContext*         ctx,
               Array_ASTNodePtr*    stack_trace,
               Array_EvalString*    res_stack,
               Array_EvalParam*     param_stack,
               EvalTopLvlInst*      ref_inst,
               ASTNode*             node)
{
    assert(node->type == ASTNodeParamAcc);
    array_astnodeptr_push(stack_trace, node);

    EvalParam* p = NULL;
    EvalTopLvlInst* inst = NULL;

    if (node->param_acc.namespace == g_keywords[KeywordRef])
    {
        if (!ref_inst)
        {
            token_msg(&node->param_acc.ident, LogSeverityError, "cannot access ref prefixed param outside of ref element");
            return EvalResFailure;
        }
        inst = ref_inst;
        p = evalparamstack_search(&ref_inst->param_stack, node->param_acc.ident.intern);
    }
    else if (node->param_acc.namespace)
    {
        inst = dict_get(&ctx->inst_ids, node->param_acc.namespace);
        if (!inst)
        {
            token_msg(&node->param_acc.ident,
                      LogSeverityError,
                      "no top-level instantiation found for referenced ID \"%s\"",
                      node->param_acc.namespace);
            return EvalResFailure;
        }
        p = evalparamstack_search(&inst->param_stack, node->param_acc.ident.intern);
    }
    else
    {
        p = evalparamstack_search(param_stack, node->param_acc.ident.intern);
    }

    if (p)
    {
        array_evalstr_append(res_stack, p->strings.data, p->strings.len);
    }
    else
    {
        if (node->param_acc.default_value)
        {
            // Param Access with default value
            for (ASTNode* child = node->first_child; child != NULL; child = child->next)
            {
                EvalResult child_res = eval_param_def_child(ctx, stack_trace, res_stack, param_stack, ref_inst, child);
                if (child_res != EvalResSuccess)
                {
                    return child_res;
                }
            }
        }
        else
        {
            if (node->param_acc.namespace)
            {
                assert(inst);
                token_msg(&node->param_acc.ident,
                          LogSeverityError,
                          "parameter \"%s\" not found in referenced instantiation",
                          node->param_acc.ident.intern);
                token_msg(&inst->node->param_acc.ident,
                          LogSeverityErrorNote,
                          "parameter expected in this instantiation");

                p = evalparamstack_closest_match(&inst->param_stack, node->param_acc.ident.intern);
                if (p)
                {
                    token_msg(&p->token,
                              LogSeverityErrorNote,
                              "did you mean \"%s.%s\"?",
                              node->param_acc.namespace,
                              p->intern);
                }
            }
            else
            {
                token_msg(&node->param_acc.ident,
                          LogSeverityError,
                          "parameter \"%s\" not found",
                          node->param_acc.ident.intern);
                p = evalparamstack_closest_match(param_stack, node->param_acc.ident.intern);
                if (p)
                    token_msg(&p->token, LogSeverityErrorNote, "did you mean \"%s\"?", p->intern);
            }

            io_err_note("evaluation stack trace:");
            eval_stack_trace_print(stderr, stack_trace);
            return EvalResFailure;
        }
    }

    array_astnodeptr_pop_safe(stack_trace, NULL);
    return EvalResSuccess;
}

static EvalResult
eval_param_def_child(EvalContext*       ctx,
                     Array_ASTNodePtr*  stack_trace,
                     Array_EvalString*  res_stack,
                     Array_EvalParam*   param_stack,
                     EvalTopLvlInst*    ref_inst,
                     ASTNode*           node)
{
    switch (node->type)
    {
        case ASTNodeParamAcc:
        {
            return eval_param_acc(ctx, stack_trace, res_stack, param_stack, ref_inst, node);
        } break;

        case ASTNodeText:
        {
            eval_text(res_stack, node);
            return EvalResSuccess;
        } break;

        default: IO_FATAL("unexpected node type in eval_param_def_child");
    }
}

static EvalResult
eval_param_def(EvalContext*         ctx,
               Array_ASTNodePtr*    stack_trace,
               Array_EvalString*    res_stack,
               Array_EvalParam*     param_stack,
               EvalTopLvlInst*      ref_inst,
               ASTNode*             node)
{
    assert(node->type == ASTNodeParamDef);
    array_astnodeptr_push(stack_trace, node);

    u32 res_sp = res_stack->len;
    for (ASTNode* child = node->first_child; child != NULL; child = child->next)
    {
        EvalResult child_res = eval_param_def_child(ctx, stack_trace, res_stack, param_stack, ref_inst, child);
        if (child_res != EvalResSuccess)
        {
            return child_res;
        }
    }

    assert(res_stack->len >= res_sp);
    u32 num_results = res_stack->len - res_sp;

    EvalParam p = {0};
    p.intern = node->param_def.ident.intern;
    p.strings = array_evalstr_create(MAX(1, num_results));
    p.token = node->param_def.ident;

    if (num_results)
    {
        array_evalstr_append(&p.strings, array_evalstr_ptr_safe(res_stack, res_sp), num_results);
        res_stack->len = res_sp;
    }

    array_evalparam_push(param_stack, p);
    array_astnodeptr_pop_safe(stack_trace, NULL);
    return EvalResSuccess;
}

static EvalResult eval_inst(EvalContext*, Array_ASTNodePtr*, Array_EvalString*, Array_EvalParam*, EvalTopLvlInst*, ASTNode*);
static EvalResult eval_markdown(EvalContext*, Array_ASTNodePtr*, Array_EvalString*, Array_EvalParam*, EvalTopLvlInst*, ASTNode*);
static EvalResult eval_ref(EvalContext*, Array_ASTNodePtr*, Array_EvalString*, Array_EvalParam*, ASTNode*);
static EvalResult eval_if(EvalContext*, Array_ASTNodePtr*, Array_EvalString*, Array_EvalParam*, EvalTopLvlInst*, ASTNode*);

static EvalResult
eval_inst_child(EvalContext*        ctx,
                Array_ASTNodePtr*   stack_trace,
                Array_EvalString*   res_stack,
                Array_EvalParam*    param_stack,
                EvalTopLvlInst*     ref_inst,
                ASTNode*            node)
{
    switch (node->type)
    {
        case ASTNodeInst:
        {
            return eval_inst(ctx, stack_trace, res_stack, param_stack, ref_inst, node);
        } break;

        case ASTNodeParamDef:
        {
            return eval_param_def(ctx, stack_trace, res_stack, param_stack, ref_inst, node);
        } break;

        case ASTNodeParamAcc:
        {
            return eval_param_acc(ctx, stack_trace, res_stack, param_stack, ref_inst, node);
        } break;

        case ASTNodeText:
        {
            eval_text(res_stack, node);
            return EvalResSuccess;
        } break;

        case ASTNodeRef:
        {
            return eval_ref(ctx, stack_trace, res_stack, param_stack, node);
        } break;

        case ASTNodeMarkdown:
        {
            return eval_markdown(ctx, stack_trace, res_stack, param_stack, ref_inst, node);
        } break;

        case ASTNodeIf:
        {
            return eval_if(ctx, stack_trace, res_stack, param_stack, ref_inst, node);
        } break;

        default: IO_FATAL("unexpected node type in eval_inst_child");
    }
}

static EvalResult
eval_inst(EvalContext*          ctx,
          Array_ASTNodePtr*     stack_trace,
          Array_EvalString*     res_stack,
          Array_EvalParam*      param_stack,
          EvalTopLvlInst*       ref_inst,
          ASTNode*              node)
{
    assert(node->type == ASTNodeInst);
    array_astnodeptr_push(stack_trace, node);

    // Search the stack_trace to prevent a template instatiating itself in its definition
    for (u32 i = 0; i < stack_trace->len; ++i)
    {
        ASTNode* parent = array_astnodeptr_get_safe(stack_trace, i);
        if (parent->type == ASTNodeTemplate &&
            parent->template.ident.intern == node->inst.template_ident.intern)
        {
            token_msg(&node->inst.template_ident, LogSeverityError, "a template may not instantiate itself");
            io_err_note("evaluation stack trace:");
            eval_stack_trace_print(stderr, stack_trace);
            return EvalResFailure;
        }
    }

    u32 param_sp = param_stack->len;
    u32 content_sp = res_stack->len;

    for (ASTNode* child = node->first_child; child != NULL; child = child->next)
    {
        EvalResult child_res = eval_inst_child(ctx, stack_trace, res_stack, param_stack, ref_inst, child);
        if (child_res != EvalResSuccess)
        {
            return child_res;
        }
    }

    assert(res_stack->len >= content_sp);
    u32 num_results = res_stack->len - content_sp;

    EvalParam content = {0};
    content.intern = g_keywords[KeywordCONTENT];
    content.strings = array_evalstr_create(MAX(1, num_results));
    content.token = node->inst.template_ident;

    if (num_results)
    {
        array_evalstr_append(&content.strings,
                             array_evalstr_ptr_safe(res_stack, content_sp),
                             num_results);
        res_stack->len = content_sp;
    }

    array_evalparam_push(param_stack, content);

    ASTNode* template = dict_get(&ctx->templates, node->inst.template_ident.intern);
    if (template)
    {
        array_astnodeptr_push(stack_trace, template);

        for (ASTNode* child = template->first_child; child != NULL; child = child->next)
        {
            // ref_inst doesn't transfer over to the template scope
            EvalResult child_res = eval_inst_child(ctx, stack_trace, res_stack, param_stack, NULL, child);
            if (child_res != EvalResSuccess)
            {
                return child_res;
            }
        }

        array_astnodeptr_pop_safe(stack_trace, NULL);
    }

    evalparamstack_set_sp(param_stack, param_sp);

    if (!template)
    {
        token_msg(&node->inst.template_ident, LogSeverityError, "template not found");
        return EvalResFailure;
    }

    array_astnodeptr_pop_safe(stack_trace, NULL);
    return EvalResSuccess;
}

static void
eval_markdown_callback(const MD_CHAR* text, MD_SIZE size, void* userdata)
{
    StringBuffer* stringbuf = userdata;
    stringbuf_append(stringbuf, text, size);
}

static EvalResult
eval_if(EvalContext*          ctx,
        Array_ASTNodePtr*     stack_trace,
        Array_EvalString*     res_stack,
        Array_EvalParam*      param_stack,
        EvalTopLvlInst*       ref_inst,
        ASTNode*              node)
{
    assert(node->type == ASTNodeIf);
    array_astnodeptr_push(stack_trace, node);

    EvalParam* p = NULL;
    if (!node->if_clause.param_namespace)
    {
        p = evalparamstack_search(param_stack, node->if_clause.param_ident.intern);
    }
    else if (node->if_clause.param_namespace == g_keywords[KeywordRef])
    {
        if (!ref_inst)
        {
            token_msg(&node->if_clause.param_ident, LogSeverityError, "cannot access ref prefixed param outside of ref element");
            return EvalResFailure;
        }
        p = evalparamstack_search(&ref_inst->param_stack, node->if_clause.param_ident.intern);
    }
    else
    {
        EvalTopLvlInst* inst = dict_get(&ctx->inst_ids, node->if_clause.param_namespace);
        if (!inst)
        {
            token_msg(&node->if_clause.param_ident, LogSeverityError,
                      "no top-level instantiation found for referenced ID \"%s\"",
                      node->if_clause.param_namespace);
            return EvalResFailure;
        }
        p = evalparamstack_search(&inst->param_stack, node->if_clause.param_ident.intern);
    }

    bool eval_if_block;
    if (!p)
    {
        eval_if_block = node->if_clause.not_operator;
    }
    else
    {
        bool match = true;
        if (node->if_clause.match_string)
        {
            if (!p->trimmed_result)
                evalparam_expand(p);
            match = wildcard_match(p->trimmed_result, node->if_clause.match_string);
        }
        eval_if_block = node->if_clause.not_operator != match;
    }

    u32 param_sp = param_stack->len;

    ASTNode* if_block = node->first_child;
    if (eval_if_block)
    {
        array_astnodeptr_push(stack_trace, if_block);
        for (ASTNode* child = if_block->first_child; child != NULL; child = child->next)
        {
            EvalResult child_result = eval_inst_child(ctx, stack_trace, res_stack, param_stack, ref_inst, child);
            if (child_result != EvalResSuccess)
            {
                return child_result;
            }
        }
        array_astnodeptr_pop_safe(stack_trace, NULL);
    }
    else if (if_block->next)
    {
        ASTNode* else_block = if_block->next;
        array_astnodeptr_push(stack_trace, else_block);
        for (ASTNode* child = else_block->first_child; child != NULL; child = child->next)
        {
            EvalResult child_result = eval_inst_child(ctx, stack_trace, res_stack, param_stack, ref_inst, child);
            if (child_result != EvalResSuccess)
            {
                return child_result;
            }
        }
        array_astnodeptr_pop_safe(stack_trace, NULL);
    }

    evalparamstack_set_sp(param_stack, param_sp);
    array_astnodeptr_pop_safe(stack_trace, NULL);
    return EvalResSuccess;
}

static EvalResult
eval_markdown(EvalContext*          ctx,
              Array_ASTNodePtr*     stack_trace,
              Array_EvalString*     res_stack,
              Array_EvalParam*      param_stack,
              EvalTopLvlInst*       ref_inst,
              ASTNode*              node)
{
    assert(node->type == ASTNodeMarkdown);
    array_astnodeptr_push(stack_trace, node);

    u32 result_sp = res_stack->len;
    u32 param_sp = param_stack->len;

    for (ASTNode* child = node->first_child; child != NULL; child = child->next)
    {
        EvalResult child_result = eval_inst_child(ctx, stack_trace, res_stack, param_stack, ref_inst, child);
        if (child_result != EvalResSuccess)
        {
            return child_result;
        }
    }

    evalparamstack_set_sp(param_stack, param_sp);

    u32 num_results = res_stack->len - result_sp;
    if (!num_results)
    {
        array_astnodeptr_pop_safe(stack_trace, NULL);
        return EvalResSuccess;
    }

    size_t expansion_len = 0;
    const char* expansion =
        evalstrings_expand(array_evalstr_ptr_safe(res_stack, result_sp), num_results, &expansion_len);

    res_stack->len = result_sp;

    // IMPORTANT: using the ctx stringbuf has to be changed if/when multi-threading the eval code
    stringbuf_clear(&ctx->stringbuf);
    i32 md_conversion_result = md_html(expansion,
                                       expansion_len,
                                       eval_markdown_callback,
                                       &ctx->stringbuf,     // void* userdata
                                                            // unsigned parser_flags
                                       MD_FLAG_NOINDENTEDCODEBLOCKS|MD_FLAG_COLLAPSEWHITESPACE,
                                       0);                  // unsigned renderer_flags
    free((void*)expansion);

    EvalString result = {
        .val = stringbuf_copy_to_cstring(&ctx->stringbuf),
        .len = ctx->stringbuf.len,
    };
    array_evalstr_push(&ctx->allocated_strings, result);
    array_evalstr_push(res_stack, result);

    if (md_conversion_result)
    {
        io_err("markdown conversion error (in block: %s:%d:%d)",
               node->markdown.tag.file.path,
               node->markdown.tag.line,
               node->markdown.tag.column);
        return EvalResFailure;
    }

    array_astnodeptr_pop_safe(stack_trace, NULL);
    return EvalResSuccess;
}

static EvalResult
eval_ref(EvalContext*           ctx,
         Array_ASTNodePtr*      stack_trace,
         Array_EvalString*      res_stack,
         Array_EvalParam*       param_stack,
         ASTNode*               node)
{
    assert(node->type == ASTNodeRef);
    array_astnodeptr_push(stack_trace, node);

    u32 param_sp = param_stack->len;

    bool matched_inst = false;
    for (u32 i = 0; i < ctx->toplvlinsts.len; ++i)
    {
        EvalTopLvlInst* inst = array_evaltoplvlinst_ptr_safe(&ctx->toplvlinsts, i);
        EvalParam* p = evalparamstack_search(&inst->param_stack, node->ref.param_ident.intern);
        if (p)
        {
            bool match = true;

            if (node->ref.match_string)
            {
                if (!p->trimmed_result)
                    evalparam_expand(p); // IMPORTANT: modifying ctx in eval isn't thread-safe
                match = wildcard_match(p->trimmed_result, node->ref.match_string);
            }

            if (match)
            {
                matched_inst = true;
                for (ASTNode* child = node->first_child; child != NULL; child = child->next)
                {
                    EvalResult child_result = eval_inst_child(ctx,
                                                              stack_trace,
                                                              res_stack,
                                                              param_stack,
                                                              inst,
                                                              child);
                    if (child_result != EvalResSuccess)
                    {
                        return child_result;
                    }
                }
            }
        }
    }

    if (!matched_inst)
    {
        token_msg(&node->ref.param_ident, LogSeverityWarn, "no inst matched by ref");
    }

    evalparamstack_set_sp(param_stack, param_sp);
    array_astnodeptr_pop_safe(stack_trace, NULL);

    return EvalResSuccess;
}

static EvalResult
eval_global_dep_iter_create(Array_ASTNodePtr* astnodes, EvalGlobalDepIter* out_res)
{
    // (1) For each astnode:
    //  - check if there already is a corresponding graph node,
    //    if yes, we have an illegal param redefinition at top level
    //  - create a graph_node
    // (2) For each graph_node:
    //  - add all its dependencies as edges:
    //      - only unique dependencies
    //      - fail if dependency is not found

    assert(astnodes->len);

    EvalGlobalDepIter* iter = out_res;
    memset(iter, 0, sizeof *out_res);
    iter->graph         = array_depnode_create(astnodes->len);
    iter->return_stack  = array_depnodeptr_create(16);

    Dict node_table = dict_create(64); // param_def.intern -> depnode*

    // (1)

    for (u32 i = 0; i < astnodes->len; ++i)
    {
        DepNode n = {0};
        n.astnode = array_astnodeptr_get_safe(astnodes, i);
        assert(n.astnode->type == ASTNodeParamDef);
        n.edges = array_depnodeptr_create(8);
        DepNode* p = array_depnode_push(&iter->graph, n);

        DictEntry entry = dict_get_entry(&node_table, n.astnode->param_def.ident.intern);
        if (entry.present)
        {
            DepNode* prev = entry.value;
            token_msg(&n.astnode->param_acc.ident, LogSeverityError, "global parameter redefinition");
            token_msg(&prev->astnode->param_acc.ident, LogSeverityErrorNote, "previously defined here");
            goto return_failure;
        }

        entry.value = p;
        dict_apply_entry(&node_table, &entry);
    }

    // (2)

    for (u32 i = 0; i < iter->graph.len; ++i)
    {
        DepNode* n = array_depnode_ptr_safe(&iter->graph, i);

        for (ASTNode* child = n->astnode->first_child; child != NULL; child = child->next)
        {
            if (child->type == ASTNodeParamAcc)
            {
                DepNode* dependency = dict_get(&node_table, child->param_acc.ident.intern);
                if (dependency)
                {
                    // Check if edge already exists
                    bool found = false;
                    for (u32 j = 0; !found && j < n->edges.len; ++j)
                    {
                        if (dependency == array_depnodeptr_get_safe(&n->edges, j))
                            found = true;
                    }

                    if (!found)
                        array_depnodeptr_push(&n->edges, dependency);
                }
                else
                {
                    token_msg(&child->param_acc.ident, LogSeverityError, "parameter not found");
                    goto return_failure;
                }
            }
        }
    }

    iter->next = array_depnode_ptr_safe(&iter->graph, 0);

    dict_free(&node_table);
    return EvalResSuccess;

return_failure:
    dict_free(&node_table);
    return EvalResFailure;
}

static EvalResult
eval_gdi_push_dependency(EvalGlobalDepIter* iter, DepNode* n)
{
    // Check for cycle
    for (u32 i = 0; i < iter->return_stack.len; ++i)
    {
        DepNode* return_node = array_depnodeptr_get_safe(&iter->return_stack, i);
        if (return_node == n)
        {
            token_msg(&n->astnode->param_def.ident,
                      LogSeverityError,
                      "cyclical dependency in global param definitions");
            io_err_note("cyclical dependency graph:");
            fprintf(stderr, "(def: %s) ", n->astnode->param_def.ident.intern);
#ifdef _MSC_VER
            fflush(stderr);
#endif
            return EvalResFailure;
        }
    }

    if (!n->visited)
    {
        n->visited = true;
        array_depnodeptr_push(&iter->return_stack, n);
    }

    for (u32 i = 0; i < n->edges.len; ++i)
    {
        DepNode* dependency = array_depnodeptr_get_safe(&n->edges, i);

        EvalResult result = eval_gdi_push_dependency(iter, dependency);
        if (result != EvalResSuccess)
        {
            // Do our part in printing the dependency graph
            fprintf(stderr, "<- (def: %s) ", n->astnode->param_def.ident.intern);
#ifdef _MSC_VER
            fflush(stderr);
#endif
            return result;
        }
    }

    return EvalResSuccess;
}

static EvalResult
eval_global_dep_iter_next(EvalGlobalDepIter* iter, ASTNode** out_result)
{
    DepNode* res_node = NULL;
    if (array_depnodeptr_pop_safe(&iter->return_stack, &res_node) == RESULT_SUCCESS)
    {
        assert(res_node->visited);
        *out_result = res_node->astnode;
        return EvalResSuccess;
    }
    else if (iter->next >= iter->graph.data + iter->graph.len)
    {
        // Iter is finished
        *out_result = NULL;
        return EvalResSuccess;
    }
    else
    {
        // Find the next unvisited node
        DepNode* n = iter->next++;
        while (n->visited && iter->next < (iter->graph.data + iter->graph.len))
        {
            n = iter->next++;
        }

        if (!n->visited)
        {
            EvalResult res = eval_gdi_push_dependency(iter, n);
            if (res == EvalResSuccess)
            {
                assert(n->visited);
                assert(iter->return_stack.len);
                DepNode* res = NULL;
                array_depnodeptr_pop_safe(&iter->return_stack, &res);
                *out_result = res->astnode;
                return EvalResSuccess;
            }
            else
            {
                // Do our part in printing the dependency graph
                fprintf(stderr, "\n");
#ifdef _MSC_VER
                fflush(stderr);
#endif
                *out_result = NULL;
                return EvalResFailure;
            }
        }
        else
        {
            // Iter is finished
            *out_result = NULL;
            return EvalResSuccess;
        }
    }
}

static void
eval_global_dep_iter_free(EvalGlobalDepIter* iter)
{
    for (u32 i = 0; i < iter->graph.len; ++i)
    {
        DepNode* n = array_depnode_ptr_safe(&iter->graph, i);
        array_depnodeptr_free(&n->edges);
    }
    array_depnode_free(&iter->graph);
    array_depnodeptr_free(&iter->return_stack);
    memset(iter, 0, sizeof *iter);
}

static EvalResult
eval_top_level_passes(ASTNode* root, EvalContext* out_context)
{
    // This function handles 3 passes over the document top level (AST root and all its
    // siblings):

    EvalContext* ctx = out_context;
    evalcontext_init(ctx);

    Array_ASTNodePtr global_param_nodes = array_astnodeptr_create(32);
    Array_ASTNodePtr inst_nodes = array_astnodeptr_create(32);
    Array_EvalString result_stack = array_evalstr_create(64);
    Array_ASTNodePtr stack_trace = array_astnodeptr_create(16);

    // 1st pass:
    // - Find template definitions
    // - Find all global parameters (extra step for global param order independence)
    // - Save all top level insts while we're at it to speed up the third pass

    for (ASTNode* node = root; node != NULL; node = node->next)
    {
        switch (node->type)
        {
            case ASTNodeTemplate:
            {
                DictEntry entry = dict_get_entry(&ctx->templates, node->template.ident.intern);
                if (entry.present)
                {
                    ASTNode* prev = entry.value;
                    token_msg(&node->template.ident, LogSeverityError, "template already defined");
                    token_msg(&prev->template.ident, LogSeverityErrorNote, "previously defined here");
                    goto return_failure;
                }
                else
                {
                    entry.value = node;
                    dict_apply_entry(&ctx->templates, &entry);
                }
            } break;

            case ASTNodeParamDef:
            {
                array_astnodeptr_push(&global_param_nodes, node);
            } break;

            case ASTNodeInst:
            {
                array_astnodeptr_push(&inst_nodes, node);
            } break;

            default: break;
        }
    }

    // 2nd pass:
    // - Eval global parameters and save a global param stack

    if (global_param_nodes.len)
    {
        EvalGlobalDepIter param_iter;
        EvalResult param_resolve_result = eval_global_dep_iter_create(&global_param_nodes, &param_iter);
        if (param_resolve_result != EvalResSuccess)
        {
            eval_global_dep_iter_free(&param_iter);
            goto return_failure;
        }

        ASTNode* next_param = NULL;
        param_resolve_result = eval_global_dep_iter_next(&param_iter, &next_param);
        while (next_param)
        {
            array_evalstr_clear(&result_stack);
            eval_param_def(ctx, &stack_trace, &result_stack, &ctx->global_param_stack, NULL, next_param);
            param_resolve_result = eval_global_dep_iter_next(&param_iter, &next_param);
        }
        eval_global_dep_iter_free(&param_iter);
        if (param_resolve_result != EvalResSuccess)
        {
            goto return_failure;
        }
    }

    // 3rd pass (partial top level inst evaluation):
    // - Per top level inst
    //     - Eval same scope param defs
    //     - Save param stack (based on a copy of the global stack)
    //     - Put ID in dict if one exists

    for (u32 i = 0; i < inst_nodes.len; ++i)
    {
        EvalTopLvlInst* inst = array_evaltoplvlinst_push(&ctx->toplvlinsts, (EvalTopLvlInst){0});
        inst->param_stack = evalparamstack_copy(&ctx->global_param_stack);
        inst->node = array_astnodeptr_get_safe(&inst_nodes, i);

        assert(result_stack.len == 0);

        for (ASTNode* child = inst->node->first_child; child != NULL; child = child->next)
        {
            switch (child->type)
            {
                case ASTNodeParamDef:
                {
                    EvalResult result = eval_param_def(ctx,
                                                       &stack_trace,
                                                       &result_stack,
                                                       &inst->param_stack,
                                                       NULL,
                                                       child);
                    if (result != EvalResSuccess)
                        goto return_failure;
                } break;

                // The others we cannot yet evaluate because we need to be able to resolve refs.
                // Hence this whole "pre-eval" pass. We in fact need to resolve the param defs all
                // over again since params may get redefined in the same scope which can change the
                // output of other intermingled elements.
                default: break;
            }
        }

        EvalParam* id = evalparamstack_search(&inst->param_stack, g_keywords[KeywordID]);
        if (id)
        {
            assert(!id->expanded_result);
            evalparam_expand(id);
            if (!str_is_identifier(id->trimmed_result))
            {
                token_msg(&id->token, LogSeverityError, "built-in parameter ID must be a valid identifier");
                goto return_failure;
            }
            InternedString id_intern = interntable_intern_string(&g_interntable, id->trimmed_result);
            for (i32 i = 0; i < NumKeywords; ++i)
            {
                if (id_intern == g_keywords[i])
                {
                    token_msg(&id->token, LogSeverityError, "built-in parameter ID must not be equal to a reserved keyword");
                    goto return_failure;
                }
            }

            DictEntry entry = dict_get_entry(&ctx->inst_ids, id->trimmed_result);
            if (entry.present)
            {
                token_msg(&id->token, LogSeverityError, "duplicate top-level instantiation ID");
                EvalTopLvlInst* other_inst = entry.value;
                EvalParam* other_id = evalparamstack_search(&other_inst->param_stack, g_keywords[KeywordID]);
                token_msg(&other_id->token, LogSeverityErrorNote, "previously defined here");
                goto return_failure;
            }
            entry.value = inst;
            dict_apply_entry(&ctx->inst_ids, &entry);
        }
    }

    array_evalstr_free(&result_stack);
    array_astnodeptr_free(&stack_trace);
    array_astnodeptr_free(&inst_nodes);
    array_astnodeptr_free(&global_param_nodes);
    return EvalResSuccess;

return_failure:
    array_evalstr_free(&result_stack);
    array_astnodeptr_free(&stack_trace);
    array_astnodeptr_free(&inst_nodes);
    array_astnodeptr_free(&global_param_nodes);
    evalcontext_deinit(ctx);
    return EvalResFailure;
}

#ifdef RUN_TESTS

TEST_FUNC(eval_global_dep_iter)
{
    PageAlloc node_allocator = page_alloc_create(sizeof(ASTNode), 256);

    {
        const char* test_input =
            "<@def: a> <@b /> <@/def>\n"
            "<@def: b> <@c /> <@/def>\n"
            "<@def: c> <@f /> <@/def>\n"
            "<@def: d> <@f /> <@/def>\n"
            "<@def: e> <@a /><@b /><@c /><@d /> <@/def>\n"
            "<@def: f> F: LEAF :) <@/def>\n"
            "<@def: g> G: LEAF :) <@/def>\n";

        TokenList tokens = tokenize_string(test_input);
        ASTNode* root = ast_construct(tokens, &node_allocator);
        test_assert(root);

        Array_ASTNodePtr nodes = array_astnodeptr_create(16);
        for (ASTNode* node = root; node != NULL; node = node->next)
        {
            test_assert(node->type == ASTNodeParamDef);
            array_astnodeptr_push(&nodes, node);
        }

        EvalGlobalDepIter iter = {0};
        EvalResult result = eval_global_dep_iter_create(&nodes, &iter);
        test_assert(result == EvalResSuccess);
        test_assert(iter.graph.len == 7);

        ASTNode* next = NULL;
        EvalResult next_res = eval_global_dep_iter_next(&iter, &next);
        test_assert(next_res == EvalResSuccess && next && strcmp(next->param_def.ident.intern, "f") == 0);
        next_res = eval_global_dep_iter_next(&iter, &next);
        test_assert(next_res == EvalResSuccess && next && strcmp(next->param_def.ident.intern, "c") == 0);
        next_res = eval_global_dep_iter_next(&iter, &next);
        test_assert(next_res == EvalResSuccess && next && strcmp(next->param_def.ident.intern, "b") == 0);
        next_res = eval_global_dep_iter_next(&iter, &next);
        test_assert(next_res == EvalResSuccess && next && strcmp(next->param_def.ident.intern, "a") == 0);
        next_res = eval_global_dep_iter_next(&iter, &next);
        test_assert(next_res == EvalResSuccess && next && strcmp(next->param_def.ident.intern, "d") == 0);
        next_res = eval_global_dep_iter_next(&iter, &next);
        test_assert(next_res == EvalResSuccess && next && strcmp(next->param_def.ident.intern, "e") == 0);
        next_res = eval_global_dep_iter_next(&iter, &next);
        test_assert(next_res == EvalResSuccess && next && strcmp(next->param_def.ident.intern, "g") == 0);
        next_res = eval_global_dep_iter_next(&iter, &next);
        test_assert(next == NULL);
        test_assert(next_res == EvalResSuccess);

        eval_global_dep_iter_free(&iter);
        array_astnodeptr_free(&nodes);
        tokenlist_free(&tokens);
    }

    { // Direct self dependency
        const char* test_input = "<@def: a> <@a /> <@/def>\n";

        TokenList tokens = tokenize_string(test_input);
        ASTNode* root = ast_construct(tokens, &node_allocator);
        test_assert(root);

        Array_ASTNodePtr nodes = array_astnodeptr_create(16);
        for (ASTNode* node = root; node != NULL; node = node->next)
        {
            test_assert(node->type == ASTNodeParamDef);
            array_astnodeptr_push(&nodes, node);
        }

        EvalGlobalDepIter iter = {0};
        EvalResult result = eval_global_dep_iter_create(&nodes, &iter);
        test_assert(result == EvalResSuccess);
        test_assert(iter.graph.len == 1);

        ASTNode* next = NULL;
        TEST_DISABLE_OUT_STREAM(errdup, stderr);
        EvalResult next_res = eval_global_dep_iter_next(&iter, &next);
        TEST_RESTORE_OUT_STREAM(errdup, stderr);

        test_assert(next == NULL);
        test_assert(next_res == EvalResFailure);

        eval_global_dep_iter_free(&iter);
        array_astnodeptr_free(&nodes);
        tokenlist_free(&tokens);
    }

    { // Larger cycle
        // (b -> c -> d -> e -> b)
        const char* test_input =
            "<@def: a> A: LEAF :) <@/def>\n"
            "<@def: b> <@c /> <@/def>\n"
            "<@def: c> <@d /> <@/def>\n"
            "<@def: d> <@e /> <@/def>\n"
            "<@def: e> <@b /> <@/def>\n";

        TokenList tokens = tokenize_string(test_input);
        ASTNode* root = ast_construct(tokens, &node_allocator);
        test_assert(root);

        Array_ASTNodePtr nodes = array_astnodeptr_create(16);
        for (ASTNode* node = root; node != NULL; node = node->next)
        {
            test_assert(node->type == ASTNodeParamDef);
            array_astnodeptr_push(&nodes, node);
        }

        EvalGlobalDepIter iter = {0};
        EvalResult result = eval_global_dep_iter_create(&nodes, &iter);
        test_assert(result == EvalResSuccess);
        test_assert(iter.graph.len == 5);

        ASTNode* next = NULL;
        EvalResult next_res = eval_global_dep_iter_next(&iter, &next);
        test_assert(next_res == EvalResSuccess && next && strcmp(next->param_def.ident.intern, "a") == 0);

        TEST_DISABLE_OUT_STREAM(errdup, stderr);
        next_res = eval_global_dep_iter_next(&iter, &next);
        TEST_RESTORE_OUT_STREAM(errdup, stderr);
        test_assert(next == NULL);
        test_assert(next_res == EvalResFailure);

        eval_global_dep_iter_free(&iter);
        array_astnodeptr_free(&nodes);
        tokenlist_free(&tokens);
    }

    { // Undefined parameter
        const char* test_input = "<@def: a> <@undefined /> <@/def>\n";

        TokenList tokens = tokenize_string(test_input);
        ASTNode* root = ast_construct(tokens, &node_allocator);
        test_assert(root);

        Array_ASTNodePtr nodes = array_astnodeptr_create(16);
        for (ASTNode* node = root; node != NULL; node = node->next)
        {
            test_assert(node->type == ASTNodeParamDef);
            array_astnodeptr_push(&nodes, node);
        }

        EvalGlobalDepIter iter = {0};
        TEST_DISABLE_OUT_STREAM(errdup, stderr);
        EvalResult result = eval_global_dep_iter_create(&nodes, &iter);
        TEST_RESTORE_OUT_STREAM(errdup, stderr);
        test_assert(result == EvalResFailure);

        eval_global_dep_iter_free(&iter);
        array_astnodeptr_free(&nodes);
        tokenlist_free(&tokens);
    }

    { // Redefinition
        const char* test_input =
            "<@def: a> I'm A. <@/def>\n"
            "<@def: a> No, I'm A! <@/def>\n";

        TokenList tokens = tokenize_string(test_input);
        ASTNode* root = ast_construct(tokens, &node_allocator);
        test_assert(root);

        Array_ASTNodePtr nodes = array_astnodeptr_create(16);
        for (ASTNode* node = root; node != NULL; node = node->next)
        {
            test_assert(node->type == ASTNodeParamDef);
            array_astnodeptr_push(&nodes, node);
        }

        EvalGlobalDepIter iter = {0};
        TEST_DISABLE_OUT_STREAM(errdup, stderr);
        EvalResult result = eval_global_dep_iter_create(&nodes, &iter);
        TEST_RESTORE_OUT_STREAM(errdup, stderr);
        test_assert(result == EvalResFailure);

        eval_global_dep_iter_free(&iter);
        array_astnodeptr_free(&nodes);
        tokenlist_free(&tokens);
    }

    page_alloc_free(&node_allocator);

    return true;
}

TEST_FUNC(eval_top_level_passes)
{
    const char* test_input =
        "<@def: test_global1> TEST_GLOBAL1 includes <@test_global2 />! <@/def>"
        "<@in: template1>"
        "   <@def: FILE>im/a/path<@/def>"
        "   Hello, '<@test_global1 />'!"
        "<@/in>"
        "<@template: template1> <@content /> <@/template>"
        "<@def: test_global2> TEST_GLOBAL2 <@/def>";

    PageAlloc node_allocator = page_alloc_create(sizeof(ASTNode), 256);
    TokenList tokens = tokenize_string(test_input);
    ASTNode* root = ast_construct(tokens, &node_allocator);
    test_assert(root);

    EvalContext ctx = {0};
    EvalResult result = eval_top_level_passes(root, &ctx);

    test_assert(result == EvalResSuccess);
    test_assert(ctx.templates.num == 1);
    test_assert(ctx.global_param_stack.len == 2);

    EvalParam* p = NULL;
    InternedString intern = NULL;

    intern = interntable_intern_string(&g_interntable, "test_global1");
    p = evalparamstack_search(&ctx.global_param_stack, intern);
    test_assert(p);

    intern = interntable_intern_string(&g_interntable, "test_global2");
    p = evalparamstack_search(&ctx.global_param_stack, intern);
    test_assert(p);

    intern = interntable_intern_string(&g_interntable, "template1");
    p = evalparamstack_search(&ctx.global_param_stack, intern);
    test_assert(!p);

    test_assert(ctx.toplvlinsts.len == 1);
    EvalTopLvlInst* inst = array_evaltoplvlinst_ptr_safe(&ctx.toplvlinsts, 0);
    test_assert(inst->param_stack.len == 3); // 2 globals + 1 local

    intern = g_keywords[KeywordFILE];
    p = evalparamstack_search(&inst->param_stack, intern);
    test_assert(p);
    test_assert(p->strings.len == 1);
    test_assert(strncmp(p->strings.data[0].val, "im/a/path", strlen("im/a/path")) == 0);

    evalcontext_deinit(&ctx);

    tokenlist_free(&tokens);
    page_alloc_free(&node_allocator);

    return true;
}

TEST_FUNC(eval)
{
    PageAlloc node_allocator = page_alloc_create(sizeof(ASTNode), 256);
    Array_EvalString res_stack = array_evalstr_create(64);
    Array_EvalParam param_stack = array_evalparam_create(64);
    Array_ASTNodePtr stack_trace = array_astnodeptr_create(32);
    EvalContext empty_ctx = {0};
    evalcontext_init(&empty_ctx);

    { // Simple text
        const char* str = "Hello, World!";
        TokenList tokens = tokenize_string(str);
        ASTNode* node = NULL;
        jmp_buf eenv;
        if (setjmp(eenv) == 0)
        {
            node = parse_text_node(&eenv, &node_allocator, &tokens);
        }
        else
        {
            test_assert(!"parse error in eval test");
        }

        eval_text(&res_stack, node);

        test_assert(res_stack.len == 1);
        test_assert(res_stack.data[0].val == str);
        test_assert(res_stack.data[0].len == strlen(str));

        tokenlist_free(&tokens);
        array_evalstr_clear(&res_stack);
    }

    { // Undefined param
        const char* str = "<@test />";

        TokenList tokens = tokenize_string(str);
        ASTNode* node = NULL;
        jmp_buf eenv;
        if (setjmp(eenv) == 0)
        {
            node = parse_param_acc(&eenv, &node_allocator, &tokens);
        }
        else
        {
            test_assert(!"parse error in eval test");
        }

        TEST_DISABLE_OUT_STREAM(errdup, stderr);
        EvalResult res = eval_param_acc(&empty_ctx, &stack_trace, &res_stack, &param_stack, NULL, node);
        TEST_RESTORE_OUT_STREAM(errdup, stderr);

        test_assert(res == EvalResFailure);
        test_assert(stack_trace.len == 1);
        array_astnodeptr_clear(&stack_trace);

        tokenlist_free(&tokens);
    }

    { // Self-closing param
        const char* str =
            "<@def: test />"
            "<@test />";
        EvalContext ctx = {0};
        evalcontext_init(&ctx);

        TokenList tokens = tokenize_string(str);
        ASTNode* node = NULL;
        jmp_buf eenv;
        if (setjmp(eenv) == 0)
        {
            node = parse_param_def(&eenv, &node_allocator, &tokens, ParamDefLocationDefault);
            node->next = parse_param_acc(&eenv, &node_allocator, &tokens);
        }
        else
        {
            test_assert(!"parse error in eval test");
        }

        EvalResult res = eval_param_def(&empty_ctx, &stack_trace, &res_stack, &param_stack, NULL, node);
        test_assert(res == EvalResSuccess);
        res = eval_param_acc(&ctx, &stack_trace, &res_stack, &param_stack, NULL, node->next);
        test_assert(res == EvalResSuccess);

        test_assert(res_stack.len == 0);
        test_assert(stack_trace.len == 0);

        tokenlist_free(&tokens);
        array_evalstr_clear(&res_stack);
        evalparamstack_set_sp(&param_stack, 0);
    }

    { // Simple param
        const char* str =
            "<@def: test>Hallo, Welt!<@/def>"
            "<@test />";
        EvalContext ctx = {0};
        evalcontext_init(&ctx);

        TokenList tokens = tokenize_string(str);
        ASTNode* node = NULL;
        jmp_buf eenv;
        if (setjmp(eenv) == 0)
        {
            node = parse_param_def(&eenv, &node_allocator, &tokens, ParamDefLocationDefault);
            node->next = parse_param_acc(&eenv, &node_allocator, &tokens);
        }
        else
        {
            test_assert(!"parse error in eval test");
        }

        EvalResult res = eval_param_def(&empty_ctx, &stack_trace, &res_stack, &param_stack, NULL, node);
        test_assert(res == EvalResSuccess);
        res = eval_param_acc(&ctx, &stack_trace, &res_stack, &param_stack, NULL, node->next);
        test_assert(res == EvalResSuccess);

        test_assert(res_stack.len == 1);
        test_assert(res_stack.data[0].val == node->first_child->text.text.value);
        test_assert(res_stack.data[0].len == node->first_child->text.text.len);

        test_assert(stack_trace.len == 0);

        tokenlist_free(&tokens);
        array_evalstr_clear(&res_stack);
        evalparamstack_set_sp(&param_stack, 0);
    }

    { // Param default value
        const char* str =
            "<@def: test>Hallo, Welt!<@/def>"
            "<@test />"
            "<@undefined>Default :)<@/undefined>";

        TokenList tokens = tokenize_string(str);
        ASTNode* node = NULL;
        jmp_buf eenv;
        if (setjmp(eenv) == 0)
        {
            node = parse_param_def(&eenv, &node_allocator, &tokens, ParamDefLocationDefault);
            node->next = parse_param_acc(&eenv, &node_allocator, &tokens);
            node->next->next = parse_param_acc(&eenv, &node_allocator, &tokens);
        }
        else
        {
            test_assert(!"parse error in eval test");
        }

        EvalResult res = eval_param_def(&empty_ctx, &stack_trace, &res_stack, &param_stack, NULL, node);
        test_assert(res == EvalResSuccess);
        res = eval_param_acc(&empty_ctx, &stack_trace, &res_stack, &param_stack, NULL, node->next);
        test_assert(res == EvalResSuccess);
        res = eval_param_acc(&empty_ctx, &stack_trace, &res_stack, &param_stack, NULL, node->next->next);
        test_assert(res == EvalResSuccess);

        test_assert(res_stack.len == 2);
        test_assert(res_stack.data[0].val == node->first_child->text.text.value);
        test_assert(res_stack.data[0].len == node->first_child->text.text.len);
        test_assert(strncmp(res_stack.data[1].val, "Default :)", res_stack.data[1].len) == 0);

        test_assert(stack_trace.len == 0);

        tokenlist_free(&tokens);
        array_evalstr_clear(&res_stack);
        evalparamstack_set_sp(&param_stack, 0);
    }

    { // Inst
        const char* str =
            "<@template: template1>\n"
            "   <@def: a>Hello!<@/def>\n"
            "   <@CONTENT />\n"
            "   <@a /> <@testparam1 />\n"
            "   <@testparam1 />\n"
            "   <@global1 />\n"
            "<@/template>\n"
            "<@def: global1>GLOBAL<@/def>\n"
            "<@in: template1>\n"
            "   Hello, World!\n"
            "   <@def: testparam1>Hi!<@/def>\n"
            "<@/in>\n";

        TokenList tokens = tokenize_string(str);
        ASTNode* root = ast_construct(tokens, &node_allocator);
        test_assert(root);

        EvalContext ctx = {0};
        EvalResult result = eval_top_level_passes(root, &ctx);
        test_assert(result == EvalResSuccess);

        ASTNode* inst = root;
        while (inst->type != ASTNodeInst)
        {
            inst = inst->next;
        }
        test_assert(strcmp(inst->inst.template_ident.intern, "template1") == 0);

        u32 param_sp = ctx.global_param_stack.len;

        result = eval_inst(&ctx, &stack_trace, &res_stack, &ctx.global_param_stack, NULL, inst);
        test_assert(result == EvalResSuccess);

        const char* expanded_result = evalstrings_expand(res_stack.data, res_stack.len, NULL);
        test_assert(str_eq_ignore_ws(expanded_result, "Hello, World! Hello! Hi! Hi! GLOBAL"));
        free((void*)expanded_result);

        test_assert(ctx.global_param_stack.len == param_sp);
        test_assert(stack_trace.len == 0);

        evalcontext_deinit(&ctx);
        tokenlist_free(&tokens);
        array_evalstr_clear(&res_stack);
    }

    { // If
        const char* str =
            "<@template: template1>\n"
            "   <@CONTENT />\n"
            "<@/template>\n"
            "<@in: template1>\n"
            "   <@def: test1>TEST1<@/def>\n"
            "   <@if: test1>                A <@/if>\n"
            "   <@if: not test1>            B <@/if>\n"
            "   <@if: not test1=\"TEST2\">  C <@/if>\n"
            "   <@if: test1>                D <@else>   E <@/if>\n"
            "   <@if: undefined>            F <@else>   G <@/if>\n"
            "   <@if: not undefined>        H <@else>   I <@/if>\n"
            "   <@if: global1=\"GLOB*\">    J <@/if>\n"
            "   <@ref: ID=\"*\">\n"
            "       <@if: ref.test2>        K <@/if>\n"
            "       <@if: not ref.undefined>L <@/if>\n"
            "   <@/ref>\n"
            "   <@if: otherInst.test2=\"TEST*\">\n"
            "                               M\n"
            "   <@/if>\n"
            "   <@if: otherInst.undefined>  N <@else>   O <@/if>\n"
            "<@/in>\n"
            "<@in: template1>\n"
            "   <@def: ID>otherInst<@/def>\n"
            "   <@def: test2>TEST2<@/def>\n"
            "<@/in>\n"
            "<@def: global1>GLOBAL1<@/def>";

        TokenList tokens = tokenize_string(str);
        ASTNode* root = ast_construct(tokens, &node_allocator);
        test_assert(root);

        EvalContext ctx = {0};
        EvalResult result = eval_top_level_passes(root, &ctx);
        test_assert(result == EvalResSuccess);

        ASTNode* inst = root;
        while (inst->type != ASTNodeInst)
        {
            inst = inst->next;
        }
        test_assert(strcmp(inst->inst.template_ident.intern, "template1") == 0);

        u32 param_sp = ctx.global_param_stack.len;

        result = eval_inst(&ctx, &stack_trace, &res_stack, &ctx.global_param_stack, NULL, inst);
        test_assert(result == EvalResSuccess);

        const char* expanded_result = evalstrings_expand(res_stack.data, res_stack.len, NULL);
        test_assert(str_eq_ignore_ws(expanded_result, "A C D G H J K L M O"));
        free((void*)expanded_result);

        test_assert(ctx.global_param_stack.len == param_sp);
        test_assert(stack_trace.len == 0);

        evalcontext_deinit(&ctx);
        tokenlist_free(&tokens);
        array_evalstr_clear(&res_stack);
    }

    { // Ref
        const char* str =
            "<@in: template1>\n"
            "<@def: FILE>  test/index.html<@/def>\n"
            "Hello, Home!\n"
            "<@/in>\n"

            "<@in: template1>\n"
            "<@def: FILE>  test/about.html<@/def>\n"
            "Hello, About!\n"
            "<@/in>\n"

            "<@template: template1>\n"

            "<@ref: FILE='test/*'>\n"
            "LINK:<@ref.FILE />\n"
            "<@/ref>\n"

            "<@ref: undefined>\n"
            "I won't be showing up.\n"
            "<@/ref>\n"

            "<@CONTENT />\n"
            "<@/template>\n";

        TokenList tokens = tokenize_string(str);
        ASTNode* root = ast_construct(tokens, &node_allocator);
        test_assert(root);

        EvalContext ctx = {0};
        EvalResult result = eval_top_level_passes(root, &ctx);
        test_assert(result == EvalResSuccess);

        ASTNode* inst = root;
        test_assert(inst->type == ASTNodeInst);

        {
            u32 param_sp = ctx.global_param_stack.len;

            TEST_DISABLE_OUT_STREAM(errdup, stderr); // Silence non-matching ref warning
            result = eval_inst(&ctx, &stack_trace, &res_stack, &ctx.global_param_stack, NULL, inst);
            TEST_RESTORE_OUT_STREAM(errdup, stderr);
            test_assert(result == EvalResSuccess);

            const char* expanded_result = evalstrings_expand(res_stack.data, res_stack.len, NULL);
            test_assert(str_eq_ignore_ws(expanded_result, "LINK:test/index.html LINK:test/about.html Hello, Home!"));
            free((void*)expanded_result);

            test_assert(ctx.global_param_stack.len == param_sp);
            test_assert(stack_trace.len == 0);
            array_evalstr_clear(&res_stack);
        }

        inst = inst->next;
        test_assert(inst->type == ASTNodeInst);

        {
            u32 param_sp = ctx.global_param_stack.len;

            TEST_DISABLE_OUT_STREAM(errdup, stderr);
            result = eval_inst(&ctx, &stack_trace, &res_stack, &ctx.global_param_stack, NULL, inst);
            TEST_RESTORE_OUT_STREAM(errdup, stderr);
            test_assert(result == EvalResSuccess);

            const char* expanded_result = evalstrings_expand(res_stack.data, res_stack.len, NULL);
            test_assert(str_eq_ignore_ws(expanded_result, "LINK:test/index.html LINK:test/about.html Hello, About!"));
            free((void*)expanded_result);

            test_assert(ctx.global_param_stack.len == param_sp);
            test_assert(stack_trace.len == 0);
            array_evalstr_clear(&res_stack);
        }

        evalcontext_deinit(&ctx);
        tokenlist_free(&tokens);
    }

    { // Ref param not defined
        const char* str =
            "<@in: template1>\n"
            "<@def: FILE>index.html<@/def>"
            "Hello, Home!\n"
            "<@/in>\n"

            "<@template: template1>\n"

            "<@ref: FILE='*'>\n"
            "<@ref.undefined />\n"
            "<@/ref>\n"

            "<@CONTENT />\n"
            "<@/template>\n";

        TokenList tokens = tokenize_string(str);
        ASTNode* root = ast_construct(tokens, &node_allocator);
        test_assert(root);

        EvalContext ctx = {0};
        EvalResult result = eval_top_level_passes(root, &ctx);
        test_assert(result == EvalResSuccess);

        ASTNode* inst = root;
        test_assert(inst->type == ASTNodeInst);

        u32 param_sp = ctx.global_param_stack.len;

        TEST_DISABLE_OUT_STREAM(err_dup, stderr);
        result = eval_inst(&ctx, &stack_trace, &res_stack, &ctx.global_param_stack, NULL, inst);
        TEST_RESTORE_OUT_STREAM(err_dup, stderr);
        test_assert(result == EvalResFailure);

        // IMPORTANT: When eval fails, the outside caller has to clean up the stacks.  In this case
        // evalcontext_deinit does the trick since we didn't copy the the global param stack.
        test_assert(ctx.global_param_stack.len > param_sp);

        array_astnodeptr_clear(&stack_trace);
        array_evalstr_clear(&res_stack);
        evalcontext_deinit(&ctx);
        tokenlist_free(&tokens);
    }

    { // Markdown
        const char* str =
            "<@in: template1>\n"
            "<@def: FILE>index.html<@/def>\n"
            "<@def: title>Hello, title!<@/def>\n"
            "<@/in>\n"

            "<@template: template1>\n"

            "<@markdown>\n"
            "# <@title />\n"
            "Hello, world!\n"
            "[I'm a link](<@FILE />)\n"
            "<@/markdown>\n"

            "<@/template>\n";

        TokenList tokens = tokenize_string(str);
        ASTNode* root = ast_construct(tokens, &node_allocator);
        test_assert(root);

        EvalContext ctx = {0};
        EvalResult result = eval_top_level_passes(root, &ctx);
        test_assert(result == EvalResSuccess);

        ASTNode* inst = root;
        test_assert(inst->type == ASTNodeInst);

        u32 param_sp = ctx.global_param_stack.len;

        result = eval_inst(&ctx, &stack_trace, &res_stack, &ctx.global_param_stack, NULL, inst);
        test_assert(result == EvalResSuccess);

        test_assert(ctx.global_param_stack.len == param_sp);

        // for (u32 i = 0; i < res_stack.len; ++i)
        // {
        //     EvalString* s = array_evalstr_ptr_safe(&res_stack, i);
        //     fprintf(stderr, "%.*s", s->len, s->val);
        // }
        // fflush(stderr);

        const char* expanded_result = evalstrings_expand(res_stack.data, res_stack.len, NULL);
        test_assert(str_eq_ignore_ws(expanded_result, "<h1>Hello, title!</h1>"
                                     "<p>Hello, world! <a href=\"index.html\">I'm a link</a></p>"));
        free((void*)expanded_result);

        array_evalstr_clear(&res_stack);
        evalcontext_deinit(&ctx);
        tokenlist_free(&tokens);
    }

    array_astnodeptr_free(&stack_trace);
    assert(param_stack.len == 0);
    array_evalparam_free(&param_stack);
    assert(res_stack.len == 0);
    array_evalstr_free(&res_stack);
    page_alloc_free(&node_allocator);
    evalcontext_deinit(&empty_ctx);

    return true;
}
#endif
