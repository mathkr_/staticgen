static void PRINTF_FORMAT_ATT(3, 4)
token_msg(Token* t, LogSeverity sev, const char* format, ...)
{
    va_list args;
    va_start(args, format);
    io_highlight_line(sev,
                      format,
                      args,
                      t->file.path,
                      t->value - (t->column - 1),
                      t->line,
                      t->column);
    va_end(args);
}

static const char*
tokentype_get_desc(TokenType_ type)
{
        switch (type)
        {
            case TokenError:            return "token error";
            case TokenEof:              return "eof";
            case TokenTagOpen:          return "tag open";
            case TokenTagClose:         return "tag close";
            case TokenTextNode:         return "text node";
            case TokenIdent:            return "identifier";
            case TokenString:           return "string";
            case TokenForwardSlash:     return "/";
            case TokenEquals:           return "=";
            case TokenColon:            return ":";
            case TokenPeriod:           return ".";
            case TokenNewline:          return "newline";
            case TokenComment:          return "comment";
        }
        io_err("Unexpected token type value: 0x%08X", type);
        exit(EXIT_FAILURE);
}

static Tokenizer
tokenizer_create(File file)
{
    Tokenizer res = {0};
    res.file = file;
    res.p = file.buf;
    res.line = 1;
    res.column = 1;

    return res;
}

/**
 * Returns length of consecutive whitespace characters (excluding newlines) or 0.
 */
static u32
is_space(const char* p)
{
    const char* start = p;
    while (IS_SPACE_CHAR(p[0]))
    {
        ++p;
    }

    // Please don't feed me more than 4GBs worth of spaces
    return (u32)(p - start);
}

/**
 * Returns length of identifier or 0 if not an identifier.
 */
static u32
is_identifier(const char* p)
{
    const char* start = p;
    if (IS_ALPHA_CHAR(p[0]) || (p[0] == '_' && IS_ALPHA_CHAR(p[1])))
    {
        while (IS_ALPHA_CHAR(p[0])          ||
               (p[0] >= '0' && p[0] <= '9') ||
               (p[0] == '-')                ||
               (p[0] == '_'))
        {
            ++p;
        }
    }
    return (u32)(p - start);
}

/**
 * Returns length of delimited string, 0 if not a string or -1 if string is missing a closing delimiter.
 */
static i32
is_string(const char* p)
{
    const char* start = p;
    char delim = p[0];
    if (delim == '"' || delim == '\'')
    {
        ++p;

        while (!(p[0] == delim && p[-1] != '\\'))
        {
            if (!p[0] || IS_NEWLINE_CHAR(p[0]))
                return -1;

            ++p;
        }

        ++p;
    }
    return (i32)(p - start);
}

static inline void
tokenizer_single_char_token(Tokenizer* t, TokenType_ type, Token* token)
{
    token->type = type;
    token->value = t->p;
    token->len = 1;
    token->file = t->file;
    token->line = t->line;
    token->column = t->column;

    t->p += 1;
    t->column += 1;
}

static Token
tokenizer_next(Tokenizer* t)
{
    Token res = {0};

    if (!t->p[0])
    {
        res.type = TokenEof;
        res.value = t->p;
        res.len = 0;
        res.file = t->file;
        res.line = t->line;
        res.column = t->column;

        return res;
    }

    switch (t->state)
    {
        case TokOutsideTag:
        {
            res.value = t->p;
            res.file = t->file;
            res.line = t->line;
            res.column = t->column;

            if (strncmp(t->p, "<!--", 4) == 0)
            {
                t->p += 4;
                t->column += 4;

                while (true)
                {
                    if (!t->p[0])
                    {
                        res.type = TokenError;
                        res.len = 4;
                        token_msg(&res, LogSeverityError, "comment opened but never closed");
                        return res;
                    }
                    else if (strncmp(t->p, "-->", 3) == 0)
                    {
                        t->p += 3;
                        t->column += 3;
                        break;
                    }
                    else if (IS_NEWLINE_CHAR(t->p[0]))
                    {
                        t->column = 1;
                        ++t->line;
                        ++t->p;
                    }
                    else
                    {
                        ++t->p;
                        ++t->column;
                    }
                }

                res.type = TokenComment;
                assert(t->p >= res.value);
                res.len = (u32)(t->p - res.value);
                return res;
            }
            else
            {
                while (true)
                {
                    if (!t->p[0])
                    {
                        break;
                    }
                    else if (strncmp(t->p, "<@", 2) == 0)
                    {
                        t->state = TokInsideTag;
                        break;
                    }
                    else if (strncmp(t->p, "<!--", 4) == 0)
                    {
                        break;
                    }
                    else if (IS_NEWLINE_CHAR(t->p[0]))
                    {
                        t->column = 1;
                        ++t->line;
                        ++t->p;
                    }
                    else
                    {
                        ++t->p;
                        ++t->column;
                    }
                }

                res.type = TokenTextNode;
                assert(t->p >= res.value);
                res.len = (u32)(t->p - res.value);
                return res;
            }
        } break;

        case TokInsideTag:
        {
            u32 space_len = is_space(t->p);
            t->p += space_len;
            t->column += space_len;

            if (strncmp(t->p, "<@", 2) == 0)
            {
                res.type = TokenTagOpen;
                res.value = t->p;
                res.len = 2;
                res.file = t->file;
                res.line = t->line;
                res.column = t->column;

                t->p += res.len;
                t->column += res.len;

                return res;
            }

            if (t->p[0] == '>')
            {
                tokenizer_single_char_token(t, TokenTagClose, &res);
                t->state = TokOutsideTag;
                return res;
            }

            if (IS_NEWLINE_CHAR(t->p[0]))
            {
                res.type = TokenNewline;
                res.value = t->p;
                res.len = 1;
                res.file = t->file;
                res.line = t->line;
                res.column = t->column;

                t->p += 1;
                t->line += 1;
                t->column = 1;

                return res;
            }

            u32 identifier_len = is_identifier(t->p);
            if (identifier_len)
            {
                res.type = TokenIdent;
                res.value = t->p;
                res.len = identifier_len;
                res.file = t->file;
                res.line = t->line;
                res.column = t->column;

                res.intern = interntable_intern_stringn(&g_interntable, res.value, res.len);

                t->p += identifier_len;
                t->column += identifier_len;

                return res;
            }


            i32 string_len = is_string(t->p);
            if (string_len)
            {
                if (string_len == -1)
                {
                    res.type    = TokenError;
                    res.file    = t->file;
                    res.value   = t->p;
                    res.line    = t->line;
                    res.column  = t->column;
                    token_msg(&res, LogSeverityError, "string opened but never closed");
                    return res;
                }

                res.type = TokenString;
                res.value = t->p;
                res.len = (u32)string_len;
                res.file = t->file;
                res.line = t->line;
                res.column = t->column;

                t->p += string_len;
                t->column += (u32)string_len;

                return res;
            }

            if (t->p[0] == '=')
            {
                tokenizer_single_char_token(t, TokenEquals, &res);
                return res;
            }

            if (t->p[0] == '/')
            {
                tokenizer_single_char_token(t, TokenForwardSlash, &res);
                return res;
            }

            if (t->p[0] == ':')
            {
                tokenizer_single_char_token(t, TokenColon, &res);
                return res;
            }

            if (t->p[0] == '.')
            {
                tokenizer_single_char_token(t, TokenPeriod, &res);
                return res;
            }

            if (!t->p[0])
            {
                res.type = TokenEof;
                res.value = t->p;
                res.len = 0;
                res.file = t->file;
                res.line = t->line;
                res.column = t->column;

                return res;
            }
        } break;
    }

    res.type    = TokenError;
    res.file    = t->file;
    res.value   = t->p;
    res.line    = t->line;
    res.column  = t->column;
    token_msg(&res, LogSeverityError, "unexpected character/token");

    return res;
}

static inline void
tokenlist_inc(TokenList* list)
{
#ifndef NDEBUG
    if (list->t + 1 >= (list->tokens + list->num_tokens))
        IO_FATAL("incrementing token ptr past end of tokenlist");
#endif

    list->t += 1;
}

static inline void
tokenlist_advance(TokenList* list, i32 n)
{
#ifndef NDEBUG
    assert(n > 0);
    if ((list->t + n) >= (list->tokens + list->num_tokens))
        IO_FATAL("advancing token ptr past end of tokenlist");
#endif

    list->t += n;
}

static inline Token*
tokenlist_get(TokenList* list, i32 offset)
{
#ifndef NDEBUG
    assert(list->t >= list->tokens);
    if ((list->t + offset) >= (list->tokens + list->num_tokens))
    {
        IO_FATAL("attempting to read tokens past end of tokenlist");
    }
    else if ((list->t + offset) < list->tokens)
    {
        IO_FATAL("attempting to read tokens past start of tokenlist");
    }
#endif

    return list->t + offset;
}

static inline bool
tokenlist_match(TokenList* list, i32 offset, TokenType_ type)
{
    return (tokenlist_get(list, offset)->type == type);
}

static inline bool
tokenlist_match_value(TokenList* list, i32 offset, TokenType_ type, const char* value)
{
    Token* t = tokenlist_get(list, offset);
    return (t->type == type) && (strncmp(t->value, value, t->len) == 0);
}

static inline bool
tokenlist_match_ident(TokenList* list, i32 offset, InternedString intern)
{
    Token* t = tokenlist_get(list, offset);
    return (t->type == TokenIdent) && (t->intern == intern);
}

static void
tokenlist_free(TokenList* list)
{
    free(list->tokens);
    memset(list, 0, sizeof *list);
}

/**
 * Returns length of line comprised of only whitespace or 0 if not an empty line.
 *
 * @param line first character of a line
 */
static u32
is_empty_line(const char* line)
{
    const char* start = line;
    line += is_space(line);
    if (IS_NEWLINE_CHAR(line[0]))
    {
        // Empty line!
        return (u32)((line + 1) - start);
    }
    return 0;
}

static u32
is_empty_line_reverse(const char* next_line_start, const char* start_sentinel)
{
    const char* line = next_line_start;
    if (line > start_sentinel &&
        IS_NEWLINE_CHAR(line[-1]))
    {
        --line;
    }
    while (line > start_sentinel)
    {
        if (IS_NEWLINE_CHAR(line[-1]))
        {
            break;
        }
        else if (!IS_SPACE_CHAR(line[-1]))
        {
            // Line is not empty
            return 0;
        }

        --line;
    }
    assert(next_line_start >= line);
    return (u32)(next_line_start - line);
}

/**
 * Trim empty (only whitespace) lines off the start and the end of token value
 */
static void
trim_token(Token* token)
{
    const char* start = token->value;
    const char* end = token->value + token->len;

    u32 n;
    while (start < end && (n = is_empty_line(start)) > 0)
    {
        start += n;
    }

    while (start < end && (n = is_empty_line_reverse(end, start) > 0))
    {
        end -= n;
    }

    assert(start <= end);

    token->len = (u32)(end - start);
    token->value = start;
}

static void
trim_token_start(Token* token)
{
    const char* start = token->value;
    const char* end = token->value + token->len;

    u32 n;
    while (start < end && (n = is_empty_line(start)) > 0)
    {
        start += n;
    }

    assert(start <= end);

    token->len = (u32)(end - start);
    token->value = start;
}

static void
trim_token_end(Token* token)
{
    const char* start = token->value;
    const char* end = token->value + token->len;

    u32 n;
    while (start < end && (n = is_empty_line_reverse(end, start) > 0))
    {
        end -= n;
    }

    assert(start <= end);

    token->len = (u32)(end - start);
}

static void
tokenize_file(File file, Array_Token* sink, bool push_eof)
{
    Token token = {0};
    Tokenizer t = tokenizer_create(file);
    while (token = tokenizer_next(&t),
           token.type != TokenEof && token.type != TokenError)
    {
        if (token.type == TokenTextNode)
        {
            if (!token.len)
                continue;
        }
        else if (token.type == TokenComment)
        {
            if (g_config.preserve_comments)
                token.type = TokenTextNode;
            else
                continue;
        }
        array_token_push(sink, token);
    }
    if (token.type == TokenError || push_eof)
        array_token_push(sink, token);
}

static TokenList
tokenize(Array_File* files)
{
    Array_Token tokens = array_token_create(256);

    for (u32 i = 0; i < files->len; ++i)
    {
        File file = array_file_get_safe(files, i);
        tokenize_file(file, &tokens, (i == (files->len - 1)));
        if (array_token_get_safe(&tokens, tokens.len - 1).type == TokenError)
            exit(EXIT_FAILURE);
    }

    assert(array_token_get_safe(&tokens, tokens.len - 1).type == TokenEof);

    TokenList res = {
        .tokens     = tokens.data,
        .t          = tokens.data,
        .num_tokens = tokens.len,
    };
    return res;
}

static TokenList
tokenize_string(const char* str)
{
    File file = {0};
    file.path = "(string)";
    file.buf = (char*)str;

    Array_Token tokens = array_token_create(256);
    tokenize_file(file, &tokens, true);
    if (array_token_get_safe(&tokens, tokens.len - 1).type == TokenError)
        exit(EXIT_FAILURE);

    assert(tokens.data[tokens.len - 1].type == TokenEof);

    TokenList res = {
        .tokens     = tokens.data,
        .t          = tokens.data,
        .num_tokens = tokens.len,
    };
    return res;
}

#ifdef RUN_TESTS

static TokenList
tokenize_test(const char* str)
{
    File file = {0};
    file.path = "(string)";
    file.buf = (char*)str;

    Array_Token tokens = array_token_create(256);
    tokenize_file(file, &tokens, true);

    TokenList res = {
        .tokens     = tokens.data,
        .t          = tokens.data,
        .num_tokens = tokens.len,
    };
    return res;
}

static bool
toktest(const char* str, TokenType_ types[], i32 num_tokens)
{
    TEST_DISABLE_OUT_STREAM(errdup, stderr);
    TokenList list = tokenize_test(str);
    TEST_RESTORE_OUT_STREAM(errdup, stderr);
    for (i32 i = 0 ; i < MIN(num_tokens, (i32)list.num_tokens); ++i)
    {
        bool match = tokenlist_match(&list, i, types[i]);
        if (!match)
        {
            Token* mismatch = tokenlist_get(&list, i);
            fprintf(stderr, "tokentype mismatch. expected: %s, found: %s\n",
                    tokentype_get_desc(types[i]),
                    tokentype_get_desc(mismatch->type));
            token_msg(mismatch, LogSeverityError, "mismatch here");
            return false;
        }
    }
    if (list.num_tokens != (u32)num_tokens)
    {
        fprintf(stderr, "token list length mismatch. expected: %d, got: %u\n", num_tokens, list.num_tokens);
        fprintf(stderr, "in tokenized string: %s\n", str);
        return false;
    }
    tokenlist_free(&list);
    return true;
}

TEST_FUNC(tokenize)
{
    toktest("", (TokenType_[]){TokenEof}, 1);
    toktest("\n", (TokenType_[]){TokenTextNode, TokenEof}, 2);
    const char* random = "T3Wd8wYrB87Ivrp94T7H KAm0zpLtF5ll3d4xFAKY rvrvy6KMLerGaTV2TmQv i2I2SPvqf3cK9twJljCB qqQWmjpYY1IpEt4Usss6 5XQ6HTkImPGTKP8VQlq5 dmqonHb23sYDLDBPypMj GTvHyiTUlvZ5Wei7T8T9 esmvYnSSTcfkBY5LSq4W vuuPJlCGaNkYHlSWsb92";
    toktest(random, (TokenType_[]){TokenTextNode, TokenEof}, 2);
    toktest("<@", (TokenType_[]){TokenTagOpen, TokenEof}, 2);
    toktest(">", (TokenType_[]){TokenTextNode, TokenEof}, 2);
    toktest("<@>", (TokenType_[]){TokenTagOpen, TokenTagClose, TokenEof}, 3);
    toktest("<@ _>", (TokenType_[]){TokenTagOpen, TokenError}, 2);
    toktest("<@ test\n >", (TokenType_[]){TokenTagOpen, TokenIdent, TokenNewline, TokenTagClose, TokenEof}, 5);
    toktest("<@ test=\"\n >", (TokenType_[]){TokenTagOpen, TokenIdent, TokenEquals, TokenError}, 4);
    toktest("some<!-- COMMENT -->text<@def: test />",
            (TokenType_[]){TokenTextNode, TokenTextNode, TokenTagOpen, TokenIdent, TokenColon, TokenIdent, TokenForwardSlash, TokenTagClose, TokenEof}, 9);
    toktest("<@ 'ignore this delimiter: \\' '>", (TokenType_[]){TokenTagOpen, TokenString, TokenTagClose, TokenEof}, 4);
    toktest("<@ '\"\"\"\"'>", (TokenType_[]){TokenTagOpen, TokenString, TokenTagClose, TokenEof}, 4);
    toktest("\n\n\n<!--\n<@test />\n-->\n\n", (TokenType_[]){TokenTextNode, TokenTextNode, TokenEof}, 3);
    toktest("<@\t\t\t test >\t\t\t", (TokenType_[]){TokenTagOpen, TokenIdent, TokenTagClose, TokenTextNode, TokenEof}, 5);
    toktest("<@ test? >", (TokenType_[]){TokenTagOpen, TokenIdent, TokenError}, 3);
    toktest("<@ test-test >", (TokenType_[]){TokenTagOpen, TokenIdent, TokenTagClose, TokenEof}, 4);
    toktest("<!---->", (TokenType_[]){TokenEof}, 1);
    toktest("<!--", (TokenType_[]){TokenError}, 1);
    toktest("-->", (TokenType_[]){TokenTextNode, TokenEof}, 2);
    toktest("<!-- <@ test --> >test", (TokenType_[]){TokenTextNode, TokenEof}, 2);

    return true;
}

#endif
