#ifdef RUN_TESTS

static void init_interntable();

#define INVOKE_TEST(test_name) \
    fprintf(stderr, "> %s:\n", #test_name); \
    time_checkpoint = time_nano(); \
    result = CONCAT(test_, test_name)(); \
    test_time = time_sec_since(time_checkpoint); \
    num_errors += result ? 0 : 1; ++num_tests; \
    if (result) { fprintf(stderr,   "    OK             (%.2fms)\n", test_time*1000.0f); } \
    else { fprintf(stderr,          "    test failed    (%.2fms)\n", test_time*1000.0f); } \
    fflush(stderr)

i32
main(void)
{
    u32 num_tests = 0;
    u32 num_errors = 0;
    u64 time_checkpoint = 0;
    f32 test_time = 0;
    bool result;

    fprintf(stderr, "Running tests: \n\n");

    INVOKE_TEST(args);
    INVOKE_TEST(io_concat_paths);
    INVOKE_TEST(wildcard_matching);
    INVOKE_TEST(dict);
    INVOKE_TEST(dict2);
    INVOKE_TEST(string_interning);
    INVOKE_TEST(str_eq_ignore_ws);
    INVOKE_TEST(levenshtein_distance);
    INVOKE_TEST(jobqueue);
    INVOKE_TEST(align);

    init_interntable();

    INVOKE_TEST(tokenize);

    INVOKE_TEST(parser);

    INVOKE_TEST(eval_top_level_passes);
    INVOKE_TEST(eval);
    INVOKE_TEST(eval_global_dep_iter);

    if (!num_errors)
        fprintf(stderr, "\nAll tests passed.\n");
    else
        fprintf(stderr, "\n%u tests failed.\n", num_errors);

    exit((i32)num_tests);
}

#endif  // RUN_TESTS
