typedef enum {
    TokenError,
    TokenEof,
    TokenTagOpen,   // <@
    TokenTagClose,  // > closing an element
    TokenTextNode,
    TokenIdent,
    TokenString,
    TokenForwardSlash,
    TokenEquals,
    TokenColon,
    TokenPeriod,
    TokenNewline,
    TokenComment,

} TokenType_;

typedef struct {
    TokenType_      type;

    /**
     * Pointer into the file buf. NOT null terminated!
     */
    const char*     value;

    /**
     * NULL for all token types except TokenIdent
     */
    InternedString  intern;

    File            file;
    u32             len;
    u32             line;
    u32             column;

} Token;

// TAG: Array_Token Definition
#define ARRAY_ELEMENT_TYPE  Token
#define ARRAY_FUNC_PREFIX   token
#include "array_template.h"

typedef enum {
    TokOutsideTag,
    TokInsideTag,

} TokenizerState;

typedef struct {
    File            file;
    const char*     p;
    u32             line;
    u32             column;
    TokenizerState  state;

} Tokenizer;

/**
 * Convenience struct used to safely advance/match/expect tokens during parsing.
 */
typedef struct {
    Token*  tokens;
    u32     num_tokens;
    Token*  t;

} TokenList;
