typedef enum {
    EvalResSuccess  = 0,
    EvalResFailure,

} EvalResult;

typedef struct {
    const char* val;
    u32         len;

} EvalString;

// TAG: Array_EvalString Definition
#define ARRAY_ELEMENT_TYPE  EvalString
#define ARRAY_FUNC_PREFIX   evalstr
#include "array_template.h"

typedef struct {
    InternedString      intern;
    Array_EvalString    strings;
    Token               token;
    const char*         expanded_result;
    const char*         trimmed_result;     // In-Place trimmed expanded_result.
                                            // Does not need to be freed separately

} EvalParam;

// TAG: Array_EvalParam Definition
#define ARRAY_ELEMENT_TYPE  EvalParam
#define ARRAY_FUNC_PREFIX   evalparam
#include "array_template.h"

typedef struct {
    Array_EvalParam param_stack;
    ASTNode*        node;

} EvalTopLvlInst;

// TAG: Array_EvalTopLvlInst Definition
#define ARRAY_ELEMENT_TYPE  EvalTopLvlInst
#define ARRAY_FUNC_PREFIX   evaltoplvlinst
#include "array_template.h"

typedef struct {
    Dict                    templates;      // interned template ident -> template ASTNode*
    Dict                    inst_ids;       // inst id -> EvalTopLvlInst*
    Array_EvalTopLvlInst    toplvlinsts;
    Array_EvalParam         global_param_stack;
    Array_EvalString        allocated_strings;
    StringBuffer            stringbuf;      // Used for markdown conversion

} EvalContext;

typedef struct DepNode DepNode;
typedef DepNode* DepNodePtr;

// TAG: Array_DepNodePtr Definition
#define ARRAY_ELEMENT_TYPE  DepNodePtr
#define ARRAY_FUNC_PREFIX   depnodeptr
#include "array_template.h"

struct DepNode {
    ASTNode*            astnode;
    Array_DepNodePtr    edges;
    bool                visited;

};

// TAG: Array_DepNode Definition
#define ARRAY_ELEMENT_TYPE  DepNode
#define ARRAY_FUNC_PREFIX   depnode
#include "array_template.h"

// TODO: Maybe this better be just one function that returns an ordered array of ASTNode*'s ?
typedef struct {
    Array_DepNode       graph;
    Array_DepNodePtr    return_stack;
    DepNode*            next;

} EvalGlobalDepIter;
