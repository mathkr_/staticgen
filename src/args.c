static inline i32
arg_find_short_index(ArgDef* arguments, size_t argument_count, char c)
{
    for (u32 i = 0; i < argument_count; ++i)
    {
        if (c == arguments[i].nshort)
            return (i32)i;
    }

    return -1;
}

static inline i32
arg_find_long_index(ArgDef* arguments, size_t argument_count, const char* nlong)
{
    for (u32 i = 0; i < argument_count; ++i)
    {
        if (strcmp(nlong, arguments[i].nlong) == 0)
            return (i32)i;
    }

    return -1;
}

static bool
arg_is_alpha_char(char c)
{
    return  (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z');
}

static Args
args_read(i32 argc, const char** argv, ArgDef* arguments, size_t argument_count)
{
    Args res = {0};
    res.valid = true;
    res.arguments = arguments;
    res.count = argument_count;
    res.free_values = array_cstring_create(8);

    res.values = malloc((sizeof *res.values) * argument_count);
    memset(res.values, 0, (sizeof *res.values) * argument_count);

    // for (i32 i = 1; i < argc; ++i)
    // {
    //     fprintf(stderr, "debug arg[%d]: %s\n", i, argv[i]);
    // }
    // fflush(stderr);

    for (i32 i = 1; i < argc; ++i)
    {
        const char* curr  = argv[i];
        const char* next  = i < (argc - 1) ? argv[i + 1] : NULL;

        if (strncmp(curr, "--", 2) == 0)
        {
            i32 index = arg_find_long_index(arguments, argument_count, curr + 2);

            if (index == -1)
            {
                io_err("unknown commandline argument: %s", curr);
                res.valid = false;
                return res;
            }

            ArgDef* arg     = arguments + index;
            ArgValue* value = res.values + index;

            if (value->found)
            {
                if (arg->multi && !arg->flag)
                {
                    // Append a new value to our args value-linked-list
                    ArgValue** value_slot = &value->next;
                    while (*value_slot)
                    {
                        value_slot = &(*value_slot)->next;
                    }
                    assert(*value_slot == NULL);
                    *value_slot = malloc(sizeof **value_slot);
                    memset(*value_slot, 0, sizeof **value_slot);
                    value = *value_slot;
                }
                else
                {
                    io_err("commandline argument given more than once: %s", curr);
                    res.valid = false;
                    return res;
                }
            }
            else
            {
                value->found = true;
            }

            if (!arg->flag)
            {
                if ((!next) ||
                    (next[0] == '-' && arg_is_alpha_char(next[1])) ||
                    (strncmp(next, "--", 2) == 0) ||
                    (strlen(next) == 0))
                {
                    io_err("value expected for commandline argument: %s", curr);
                    res.valid = false;
                    return res;
                }

                value->value = next;
                ++i; // Skip next since it's our value
            }
        }
        else if (curr[0] == '-')
        {
            if (!strlen(curr + 1))
            {
                io_err("commandline flag(s) expected after \"-\"");
                res.valid = false;
                return res;
            }

            const char* p = curr + 1;
            while (p[0])
            {
                i32 index = arg_find_short_index(arguments, argument_count, p[0]);
                if (index == -1)
                {
                    io_err("unknown commandline flag: -%c", p[0]);
                    res.valid = false;
                    return res;
                }

                ArgDef* arg     = arguments + index;
                ArgValue* value = res.values + index;

                if (value->found)
                {
                    if (arg->multi && !arg->flag)
                    {
                        // Append a new value to our args value-linked-list
                        ArgValue** value_slot = &value->next;
                        while (*value_slot)
                        {
                            value_slot = &(*value_slot)->next;
                        }
                        assert(*value_slot == NULL);
                        *value_slot = malloc(sizeof **value_slot);
                        memset(*value_slot, 0, sizeof **value_slot);
                        value = *value_slot;
                    }
                    else
                    {
                        io_err("commandline argument given more than once: -%c (--%s)", arg->nshort, arg->nlong);
                        res.valid = false;
                        return res;
                    }
                }
                else
                {
                    value->found = true;
                }

                if (!arg->flag)
                {
                    if ((p[1] != '\0') ||
                        (!next) ||
                        (next[0] == '-' && arg_is_alpha_char(next[1])) ||
                        (strncmp(next, "--", 2) == 0) ||
                        (strlen(next) == 0))
                    {
                        io_err("value expected for commandline argument: -%c (--%s)", arg->nshort, arg->nlong);
                        res.valid = false;
                        return res;
                    }

                    value->value = next;
                    ++i; // Skip next since it's our value
                }

                ++p;
            }
        }
        else
        {
            // Free value
            array_cstring_push(&res.free_values, curr);
        }
    }

    return res;
}

static void
args_free(Args* args)
{
    array_cstring_free(&args->free_values);
    for (u32 i = 0; i < args->count; ++i)
    {
        ArgValue* v = args->values[i].next;
        while (v)
        {
            ArgValue* next = v->next;
            free(v);
            v = next;
        }
    }
    free(args->values);
    memset(args, 0, sizeof *args);
}

static const char*
args_get_value(Args* args, const char* nlong)
{
    for (u32 i = 0; i < args->count; ++i)
    {
        ArgDef* arg     = args->arguments + i;
        ArgValue* value = args->values + i;

        if (strcmp(nlong, arg->nlong) == 0)
        {
            assert(!arg->flag);

            if (!value->found)
                return NULL;
            else
                return value->value;
        }
    }

    IO_FATAL("argument not defined %s", nlong);
}

static ArgValue*
args_get_value_list(Args* args, const char* nlong)
{
    for (u32 i = 0; i < args->count; ++i)
    {
        ArgDef* arg     = args->arguments + i;
        ArgValue* value = args->values + i;

        if (strcmp(nlong, arg->nlong) == 0)
        {
            assert(!arg->flag);

            if (!value->found)
                return NULL;
            else
                return value;
        }
    }

    IO_FATAL("argument not defined %s", nlong);
}

static bool
args_has_arg(Args* args, const char* nlong)
{
    for (u32 i = 0; i < args->count; ++i)
    {
        ArgDef* arg     = args->arguments + i;
        ArgValue* value = args->values + i;

        if (strcmp(nlong, arg->nlong) == 0)
        {
            return value->found;
        }
    }

    IO_FATAL("argument not defined %s", nlong);
}


#ifdef RUN_TESTS

TEST_FUNC(args)
{
    {
        const char* argv[] = {
            "commandname",
            "FREE VALUE 0",
            "--test_value1",
            "VALUE test_value1",
            "--test_flag",
            "-ACBDE",
            "VALUE E",
            "FREE VALUE 1",
            "FREE VALUE 2",
            "--test_value2",
            "VALUE test_value2",
        };
        i32 argc = ARRAY_SIZE(argv);

        ArgDef arg_defs[] = {
            {.nlong = "test_value1",    .nshort = '\0', .flag = false},
            {.nlong = "test_value2",    .nshort = 'b',  .flag = false},
            {.nlong = "optional_value", .nshort = '\0', .flag = false},
            {.nlong = "optional_flag",  .nshort = 'd',  .flag = true},
            {.nlong = "test_flag",      .nshort = 'e',  .flag = true},
            {.nlong = "short_flag_A",   .nshort = 'A',  .flag = true},
            {.nlong = "short_flag_B",   .nshort = 'B',  .flag = true},
            {.nlong = "short_flag_C",   .nshort = 'C',  .flag = true},
            {.nlong = "short_flag_D",   .nshort = 'D',  .flag = true},
            {.nlong = "short_value_E",  .nshort = 'E',  .flag = false},
        };

        Args args = args_read(argc, argv, arg_defs, ARRAY_SIZE(arg_defs));

        const char* val;

        test_assert(args.valid);

        test_assert(args_has_arg(&args, "test_value1"));
        val = args_get_value(&args, "test_value1");
        test_assert(val && strcmp(val, "VALUE test_value1") == 0);

        test_assert(args_has_arg(&args, "test_value2"));
        val = args_get_value(&args, "test_value2");
        test_assert(val && strcmp(val, "VALUE test_value2") == 0);

        test_assert(!args_has_arg(&args, "optional_value"));
        test_assert(!args_has_arg(&args, "optional_flag"));

        test_assert(args_has_arg(&args, "test_flag"));
        test_assert(args_has_arg(&args, "short_flag_A"));
        test_assert(args_has_arg(&args, "short_flag_B"));
        test_assert(args_has_arg(&args, "short_flag_C"));
        test_assert(args_has_arg(&args, "short_flag_D"));

        test_assert(args_has_arg(&args, "short_value_E"));
        val = args_get_value(&args, "short_value_E");
        test_assert(val && strcmp(val, "VALUE E") == 0);

        test_assert(args.free_values.len == 3);
        test_assert(strcmp(array_cstring_get_safe(&args.free_values, 0), "FREE VALUE 0") == 0);
        test_assert(strcmp(array_cstring_get_safe(&args.free_values, 1), "FREE VALUE 1") == 0);
        test_assert(strcmp(array_cstring_get_safe(&args.free_values, 2), "FREE VALUE 2") == 0);

        args_free(&args);
        test_assert(args.values == NULL);
    }

    { // Short arg with value has to be the last char in a "flag string"
        const char* argv[] = {
            "commandname",
            "-ABC",
            "VALUE",
        };
        i32 argc = ARRAY_SIZE(argv);

        ArgDef arg_defs[] = {
            {.nlong = "test_flag1",     .nshort = 'A',  .flag = true},
            {.nlong = "test_value1",    .nshort = 'B',  .flag = false},
            {.nlong = "test_flag2",     .nshort = 'C',  .flag = true},
        };

        TEST_DISABLE_OUT_STREAM(dup_stderr, stderr);

        Args args = args_read(argc, argv, arg_defs, ARRAY_SIZE(arg_defs));

        TEST_RESTORE_OUT_STREAM(dup_stderr, stderr);

        test_assert(!args.valid);

        args_free(&args);
    }

    { // Multi values
        const char* argv[] = {
            "commandname",
            "-V",
            "A",
            "FREE VALUE",
            "--test_value1",
            "B",
            "-FV",
            "C",
            "FREE VALUE",
            "--test_value1",
            "D",
            "FREE VALUE",
        };
        i32 argc = ARRAY_SIZE(argv);

        ArgDef arg_defs[] = {
            {.nlong = "test_value1",    .nshort = 'V',  .multi = true},
            {.nlong = "test_flag1",     .nshort = 'F',  .flag = true},
        };

        Args args = args_read(argc, argv, arg_defs, ARRAY_SIZE(arg_defs));

        test_assert(args.valid);
        ArgValue* vlist = args_get_value_list(&args, "test_value1");
        test_assert(vlist && strcmp(vlist->value, "A") == 0);
        vlist = vlist->next;
        test_assert(vlist && strcmp(vlist->value, "B") == 0);
        vlist = vlist->next;
        test_assert(vlist && strcmp(vlist->value, "C") == 0);
        vlist = vlist->next;
        test_assert(vlist && strcmp(vlist->value, "D") == 0);
        test_assert(args_has_arg(&args, "test_flag1"));

        test_assert(args.free_values.len == 3);

        args_free(&args);
    }

    { // "-" has to be followed by a "flag string"
        const char* argv[] = {
            "commandname",
            "FREE VALUE",
            "-",
            "FREE VALUE",
        };
        i32 argc = ARRAY_SIZE(argv);

        ArgDef arg_defs[] = {
            {.nlong = "help",   .nshort = 'h',  .flag = true},
        };

        TEST_DISABLE_OUT_STREAM(dup_stderr, stderr);

        Args args = args_read(argc, argv, arg_defs, ARRAY_SIZE(arg_defs));

        TEST_RESTORE_OUT_STREAM(dup_stderr, stderr);

        test_assert(!args.valid);

        args_free(&args);
    }

    { // Duplicate arguments are disallowed
        const char* argv[] = {
            "commandname",
            "FREE VALUE",
            "-h",
            "--help",
        };
        i32 argc = ARRAY_SIZE(argv);

        ArgDef arg_defs[] = {
            {.nlong = "help",   .nshort = 'h',  .flag = true},
        };

        TEST_DISABLE_OUT_STREAM(dup_stderr, stderr);

        Args args = args_read(argc, argv, arg_defs, ARRAY_SIZE(arg_defs));

        TEST_RESTORE_OUT_STREAM(dup_stderr, stderr);

        test_assert(!args.valid);

        args_free(&args);
    }

    { // Unknown short arg
        const char* argv[] = {
            "commandname",
            "-x",
            "FREE VALUE",
            "--help",
        };
        i32 argc = ARRAY_SIZE(argv);

        ArgDef arg_defs[] = {
            {.nlong = "help",   .nshort = 'h',  .flag = true},
        };

        TEST_DISABLE_OUT_STREAM(dup_stderr, stderr);

        Args args = args_read(argc, argv, arg_defs, ARRAY_SIZE(arg_defs));

        TEST_RESTORE_OUT_STREAM(dup_stderr, stderr);

        test_assert(!args.valid);

        args_free(&args);
    }

    { // Unknown long arg
        const char* argv[] = {
            "commandname",
            "--unknown",
            "FREE VALUE",
        };
        i32 argc = ARRAY_SIZE(argv);

        ArgDef arg_defs[] = {
            {.nlong = "help",   .nshort = 'h',  .flag = true},
        };

        TEST_DISABLE_OUT_STREAM(dup_stderr, stderr);

        Args args = args_read(argc, argv, arg_defs, ARRAY_SIZE(arg_defs));

        TEST_RESTORE_OUT_STREAM(dup_stderr, stderr);

        test_assert(!args.valid);

        args_free(&args);
    }

    { // Disallow empty arg values
        const char* argv[] = {
            "commandname",
            "--test",
            "",
        };
        i32 argc = ARRAY_SIZE(argv);

        ArgDef arg_defs[] = {
            {.nlong = "test",   .nshort = '\0',  .flag = false},
        };

        TEST_DISABLE_OUT_STREAM(dup_stderr, stderr);

        Args args = args_read(argc, argv, arg_defs, ARRAY_SIZE(arg_defs));

        TEST_RESTORE_OUT_STREAM(dup_stderr, stderr);

        test_assert(!args.valid);

        args_free(&args);
    }

    return true;
}

#endif
