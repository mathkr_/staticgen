#if defined(__clang__) || defined(__GNUC__)

#define PRINTF_FORMAT_ATT(format_idx, format_args)          __attribute__((format(printf, format_idx, format_args)))
#define PRINTF_FORMAT_ATT_NORETURN(format_idx, format_args) __attribute__((format(printf, format_idx, format_args), noreturn))

#elif defined(_MSC_VER) && !defined(__INTEL_COMPILER)

#define PRINTF_FORMAT_ATT(format_idx, format_args)
#define PRINTF_FORMAT_ATT_NORETURN(format_idx, format_args) __declspec(noreturn)

#endif

typedef struct {
    const char* src;
    const char* dst;
} IOCopyJob;

// TAG: Array_IIOOCopyJob Definition
#define ARRAY_ELEMENT_TYPE  IOCopyJob
#define ARRAY_FUNC_PREFIX   iocopyjob
#include "array_template.h"


static inline void
PRINTF_FORMAT_ATT(3, 0) // Specify 0 when args cannot be checked
io_vprint(FILE* dest, const char* prefix, const char* format, va_list args)
{
    if (prefix)
        fprintf(dest, "%s: ", prefix);

    vfprintf(dest, format, args);
    fputc('\n', dest);

#if defined(_MSC_VER)
    fflush(dest);
#endif
}

static void PRINTF_FORMAT_ATT(1, 2)
io_log(const char* format, ...)
{
    va_list args;
    va_start(args, format);
    io_vprint(stdout, NULL, format, args);
    va_end(args);
}

static void PRINTF_FORMAT_ATT(1, 2)
io_verbose_log(const char* format, ...)
{
    if (!g_config.verbose)
        return;

    va_list args;
    va_start(args, format);
    io_vprint(stdout, NULL, format, args);
    va_end(args);
}

static void PRINTF_FORMAT_ATT(1, 2)
io_wrn(const char* format, ...)
{
    va_list args;
    va_start(args, format);
    io_vprint(stderr, log_severity_strings[LogSeverityWarn], format, args);
    va_end(args);
}

static void PRINTF_FORMAT_ATT(1, 2)
io_wrn_note(const char* format, ...)
{
    va_list args;
    va_start(args, format);
    io_vprint(stderr, log_severity_strings[LogSeverityWarnNote], format, args);
    va_end(args);
}

static void PRINTF_FORMAT_ATT(1, 2)
io_err(const char* format, ...)
{
    va_list args;
    va_start(args, format);
    io_vprint(stderr, log_severity_strings[LogSeverityError], format, args);
    va_end(args);
}

static void PRINTF_FORMAT_ATT(1, 2)
io_err_note(const char* format, ...)
{
    va_list args;
    va_start(args, format);
    io_vprint(stderr, log_severity_strings[LogSeverityErrorNote], format, args);
    va_end(args);
}

static void PRINTF_FORMAT_ATT_NORETURN(3, 4)
io_fatal(const char* file, i32 line, const char* format, ...)
{
    FILE* f = stderr;
    fprintf(f, "fatal error in %s:%d: ", file, line);

    va_list args;
    va_start(args, format);
    vfprintf(f, format, args);
    va_end(args);

    fprintf(f, "\n");
    fflush(f);

    // "Invoke" the debugger :-)
    i32 x = *(volatile i32*)(0);
    fprintf(f, "%d", x); // Making sure the read isn't optimized away
    abort();
}

static void
PRINTF_FORMAT_ATT(2, 0) // Specify 0 when args cannot be checked
io_highlight_line(LogSeverity severity,
                  const char* format,
                  va_list args,
                  const char* file,
                  const char* line_start,
                  u32 line,
                  u32 column)
{
    const char* line_p = line_start;
    while (line_p[0] && line_p[0] != '\r' && line_p[0] != '\n')
    {
        ++line_p;
    }
    assert(line_p >= line_start);
    u32 line_len = line_p - line_start;

    FILE* f = severity <= LogSeverityLog ? stdout : stderr;

    fprintf(f, "%s:%u:%u: %s: ", file, line, column, log_severity_strings[severity]);
    vfprintf(f, format, args);
    fprintf(f,"\n%.*s\n%*s^\n", line_len, line_start, column - 1, "");

#if defined(_MSC_VER)
    fflush(f);
#endif
}

static const char*
io_concat_paths(const char** paths, size_t num_paths)
{
    assert(num_paths);
    size_t total_len = num_paths; // num_paths - 1 ('/') + 1 ('\0')

    for (size_t i = 0; i < num_paths; ++i)
    {
        const char* path = paths[i];
        total_len += strlen(path);
    }

    char* res = malloc(total_len);
    char* p = res;

    for (size_t i = 0; i < num_paths; ++i)
    {
        const char* path = paths[i];
        if (i > 0 && path[0] == '/')
            ++path;

        size_t path_len = strlen(path);

        if (p > res && p[-1] != '/')
        {
            p[0] = '/';
            ++p;
        }

        memcpy(p, path, path_len);
        p += path_len;
    }

    p[0] = '\0';

    return res;
}

/**
 * Loads text file into memory.
 *
 * Resulting buffer gets null-terminated. Buffer gets allocated with malloc. \r\n gets turned into \n.
 *
 * @param path              path to file
 * @param out_file_size     gets set to file size in bytes (not including the added null-terminator)(can be NULL)
 *
 * @return returns pointer to the created buffer or NULL if an error occured.
 */
static char*
io_file_load_buf(const char* path, size_t* out_file_size)
{
    FILE* file = fopen(path, "rb");

    if (file)
    {
        fseek(file, 0L, SEEK_END);
        size_t size = (size_t)ftell(file);
        fseek(file, 0L, SEEK_SET);

        char* buffer = malloc(size + 1);
        if (!buffer)
        {
            io_err("allocation failure for file %s: %s", path, strerror(errno));
            fclose(file);
            return NULL;
        }

        size_t bytes_read = 0;
        while (bytes_read < size)
        {
            bytes_read += fread(buffer + bytes_read, 1, size, file);
            if (ferror(file))
            {
                io_err("error while reading file %s: %s", path, strerror(errno));
                free(buffer);
                fclose(file);
                return NULL;
            }
        }

        assert(bytes_read == size);
        fclose(file);

        buffer[size] = '\0';
        char* res = malloc(size + 1);

        char* dst = res;
        char* src = buffer;
        char* cr = NULL;
        while (cr = strchr(src, '\r'), cr != NULL)
        {
            size_t len = (size_t)(cr - src);
            memcpy(dst, src, len);
            dst += len;
            src = cr + 1;
        }
        strcpy(dst, src);
        free(buffer);

        if (out_file_size)
            *out_file_size = (size_t)(dst - res);
        return res;
    }
    else
    {
        io_err("could not open file %s: %s", path, strerror(errno));
        return NULL;
    }
}

static File
io_file_load(const char* path)
{
    File res = {0};
    res.path = path;
    res.buf = io_file_load_buf(path, NULL);
    if (!res.buf)
        exit(EXIT_FAILURE);

    return res;
}

#ifdef _MSC_VER

static IOStats
io_stat(const char* path)
{
    IOStats res = {0};

    struct _stat buf;
    i32 stat_result = _stat(path, &buf);
    if (stat_result == 0)
    {
        res.exists  = 1;
        res.size = (size_t)buf.st_size;

        if (buf.st_mode & _S_IFDIR ? 1 : 0)
            res.type = IOStatDir;
        else if (buf.st_mode & _S_IFREG ? 1 : 0)
            res.type = IOStatFile;
        else
            res.type = IOStatOther;
    }
    else if (errno != ENOENT)
    {
        res.stat_error = 1;
        res.type = IOStatError;
    }

    return res;
}

static inline bool
io_mkdir(const char* path)
{
    i32 result = _mkdir(path);
    return (result == 0 || errno == EEXIST);
}

// Not thread safe
static const char*
io_win32_strerror(DWORD error)
{
    static const char* last_err = NULL;

    if (last_err)
        LocalFree((HLOCAL)last_err);

    FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
                   NULL,
                   error,
                   MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                   (LPSTR)&last_err,
                   0,
                   NULL);
    return last_err;
}

static void
io_copy_dir_rec(const char* src,
                const char* dst,
                Array_IOCopyJob* jobs)
{
    if (!CreateDirectoryA(dst, NULL) && GetLastError() != ERROR_ALREADY_EXISTS)
    {
        io_err("could not create directory: %s", dst);
        return;
    }

    WIN32_FIND_DATA fd = {0};
    HANDLE hfind = INVALID_HANDLE_VALUE;

    const char* search_str = io_concat_paths((const char*[]){src, "*"}, 2);
    hfind = FindFirstFile(search_str, &fd);
    free((void*)search_str);

    if (hfind == INVALID_HANDLE_VALUE)
    {
        io_err("could not enumerate contents of directory: %s", src);
        return;
    }

    do
    {
        if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
        {
            if (strcmp(fd.cFileName, ".") == 0 ||
                strcmp(fd.cFileName, "..") == 0)
            {
                continue;
            }
            else
            {
                const char* src_child_path = io_concat_paths((const char*[]){src, fd.cFileName}, 2);
                const char* dst_child_path = io_concat_paths((const char*[]){dst, fd.cFileName}, 2);
                io_copy_dir_rec(src_child_path, dst_child_path, jobs);
                free((void*)src_child_path);
                free((void*)dst_child_path);
            }
        }
        else
        {
            IOCopyJob job;
            job.src = io_concat_paths((const char*[]){src, fd.cFileName}, 2);
            job.dst = io_concat_paths((const char*[]){dst, fd.cFileName}, 2);
            array_iocopyjob_push(jobs, job);
        }
    } while (FindNextFile(hfind, &fd) != 0);

    if (GetLastError() != ERROR_NO_MORE_FILES)
        io_err("error while enumerating contents of dir: %s", src);
    FindClose(hfind);
}

void
io_copy_job_func(void* userptr)
{
    IOCopyJob* job = userptr;
    if (!CopyFile(job->src, job->dst, false))
        io_err("could not copy file: %s", job->src);
}

static void
io_copy_dir(const char* src, const char* dst, JobQueue* jobqueue)
{
    Array_IOCopyJob copyjobs = array_iocopyjob_create(64);
    io_copy_dir_rec(src, dst, &copyjobs);

    if (copyjobs.len)
    {
        // At this point the directory structure should have been fully recreated in dst and we only
        // need to copy all the files over.

        io_verbose_log("copying %u static files", copyjobs.len);

        Array_Job jobs = array_job_create(copyjobs.len);
        Job job;
        job.func = io_copy_job_func;
        for (u32 i = 0; i < copyjobs.len; ++i)
        {
            job.userptr = array_iocopyjob_ptr_safe(&copyjobs, i);
            array_job_push(&jobs, job);
        }

        jobqueue_push_jobs(jobqueue, jobs.data, (i32)jobs.len);
        jobqueue_wait_until_finished(jobqueue);

        array_job_free(&jobs);
    }

    for (u32 i = 0; i < copyjobs.len; ++i)
    {
        IOCopyJob* job = array_iocopyjob_ptr_safe(&copyjobs, i);
        free((void*)job->src);
        free((void*)job->dst);
    }
    array_iocopyjob_free(&copyjobs);
}

static bool
io_rm_dir_contents(const char* path)
{
    const char* search_str = io_concat_paths((const char*[]){path, "*"}, 2);

    bool res = true;
    WIN32_FIND_DATA fd;
    HANDLE hfind = FindFirstFile(search_str, &fd);

    free((void*)search_str);

    if (hfind == INVALID_HANDLE_VALUE)
    {
        io_err("could not remove contents of directory \"%s\": %s", path, io_win32_strerror(GetLastError()));
        res = false;
        goto cleanup;
    }

    do
    {
        if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
        {
            if (strcmp(fd.cFileName, ".") == 0 ||
                strcmp(fd.cFileName, "..") == 0)
            {
                continue;
            }
            else
            {
                const char* child_path = io_concat_paths((const char*[]){path, fd.cFileName}, 2);
                res = io_rm_dir_contents(child_path);
                if (!res)
                {
                    free((void*)child_path);
                    goto cleanup;
                }
                else
                {
                    io_verbose_log("removing directory: %s", child_path);
                    res = RemoveDirectoryA(child_path);
                    free((void*)child_path);
                    if (!res)
                    {
                        io_err("could not remove directory \"%s\": %s", fd.cFileName, io_win32_strerror(GetLastError()));
                        goto cleanup;
                    }
                }
            }
        }
        else
        {
            const char* child_path = io_concat_paths((const char*[]){path, fd.cFileName}, 2);
            io_verbose_log("removing file:      %s", child_path);
            res = DeleteFileA(child_path);
            free((void*)child_path);
            if (!res)
            {
                io_err("could not remove file \"%s\": %s", fd.cFileName, io_win32_strerror(GetLastError()));
                goto cleanup;
            }
        }

    } while (FindNextFile(hfind, &fd) != 0);

    DWORD error = GetLastError();
    if (error != ERROR_NO_MORE_FILES)
    {
        io_err("could not remove contents of dir \"%s\": %s", path, io_win32_strerror(error));
        res = false;
    }

cleanup:
    if (hfind != INVALID_HANDLE_VALUE)
        FindClose(hfind);
    return res;
}

#else // _MSC_VER

static IOStats
io_stat(const char* path)
{
    IOStats res = {0};

    struct stat buf;
    i32 stat_result = stat(path, &buf);
    if (stat_result == 0)
    {
        res.exists  = 1;
        res.size    = (size_t)buf.st_size;
        switch (buf.st_mode & S_IFMT)
        {
            case S_IFREG:
            {
                res.type = IOStatFile;
            } break;
            case S_IFDIR:
            {
                res.type = IOStatDir;
            } break;
            default:
            {
                res.type = IOStatOther;
            } break;
        }
    }
    else if (errno != ENOENT)
    {
        res.type = IOStatError;
        res.stat_error = 1;
        io_err("file stat error: %s", strerror(errno));
    }

    return res;
}

static inline bool
io_mkdir(const char* path)
{
    i32 result = mkdir(path, 0777);
    return (result == 0 || errno == EEXIST);
}

static i32
io_nftw_rm(const char* path,
           PARAM_UNUSED const struct stat* sbuf,
           PARAM_UNUSED i32 type,
           PARAM_UNUSED struct FTW* ftwb)
{
    io_verbose_log("removing file/dir: %s", path);
    if (remove(path) != 0)
    {
        io_err("remove file/dir error: %s", strerror(errno));
        return -1;
    }
    return 0;
}

static bool
io_rm_dir_contents_recursive(const char* path)
{
    i32 nftw_result = nftw(path, io_nftw_rm, 10, FTW_DEPTH|FTW_MOUNT|FTW_PHYS);
    if (nftw_result != 0)
    {
        io_err("could not remove directory %s: %s", path, strerror(errno));
        return false;
    }
    return true;
}

static bool
io_unix_copy_file(const char* src_path, const char* dst_path)
{
    i32 srcfd = -1;
    i32 dstfd = -1;
    char* buf = NULL;

    srcfd = open(src_path, O_RDONLY);
    if (srcfd < 0)
        goto return_error;
    dstfd = open(dst_path, O_WRONLY|O_CREAT|O_TRUNC, 0666);
    if (dstfd < 0)
        goto return_error;

    size_t buf_size = MEGABYTES(2);
    buf = malloc(buf_size);
    ssize_t bytes_read;
    while ((bytes_read = read(srcfd, buf, buf_size)) > 0)
    {
        char* p = buf;
        while (bytes_read > 0)
        {
            ssize_t bytes_written = write(dstfd, buf, (size_t)bytes_read);
            if (bytes_written < 0 && errno != EINTR)
            {
                goto return_error;
            }
            else
            {
                bytes_read -= bytes_written;
                p += bytes_written;
            }
        }
    }

    if (bytes_read == 0)
    {
        close(srcfd);
        close(dstfd);
        free(buf);
        return true;
    }

return_error:
    io_err("error during copy file: %s", strerror(errno));
    if (buf)
        free(buf);
    if (srcfd != -1)
        close(srcfd);
    if (dstfd != -1)
        close(dstfd);
    return false;
}

typedef enum {
    CopyDirUnknown,
    CopyDirDir,
    CopyDirFile,

} CopyDirEntryType;

static bool
io_copy_dir_contents_recursive(const char* path, const char* dest)
{
    DIR* dir = NULL;
    if (!io_mkdir(dest))
    {
        io_err("could not create directory for copying: %s", dest);
        goto return_error;
    }

    dir = opendir(path);
    if (!dir)
    {
        io_err("could not open directory %s: %s",
               path, strerror(errno));
        goto return_error;
    }
    struct dirent* entry = NULL;

    while (errno = 0,
           entry = readdir(dir),
           entry != NULL)
    {
        if (strcmp(entry->d_name, ".") == 0 ||
            strcmp(entry->d_name, "..") == 0)
        {
            continue;
        }

        const char* child_path = io_concat_paths((const char*[]){path, entry->d_name}, 2);
        CopyDirEntryType type = CopyDirUnknown;
        switch (entry->d_type)
        {
            case DT_DIR: type = CopyDirDir;     break;
            case DT_REG: type = CopyDirFile;    break;
            case DT_UNKNOWN: // Some filesystems don't support d_type and
            {                // only return DT_UNKNOWN
                switch (io_stat(child_path).type)
                {
                    case IOStatDir:     type = CopyDirDir;  break;
                    case IOStatFile:    type = CopyDirFile; break;
                    default: break;
                }
            } break;
        }

        switch (type)
        {
            case CopyDirDir:
            {
                const char* child_dst_path =
                    io_concat_paths((const char*[]){dest, entry->d_name}, 2);
                bool child_res = io_copy_dir_contents_recursive(child_path, child_dst_path);
                free((void*)child_path);
                free((void*)child_dst_path);
                if (!child_res)
                    goto return_error;
            } break;
            case CopyDirFile:
            {
                const char* child_dst_path =
                    io_concat_paths((const char*[]){dest, entry->d_name}, 2);
                io_verbose_log("copying file: %s  ->  %s", child_path, child_dst_path);
                bool copy_res = io_unix_copy_file(child_path, child_dst_path);
                free((void*)child_path);
                free((void*)child_dst_path);
                if (!copy_res)
                    goto return_error;
            } break;
            default: break;
        }
    }

    if (errno != 0)
    {
        io_err("could not read contents of directory %s: %s",
               path, strerror(errno));
        goto return_error;
    }

    closedir(dir);
    return true;

return_error:
    if (dir)
        closedir(dir);
    return false;
}

#endif // else _MSC_VER

/**
 * Creates a directory and all its parent directories.
 *
 * @param path      path to a directory
 */
static inline bool
io_mkdir_recursive(char* path)
{
    char* p = path;
    if (p[0] == '/') // Skip root '/' on unix
        ++p;
    while (true)
    {
        while (p[0] && p[0] != '/')
        {
            ++p;
        }

        if (p[0] == '/')
        {
            p[0] = '\0';
            if (!io_mkdir(path))
            {
                io_err("could not create directory \"%s\"", path);
                p[0] = '/';
                return false;
            }
            p[0] = '/';
            ++p;
        }
        else
        {
            if (!io_mkdir(path))
            {
                io_err("could not create directory \"%s\"", path);
                return false;
            }

            assert(p[0] == '\0');
            break;
        }
    }

    return true;
}

/**
 * Creates a files parent directory and all its parent directories.
 *
 * @param path      path to a file
 */
static inline bool
io_mkdir_recursive_file(char* path)
{
    char* p = path;
    if (p[0] == '/') // Skip root '/' on unix
        ++p;
    while (true)
    {
        while (p[0] && p[0] != '/')
        {
            ++p;
        }

        if (p[0] == '/')
        {
            p[0] = '\0';
            if (!io_mkdir(path))
            {
                io_err("could not create directory \"%s\"", path);
                p[0] = '/';
                return false;
            }
            p[0] = '/';
            ++p;
        }
        else
        {
            // Last part is the file
            assert(p[0] == '\0');
            break;
        }
    }
    return true;
}


#ifdef RUN_TESTS

TEST_FUNC(io_concat_paths)
{
    {
        const char* paths[] = {
            "/",
            "/DIR1",
            "DIR2/",
            "/DIR3/",
            "DIR4",
            "somefile.txt",
        };
        const char* result = io_concat_paths(paths, ARRAY_SIZE(paths));
        test_assert(strcmp(result, "/DIR1/DIR2/DIR3/DIR4/somefile.txt") == 0);
        free((void*)result);
    }

    {
        const char* paths[] = {
            "SOME",
            "RELATIVE/",
            "/PATH/",
        };
        const char* result = io_concat_paths(paths, ARRAY_SIZE(paths));
        test_assert(strcmp(result, "SOME/RELATIVE/PATH/") == 0);
        free((void*)result);
    }

    {
        const char* paths[] = {
            "C:/",
            "/Windows/",
            "..",
            ".",
            "code/staticgen/out",
            "*",
        };
        const char* result = io_concat_paths(paths, ARRAY_SIZE(paths));
        test_assert(strcmp(result, "C:/Windows/.././code/staticgen/out/*") == 0);
        free((void*)result);
    }

    return true;
}

#endif
