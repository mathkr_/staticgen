typedef struct
{
    const char*     nlong;
    char            nshort; // May be '\0' which means no short name for that arg
    bool            flag;
    bool            multi;  // Arg may occur more than once with different values

} ArgDef;

typedef struct ArgValue ArgValue;
struct ArgValue {
    const char* value;
    bool        found;
    ArgValue*   next;
};

typedef struct
{
    ArgDef*         arguments;
    ArgValue*       values;
    size_t          count;
    Array_CString   free_values;
    bool            valid;

} Args;
