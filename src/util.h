typedef const char* CString;

// TAG: Array_CString Definition
#define ARRAY_ELEMENT_TYPE  CString
#define ARRAY_FUNC_PREFIX   cstring
#include "array_template.h"

// TAG: Array_u32 Definition
#define ARRAY_ELEMENT_TYPE  u32
#define ARRAY_FUNC_PREFIX   u32
#include "array_template.h"

//########################//
// STRINGBUFFER
//########################//

typedef struct {
    char*   base;
    size_t  cap;
    size_t  len;

} StringBuffer;

static StringBuffer stringbuf_create(size_t cap);
static void         stringbuf_free(StringBuffer* buf);
static void         stringbuf_clear(StringBuffer* buf);
static void         stringbuf_append(StringBuffer* buf, const char* str, size_t len);

/**
 * Returns the stringbuf as a null-terminated cstring.
 * IMPORTANT: This invalidates the stringbuf. Do not use it afterwards.
 */
static inline char* stringbuf_to_cstring(StringBuffer* buf);

static inline char* stringbuf_copy_to_cstring(StringBuffer* buf);

//########################//
// DYNBUF
//########################//

typedef struct {
    void*   elems;
    u32     cap;
    u32     len;
    u32     elem_size;

} DynBuf;

/**
 * Creates a new DynBuf.
 *
 * @param   elem_size   size of one element in bytes
 * @param   cap         initial number of elements allocated for
 */
static DynBuf       buf_create(size_t elem_size, u32 cap);
static void         buf_destroy(DynBuf* buf);
static inline void* buf_get(DynBuf* buf, u32 idx);
static void*        buf_append(DynBuf* buf, void* elems, u32 count);
static inline void* buf_push(DynBuf* buf, void* elem);

//########################//
// PAGE ALLOC
//########################//

typedef struct PAPage PAPage;
struct PAPage {
    PAPage* next;
    void*   elems;
};

typedef struct {
    PAPage*     first_page;
    PAPage*     last_page;

    size_t      page_cap;
    size_t      last_page_len;
    size_t      elem_size;

} PageAlloc;

static PageAlloc    page_alloc_create(size_t elem_size, size_t page_cap);
static void         page_alloc_free(PageAlloc* pa);
static inline void* page_alloc(PageAlloc* pa);

//########################//
// TIME
//########################//

#define NANOSECONDS_IN_SEC   1000000000ULL
#define NANOSECONDS_IN_MSEC  1000000ULL

static u64          time_nano();
static f32          time_nano_to_sec(u64 nano);
static f64          time_nano_to_msec(u64 nano);
static inline f32   time_sec_since(u64 nano);

//########################//
// HASH
//########################//

static inline u64 hash64str(const char* str);
static inline u32 hash32str(const char* str);

static inline u64 hash64(const char* str, size_t len);
static inline u32 hash32(const char* str, size_t len);

static inline u64 hash_ptr(void* ptr);

// https://en.wikipedia.org/wiki/Fowler_Noll_Vo_hash
#define FNV_OFFSET_BASIS    0xcbf29ce484222325LU
#define FNV64_PRIME         0x100000001b3LU

#define FNV32_OFFSET_BASIS  2166136261U
#define FNV32_PRIME         16777619U

static inline u64 hash_fnv1a64_null_terminated(const char* data);
static inline u64 hash_fnv1a64(const char* data, size_t len);
static inline u64 hash_fnv1a64_continue(const char* data, size_t len, u64 hash);
static inline u32 hash_fnv1a32_null_terminated(const char* data);
static inline u32 hash_fnv1a32(const char* data, size_t len);
static inline u32 hash_fnv1a32_continue(const char* data, size_t len, u32 hash);

//########################//
// DICT
//########################//

#if defined(_MSC_VER)
#define STRDUP(s) _strdup(s)
#else
#define STRDUP(s) strdup(s)
#endif

/**
 * Simple "string -> void*" table using open addressing and linear probing.
 * Capacity has to be a power of two.
 *
 * Every key is malloc'ed on entry.
 */
typedef struct {
    u32             num;
    u32             cap;
    const char**    keys;
    void**          values;

} Dict;

/**
 * Allocates storage for keys and values. Capacity has to be a power of two.
 */
static Dict    dict_create(u32 capacity);

/**
 * Deallocates all storage.
 */
static void    dict_free(Dict* map);

static void    dict_put(Dict* map, const char* key, void* value);
static void    dict_putn(Dict* map, const char* key, size_t keylen, void* value);

/**
 * Returns the value corresponding to key or NULL if key does not exist.
 * IMPORTANT: A NULL-value that has been inserted along with a key is indistinguishable
 * from a non-existant key-value pair with this function.
 *
 * If your table may contain NULL-values use dict_get_slot instead to check if your key exists.
 */
static void*   dict_get(Dict* map, const char* key);
static void*   dict_getn(Dict* map, const char* key, size_t keylen);

/**
 * Represents an entry of the dict to facilitate checking the dict for occurence of a key
 * and changing the value depending on the result "in one swoop" without the need to hash the key
 * and traverse the table more than once.
 *
 * IMPORTANT:
 * - Only make changes to the value field.
 * - Any call to a function that can change the table may invalidate the entry.
 * - Do not keep entries around.
 *
 * To apply the changes call dict_apply_entry.
 */
typedef struct {
    void*               value;
    const u32           idx;
    const char* const   key;
    const size_t        keylen; // Zero if key is null terminated
    const bool          present;

} DictEntry;

/**
 * Returns a DictEntry that is populated with values from the dict.
 * This is a read only function. To apply changes to the entry's value call dict_apply_entry.
 */
static DictEntry dict_get_entry(Dict* map, const char* key);
static DictEntry dict_get_entryn(Dict* map, const char* key, size_t keylen);

static void dict_apply_entry(Dict* map, DictEntry* entry);

//########################//
// STRING INTERN TABLE
//########################//

typedef const char* InternedString;

typedef struct {
    Dict strings;

} InternTable;

static InternTable      interntable_create(u32 init_capacity);
static InternedString   interntable_intern_stringn(InternTable* table, const char* str, size_t strlen);
static InternedString   interntable_intern_string(InternTable* table, const char* str);
