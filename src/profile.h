#ifdef RUN_PROFILING

typedef struct {
    const char* name;
    u64         time;
} ProfileStackEntry;

typedef struct {
    const char* name;
    u64         start_time;
    u64         end_time;
    i32         stack_level;
} ProfileEntry;

#define MAX_PROFILE_ENTRIES         8192
#define MAX_PROFILE_STACK_DEPTH     1024

typedef struct {
    ProfileEntry        entries[MAX_PROFILE_ENTRIES];
    i32                 num_entries;

    ProfileStackEntry   stack[MAX_PROFILE_STACK_DEPTH];
    i32                 sp;

} Profiler;

#define PROF_PUSH(name)     profile_push(name)
#define PROF_POP(name)      profile_pop(name)
#define PROF_PRINT()        profile_print()

#else

#define PROF_PUSH(name)
#define PROF_POP(name)
#define PROF_PRINT()

#endif
