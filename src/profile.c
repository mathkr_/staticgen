#ifdef RUN_PROFILING

static Profiler g_profiler;

static inline void
profile_push(const char* name)
{
    g_profiler.stack[g_profiler.sp++] = (ProfileStackEntry){
        .name = name,
        .time = time_nano(),
    };
}

static inline void
profile_pop(const char* name)
{
    u64 time = time_nano();
    ProfileStackEntry start = g_profiler.stack[--g_profiler.sp];
    if (strcmp(name, start.name) != 0)
        IO_FATAL("profile stack mismatch: expected '%s', found '%s'", start.name, name);
    g_profiler.entries[g_profiler.num_entries++] = (ProfileEntry){
        .name = name,
        .start_time = start.time,
        .end_time = time,
        .stack_level = g_profiler.sp,
    };
}

i32
profile_entry_cmp(const void* a, const void* b)
{
    const ProfileEntry* left = a;
    const ProfileEntry* right = b;

    i64 res = (i64)left->start_time - (i64)right->start_time;
    if (!res)
        res = (i64)right->end_time - (i64)left->end_time;
    return res;
}

static void
profile_print_indent(i32 n)
{
    for (i32 i = 0; i < n; ++i)
    {
        printf("|   ");
    }
}

static void
profile_print()
{
    if (!g_profiler.num_entries)
    {
        printf("no profile entries collected\n");
        return;
    }

    if (g_profiler.sp)
        IO_FATAL("profile stackpointer not zero in profile_print: %d", g_profiler.sp);

    qsort(g_profiler.entries, (size_t)g_profiler.num_entries, sizeof(ProfileEntry), profile_entry_cmp);
    u64 begin = g_profiler.entries[0].start_time;
    printf("profile:\n");
    for (i32 i = 0; i < g_profiler.num_entries; ++i)
    {
        ProfileEntry* e = g_profiler.entries + i;
        i32 indent = e->stack_level;

        profile_print_indent(indent);
        printf("name:  %-20s - start: %10.4fms - duration: %10.4fms\n",
               e->name,
               time_nano_to_msec(e->start_time - begin),
               time_nano_to_msec(e->end_time - e->start_time));
    }
}

#endif // RUN_PROFILING
