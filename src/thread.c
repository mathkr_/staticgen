#ifdef _WIN32

/*********************/
/******* MUTEX *******/
/*********************/

static inline Mutex
mutex_create()
{
    CRITICAL_SECTION* cs = malloc(sizeof(CRITICAL_SECTION));
    InitializeCriticalSection(cs);
    return cs;
}

static inline void
mutex_destroy(Mutex* mutex)
{
    DeleteCriticalSection(*mutex);
    free(*mutex);
}

static inline void
mutex_lock(Mutex* mutex)
{
    EnterCriticalSection(*mutex);
}

static inline bool
mutex_try_lock(Mutex* mutex)
{
    return TryEnterCriticalSection(*mutex);
}

static inline void
mutex_unlock(Mutex* mutex)
{
    LeaveCriticalSection(*mutex);
}

/**************************/
/******* SEMAPHORE  *******/
/**************************/

static inline Semaphore
semaphore_create(i32 initial_count)
{
    HANDLE res = CreateSemaphore(NULL, initial_count, MAXLONG, NULL);
    return res;
}

static inline void
semaphore_destroy(Semaphore* sem)
{
    CloseHandle(*sem);
}

static inline void
semaphore_wait(Semaphore* sem)
{
    WaitForSingleObject(*sem, INFINITE);
}

static inline bool
semaphore_try_wait(Semaphore* sem)
{
    return (WaitForSingleObject(*sem, 0) == WAIT_OBJECT_0);
}

static inline void
semaphore_signal(Semaphore* sem, i32 count)
{
    ReleaseSemaphore(*sem, count, NULL);
}

/*************************/
/******* JOBQUEUE  *******/
/*************************/

u32 __stdcall
jobqueue_internal_worker_loop(void* userptr)
{
    Job job;
    JobQueueWin32* queue = userptr;
    while(true)
    {
        // Wait until job is available
        semaphore_wait(&queue->jobs_available);

        // We have a job, lock the queue and take it.
        //
        // NOTE: If lots of jobs have just been added and all workers have been idle previously
        // there will be lots of contention. Switching to a lock-free queue could be the first point
        // of optimization here.
        mutex_lock(&queue->mutex);
        PARAM_UNUSED i32 pop_result = array_job_pop_safe(&queue->jobs, &job);
        mutex_unlock(&queue->mutex);
        assert(pop_result == RESULT_SUCCESS);

        // Do the job
        job.func(job.userptr);

        // Signal that we finished it
        semaphore_signal(&queue->jobs_finished, 1);
    }

    return 0;
}

static JobQueue*
jobqueue_create()
{
    // GetMaximumProcessorCount requires at least windows 7
    i32 num_threads = (i32)GetMaximumProcessorCount(ALL_PROCESSOR_GROUPS);
    if (!num_threads)
    {
        io_err("couldn't get number of logical processors (error code: 0x%X)", GetLastError());
        exit(EXIT_FAILURE);
    }

    JobQueueWin32* res = calloc(1, sizeof *res);
    res->mutex = mutex_create();
    mutex_lock(&res->mutex);
    res->jobs_available = semaphore_create(0);
    res->jobs_finished = semaphore_create(0);
    res->jobs = array_job_create(128);

    res->num_threads = num_threads;
    res->thread_handles = malloc((size_t)num_threads * sizeof(HANDLE));

    for (i32 i = 0; i < res->num_threads; ++i)
    {
        res->thread_handles[i] = _beginthreadex(NULL, 0, jobqueue_internal_worker_loop, res, 0, NULL);
    }
    mutex_unlock(&res->mutex);

    return res;
}

static void
jobqueue_push_jobs(JobQueue* queue, Job* jobs, i32 num_jobs)
{
    assert(num_jobs > 0);
    mutex_lock(&queue->mutex);
    array_job_append(&queue->jobs, jobs, (u32)num_jobs);
    queue->num_jobs_pushed += num_jobs;
    mutex_unlock(&queue->mutex);
    semaphore_signal(&queue->jobs_available, num_jobs);
}

static void
jobqueue_wait_until_finished(JobQueue* queue)
{
    while (queue->num_jobs_pushed)
    {
        semaphore_wait(&queue->jobs_finished);
        --queue->num_jobs_pushed;
    }
#ifndef NDEBUG
    mutex_lock(&queue->mutex);
    assert(queue->jobs.len == 0);
    mutex_unlock(&queue->mutex);
#endif
}

#else
#endif

#ifdef RUN_TESTS

#include <stdatomic.h>

static void
test_helper_sleep(i32 millis)
{
#ifdef WIN32
    Sleep((u32)millis);
#elif _POSIX_C_SOURCE >= 199309L
    struct timespec ts;
    ts.tv_sec = millis / 1000;
    ts.tv_nsec = (millis % 1000) * 1000000;
    nanosleep(&ts, NULL);
#else
    usleep(millis * 1000);
#endif
}

static void
test_helper_job_sleep(void* userptr)
{
    volatile i32* n = userptr;
    assert(*n != -1);
    // test_helper_sleep(*n);
    *n = -*n;
}

static void
test_helper_job_inc(void* userptr)
{
    volatile atomic_int* n = userptr;
    atomic_fetch_add(n, 1);
}

TEST_FUNC(jobqueue)
{
#define NUM_JOBS 512

    JobQueue* queue = jobqueue_create();

    volatile i32 ns[NUM_JOBS];
    volatile atomic_int n = 0;

    Job jobs[NUM_JOBS];

    /********/
    for (i32 i = 0; i < NUM_JOBS; ++i)
    {
        ns[i] = i;
        jobs[i].userptr = (void*)(ns + i);
        jobs[i].func = test_helper_job_sleep;
    }
    jobqueue_push_jobs(queue, jobs, NUM_JOBS);
    jobqueue_wait_until_finished(queue);

    for (i32 i = 0; i < NUM_JOBS; ++i)
        test_assert(ns[i] == -i);

    /********/
    for (i32 i = 0; i < NUM_JOBS; ++i)
        ns[i] = i;
    jobqueue_push_jobs(queue, jobs, NUM_JOBS);
    jobqueue_wait_until_finished(queue);

    for (i32 i = 0; i < NUM_JOBS; ++i)
        test_assert(ns[i] == -i);

    /********/
    for (i32 i = 0; i < NUM_JOBS; ++i)
        ns[i] = i;
    jobqueue_push_jobs(queue, jobs, NUM_JOBS);
    for (i32 i = 0; i < NUM_JOBS; ++i)
    {
        jobs[i].userptr = (void*)&n;
        jobs[i].func = test_helper_job_inc;
    }
    jobqueue_push_jobs(queue, jobs, NUM_JOBS);
    jobqueue_wait_until_finished(queue);

    test_assert(n == NUM_JOBS);
    for (i32 i = 0; i < NUM_JOBS; ++i)
        test_assert(ns[i] == -i);
    n = 0;

    /********/
    i32 iterations = 4;
    for (i32 i = 0; i < iterations; ++i)
        jobqueue_push_jobs(queue, jobs, NUM_JOBS);
    jobqueue_wait_until_finished(queue);

    test_assert(n == iterations * NUM_JOBS);
    // ns[..] shouldn't have changed, of course:
    for (i32 i = 0; i < NUM_JOBS; ++i)
        test_assert(ns[i] == -i);

    /********/
    return true;
#undef NUM_JOBS
}
#endif
