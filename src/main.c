#if defined(_MSC_VER)

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include <sys/types.h>  // _stat
#include <sys/stat.h>   // _stat
#include <direct.h>     // _mkdir
#include <process.h>    // _beginthreadex

#ifdef RUN_PROFILING
#include <search.h>
#endif

#endif // _MSC_VER

#include <stdbool.h>    // bool
#include <stdlib.h>     // exit, abort
#include <stdint.h>     // uintN_t, INTN_MAX
#include <assert.h>     // assert
#include <string.h>     // memcpy, strdup, strerror
#include <stdio.h>      // printf
#include <stdarg.h>     // va_start/end
#include <inttypes.h>   // PRIu64 macro
#include <setjmp.h>     // setjmp, longjmp

#ifdef __linux__

#include <sys/types.h>  // stat
#include <sys/stat.h>   // stat
#include <sys/unistd.h> // stat
#include <fcntl.h>      // open
#include <ftw.h>        // nftw
#include <dirent.h>     // opendir/readdir
#include <time.h>       // clock_gettime
#include <errno.h>      // strerror

#endif // __linux__

/**************************/
/**** THIRDPARTY STUFF ****/
/**************************/

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"

#include "md4c/md4c.h"
#include "md4c/md4c-html.h"
#include "md4c/entity.h"

#include "md4c/md4c.c"

#undef ISDIGIT
#undef ISLOWER
#undef ISUPPER
#undef ISALNUM

#include "md4c/md4c-html.c"
#include "md4c/entity.c"

#pragma GCC diagnostic pop

/*****************/

typedef uint8_t     u8;
typedef uint16_t    u16;
typedef uint32_t    u32;
typedef uint64_t    u64;

typedef int8_t      i8;
typedef int16_t     i16;
typedef int32_t     i32;
typedef int64_t     i64;

typedef float       f32;
typedef double      f64;

#define RESULT_SUCCESS  ((i32)0)
#define RESULT_FAILURE  ((i32)-1)

#define ARRAY_SIZE(x)   (sizeof((x))/sizeof((x[0])))

#define PARAM_UNUSED    __attribute__((unused))

// The expressions a and b can't have side effects or they will happen multiple times
#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#define MIN(a, b) (((a) < (b)) ? (a) : (b))

/**************************/
/**** PROJECT INCLUDES ****/
/**************************/

// We are using a "unity build"

#include "test.h"
#include "thread.h"
#include "io.h"
#include "util.h"
#include "token.h"
#include "parse.h"
#include "eval.h"
#include "args.h"
#include "config.h"

/**** GLOBALS ****/

static InternTable      g_interntable;
static InternedString   g_keywords[NumKeywords];

static Config           g_config = {

    .out_dir                = "out",
    .static_dir             = "static",
    .preserve_comments      = false,
    .verbose                = false,
    .clean_out_dir          = false,
    .copy_static_files      = true,

};

/*****************/

#include "profile.h"
#include "profile.c"

#include "thread.c"
#include "io.c"
#include "util.c"
#include "token.c"
#include "parse.c"
#include "eval.c"
#include "args.c"
#include "test.c"

/**************************/

static void
init_interntable()
{
    g_interntable = interntable_create(256);

    g_keywords[KeywordTemplate]     = interntable_intern_string(&g_interntable, "template");
    g_keywords[KeywordInst]         = interntable_intern_string(&g_interntable, "in");
    g_keywords[KeywordRef]          = interntable_intern_string(&g_interntable, "ref");
    g_keywords[KeywordDef]          = interntable_intern_string(&g_interntable, "def");
    g_keywords[KeywordMarkdown]     = interntable_intern_string(&g_interntable, "markdown");
    g_keywords[KeywordIf]           = interntable_intern_string(&g_interntable, "if");
    g_keywords[KeywordElse]         = interntable_intern_string(&g_interntable, "else");
    g_keywords[KeywordNot]          = interntable_intern_string(&g_interntable, "not");

    g_keywords[KeywordCONTENT]      = interntable_intern_string(&g_interntable, "CONTENT");
    g_keywords[KeywordFILE]         = interntable_intern_string(&g_interntable, "FILE");
    g_keywords[KeywordOUT_DIR]      = interntable_intern_string(&g_interntable, "OUT_DIR");
    g_keywords[KeywordSTATIC_DIR]   = interntable_intern_string(&g_interntable, "STATIC_DIR");
    g_keywords[KeywordID]           = interntable_intern_string(&g_interntable, "ID");

#ifndef NDEBUG
    for (i32 i = 0; i < NumKeywords; ++i)
        assert(g_keywords[i] != NULL);
#endif
}

#ifndef RUN_TESTS

i32
main(i32 argc, const char* argv[])
{
    PROF_PUSH("main");
    u64 start_time = time_nano();

    PARAM_UNUSED JobQueue* jobqueue = jobqueue_create();

    // Handle commandline args

    PROF_PUSH("args");
    ArgDef arg_defs[] = {
        {.nlong = "out-dir",            .nshort = 'O',  .flag = false},
        {.nlong = "static-dir",         .nshort = 'S',  .flag = false},
        {.nlong = "clean-out-dir",      .nshort = '\0', .flag = true},
        {.nlong = "no-static-files",    .nshort = '\0', .flag = true},
        {.nlong = "preserve-comments",  .nshort = 'c',  .flag = true},
        {.nlong = "verbose",            .nshort = 'v',  .flag = true},
        {.nlong = "help",               .nshort = 'h',  .flag = true},
        {.nlong = "version",            .nshort = 'V',  .flag = true},
    };

    Args args = args_read(argc, argv, arg_defs, ARRAY_SIZE(arg_defs));
    if (!args.valid)
    {
        io_err_note("see \"%s --help\" for commandline interface", argv[0]);
        exit(EXIT_FAILURE);
    }

    if (args_has_arg(&args, "help"))
    {
        printf("Usage: %s [OPTIONS] INPUT_FILES\n", argv[0]);
        printf("Options:\n");
        printf("    -O PATH, --out-dir PATH             sets output directory to PATH.\n");
        printf("                                        default: \"%s\"\n", g_config.out_dir);
        printf("\n");
        printf("    -S PATH, --static-dir PATH          sets static directory to PATH.\n");
        printf("                                        if it exists, its contents get copied into the\n");
        printf("                                        output directory before any output is generated.\n");
        printf("                                        default: \"%s\"\n", g_config.static_dir);
        printf("\n");
        printf("    --clean-out-dir                     deletes all files and directories inside\n");
        printf("                                        the output directory recursively before\n");
        printf("                                        generating any output.\n");
        printf("\n");
        printf("                                        IMPORTANT: use with caution and make sure\n");
        printf("                                        you set the correct output directory\n");
        printf("                                        to avoid unintended data loss.\n");
        printf("\n");
        printf("    --no-static-files                   don't copy files from the static directory\n");
        printf("                                        to the output directory\n");
        printf("\n");
        printf("                                        NOTE: you don't need to add this flag\n");
        printf("                                        if the set static directory does not exist.\n");
        printf("\n");
        printf("    -c, --preserve-comments             preserve html comments in input files.\n");
        printf("\n");
        printf("    -v, --verbose                       print more information during operations.\n");
        printf("\n");
        printf("    -h, --help                          print this help text.\n");
        printf("\n");
        printf("    -V, --version                       print program version.\n");
        exit(EXIT_SUCCESS);
    }

    if (args_has_arg(&args, "version"))
    {
        printf("%s %s\n", PROGRAM_NAME, SEMANTIC_VERSION);
        exit(EXIT_SUCCESS);
    }

    if (args.free_values.len == 0)
    {
        io_err("no input files supplied");
        io_err_note("see \"%s --help\" for commandline interface", argv[0]);
        exit(EXIT_FAILURE);
    }

    if (args_has_arg(&args, "clean-out-dir"))
    {
        g_config.clean_out_dir = true;
    }

    if (args_has_arg(&args, "verbose"))
    {
        g_config.verbose = true;
    }

    if (args_has_arg(&args, "no-static-files"))
    {
        g_config.copy_static_files = false;
    }

    {
        const char* out_dir = args_get_value(&args, "out-dir");
        if (out_dir)
            g_config.out_dir = out_dir;

        const char* static_dir = args_get_value(&args, "static-dir");
        if (static_dir)
            g_config.static_dir = static_dir;
    }
    PROF_POP("args");

    // Create interntable, intern keywords

    init_interntable();

    // Load input files

    PROF_PUSH("load files");
    Array_File input_files = array_file_create(8);
    for (u32 i = 0; i < args.free_values.len; ++i)
    {
        const char* path = array_cstring_get_safe(&args.free_values, i);
        array_file_push(&input_files, io_file_load(path));
    }
    assert(input_files.len); // "No input files" shouldn't go through args
    PROF_POP("load files");

    // Lex and parse

    PROF_PUSH("tokenize");
    PageAlloc node_allocator = page_alloc_create(sizeof(ASTNode), 512);
    TokenList tokens = tokenize(&input_files);
    PROF_POP("tokenize");

    PROF_PUSH("parse");
    ASTNode* ast_root = ast_construct(tokens, &node_allocator);
    if (!ast_root)
        exit(EXIT_FAILURE);
    PROF_POP("parse");

    PROF_PUSH("eval toplvl");
    EvalContext ctx = {0};
    EvalResult top_level_result = eval_top_level_passes(ast_root, &ctx);
    PROF_POP("eval toplvl");

    if (top_level_result != EvalResSuccess)
    {
        exit(EXIT_FAILURE);
    }

    { // Check if OUT_DIR or STATIC_DIR have been set in input files

        EvalParam* out_dir_param = evalparamstack_search(&ctx.global_param_stack, g_keywords[KeywordOUT_DIR]);
        if (out_dir_param)
        {
            if (!out_dir_param->trimmed_result)
                evalparam_expand(out_dir_param);
            if (args_get_value(&args, "out-dir"))
            {
                token_msg(&out_dir_param->token, LogSeverityWarn, "OUT_DIR param set here and in cmdline arguments");
                io_wrn_note("commandline argument takes precedence");
            }
            else
            {
                io_verbose_log("output directory set in file: %s", out_dir_param->trimmed_result);
                g_config.out_dir = out_dir_param->trimmed_result;
            }
        }

        EvalParam* static_dir_param = evalparamstack_search(&ctx.global_param_stack, g_keywords[KeywordSTATIC_DIR]);
        if (static_dir_param)
        {
            if (!static_dir_param->trimmed_result)
                evalparam_expand(static_dir_param);
            if (args_get_value(&args, "static-dir"))
            {
                token_msg(&static_dir_param->token, LogSeverityWarn, "STATIC_DIR param set here and in cmdline arguments");
                io_wrn_note("commandline argument takes precedence");
            }
            else
            {
                io_verbose_log("static directory set in file: %s", static_dir_param->trimmed_result);
                g_config.static_dir = static_dir_param->trimmed_result;
            }
        }
    }

    // TODO/FIXME: These file operations are slow as heck.
    // Check and maybe create/clean output directory

    switch (io_stat(g_config.out_dir).type)
    {
        case IOStatFile:
        {
            io_err("cannot use output directory: %s is a regular file", g_config.out_dir);
            io_err_note("remove the file or use \"--out-dir DIR\" to set a different directory");
            exit(EXIT_FAILURE);
        } break;

        case IOStatDir:
        {
            if (g_config.clean_out_dir)
            {
                PROF_PUSH("clean out-dir");
                io_verbose_log("cleaning output directory: %s", g_config.out_dir);
                if (!io_rm_dir_contents(g_config.out_dir))
                    exit(EXIT_FAILURE);
                PROF_POP("clean out-dir");
            }
        } break;

        case IOStatNoExist:
        {
            io_verbose_log("creating output directory: %s", g_config.out_dir);
            if (!io_mkdir_recursive((char*)g_config.out_dir))
                exit(EXIT_FAILURE);
        } break;

        case IOStatError:
        case IOStatOther:
        {
            io_err("cannot use output directory: %s", g_config.out_dir);
            exit(EXIT_FAILURE);
        } break;
    }

    // Check and maybe copy files from static dir to output dir

    if (g_config.copy_static_files)
    {
        switch (io_stat(g_config.static_dir).type)
        {
            case IOStatFile:
            {
                io_err("cannot use static directory: \"%s\" is a regular file", g_config.static_dir);
                io_err_note("remove the file or use \"--static-dir DIR\" to set a different directory");
                exit(EXIT_FAILURE);
            } break;

            case IOStatDir:
            {
                PROF_PUSH("copy static-dir");
                io_verbose_log("copying files from static dir \"%s\" to output dir \"%s\"",
                               g_config.static_dir, g_config.out_dir);
                io_copy_dir(g_config.static_dir, g_config.out_dir, jobqueue);
                PROF_POP("copy static-dir");
            } break;

            case IOStatNoExist:
            {
                io_verbose_log("static dir \"%s\" not found", g_config.static_dir);
            } break;

            case IOStatError:
            case IOStatOther:
            {
                io_err("cannot use static directory: %s", g_config.out_dir);
                exit(EXIT_FAILURE);
            } break;
        }
    }

    // Eval each top level instantiation and write output to file
    PROF_PUSH("eval");

    Dict out_path_table = dict_create(128);
    size_t num_files_generated = 0;

    Array_ASTNodePtr stack_trace = array_astnodeptr_create(32);
    Array_EvalString output = array_evalstr_create(256);
    Array_EvalParam param_stack = evalparamstack_copy(&ctx.global_param_stack);
    for (u32 i = 0; i < ctx.toplvlinsts.len; ++i)
    {
        PROF_PUSH("eval-loop");
        PROF_PUSH("prep path");
        EvalTopLvlInst* inst = array_evaltoplvlinst_ptr_safe(&ctx.toplvlinsts, i);
        EvalParam* file_param = evalparamstack_search(&inst->param_stack, g_keywords[KeywordFILE]);
        array_evalstr_clear(&output);

        if (!file_param)
        {
            token_msg(&inst->node->inst.template_ident,
                      LogSeverityWarn,
                      "no FILE parameter defined in top level instantiation");
            io_wrn_note("not generating output for that instantiation");
            continue;
        }

        if (!file_param->trimmed_result)
            evalparam_expand(file_param);

        if (!strlen(file_param->trimmed_result))
        {
            token_msg(&file_param->token, LogSeverityWarn,
                      "empty FILE parameter defined in top level instantiation");
            io_wrn_note("not generating output for that instantiation");
            continue;
        }

        const char* out_path = io_concat_paths((const char*[]){g_config.out_dir, file_param->expanded_result}, 2);

        DictEntry entry = dict_get_entry(&out_path_table, out_path);
        if (entry.present)
        {
            token_msg(&inst->node->inst.template_ident,
                      LogSeverityWarn,
                      "non-unique output file parameter in top level instantiation");
            io_wrn_note("overwriting file with last instantiation using that output file parameter");
            token_msg(&((EvalTopLvlInst*)entry.value)->node->inst.template_ident,
                      LogSeverityWarnNote,
                      "same output file parameter previously used in this top level instantiation");
        }
        else
        {
            ++num_files_generated;
        }
        entry.value = inst;
        dict_apply_entry(&out_path_table, &entry);
        PROF_POP("prep path");

        // Open file and write inst results

        PROF_PUSH("mkdir");
        io_mkdir_recursive_file((char*)out_path);
        PROF_POP("mkdir");

        FILE* file = fopen(out_path, "wb");
        if (!file)
        {
            token_msg(&file_param->token, LogSeverityWarn,
                      "could not open file for FILE parameter defined in top level instantiation");
            io_err_note("could not open file \"%s\" for writing: %s", out_path, strerror(errno));
            exit(EXIT_FAILURE);
        }

        PROF_PUSH("eval");
        EvalResult eval_result = eval_inst(&ctx, &stack_trace, &output, &param_stack, NULL, inst->node);
        if (eval_result != EvalResSuccess)
        {
            exit(EXIT_FAILURE);
        }
        PROF_POP("eval");

        PROF_PUSH("write");
        for (u32 j = 0; j < output.len; ++j)
        {
            EvalString* s = array_evalstr_ptr_safe(&output, j);

            size_t bytes_written = 0;
            while (bytes_written < s->len)
            {
                bytes_written += fwrite(s->val + bytes_written, 1, s->len - bytes_written, file);
                if (ferror(file))
                {
                    io_err("error during writing of file \"%s\"", out_path);
                    exit(EXIT_FAILURE);
                }
            }
        }

        fclose(file);
        free((void*)out_path);
        PROF_POP("write");
        PROF_POP("eval-loop");
    }

    PROF_POP("eval");

    io_log("%" PRIu64 " files generated", num_files_generated);
    io_log("ran in %.4fs", time_sec_since(start_time));
    PROF_POP("main");
    PROF_PRINT();
    exit(EXIT_SUCCESS);
}

#endif // RUN_TESTS
