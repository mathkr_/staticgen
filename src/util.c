//########################//
// STRINGBUFFER
//########################//

static StringBuffer
stringbuf_create(size_t cap)
{
    StringBuffer res = {0};
    res.base = malloc(cap);
    res.cap = cap;

    return res;
}

static void
stringbuf_free(StringBuffer* buf)
{
    free(buf->base);
    buf->base = NULL;
    buf->cap = buf->len = 0;
}

static void
stringbuf_clear(StringBuffer* buf)
{
    assert(buf->len < buf->cap);
    buf->len = 0;
}

static void
stringbuf_append(StringBuffer* buf, const char* str, size_t len)
{
    assert(buf->len < buf->cap);
    if ((buf->len + len + 1) > buf->cap)
    {
        buf->cap = MAX(buf->len + len + 1, 2 * buf->cap);
        buf->base = realloc(buf->base, buf->cap);
    }
    memcpy(buf->base + buf->len, str, len);
    buf->len += len;
}

static inline char*
stringbuf_to_cstring(StringBuffer* buf)
{
    assert(buf->len < buf->cap);
    buf->base[buf->len] = '\0';
    char* res = buf->base;
    buf->base = NULL;
    buf->cap = buf->len = 0;

    return res;
}

static inline char*
stringbuf_copy_to_cstring(StringBuffer* buf)
{
    assert(buf->len < buf->cap);
    buf->base[buf->len] = '\0';
    char* res = STRDUP(buf->base);
    return res;
}

//########################//

#define IS_ALPHA_CHAR(c)        (((c) >= 'a' && (c) <= 'z') || ((c) >= 'A' && (c) <= 'Z'))
#define IS_WHITESPACE_CHAR(c)   (((c) == ' ') || ((c) == '\t') || ((c) == '\n') || ((c) == '\r'))
#define IS_NEWLINE_CHAR(c)      ((c) == '\n')
#define IS_SPACE_CHAR(c)        ((c) == ' ' || (c) == '\t')
#define TO_LOWER_CHAR(c)        (((c) >= 'A' && (c) <= 'Z') ? ((c) + ('a' - 'A')) : (c))

static bool
str_is_identifier(const char* p)
{
    if (IS_ALPHA_CHAR(p[0]) || (p[0] == '_' && IS_ALPHA_CHAR(p[1])))
    {
        while (p[0])
        {
            if (IS_ALPHA_CHAR(p[0])         ||
               (p[0] >= '0' && p[0] <= '9') ||
               (p[0] == '-')                ||
               (p[0] == '_'))
            {
                ++p;
            }
            else
            {
                return false;
            }
        }
        return true;
    }
    return false;
}

/**
 * Levenshtein distance based on pseudo code from:
 * https://en.wikipedia.org/wiki/Levenshtein_distance (accessed on 2020/06/19)
 *
 * Note that this implementation is fairly inefficient and computing the distance
 * is generally not a lightweight operation.
 *
 */
static i32
str_levenshtein_ignorecase(const char* s, const char* t)
{
    i32 m = (i32)strlen(s);
    i32 n = (i32)strlen(t);

    i32* mem = malloc((size_t)(2 * (n + 1)) * sizeof(i32));

    i32* v0 = mem;
    i32* v1 = mem + (n + 1);

    for (i32 i = 0; i <= n; ++i)
        v0[i] = i;

    for (i32 i = 0; i <= m - 1; ++i)
    {
        v1[0] = i + 1;

        for (i32 j = 0; j <= n - 1; ++j)
        {
            i32 deletion_cost = v0[j + 1] + 1;
            i32 insertion_cost = v1[j] + 1;
            i32 substitution_cost = (TO_LOWER_CHAR(s[i]) == TO_LOWER_CHAR(t[j]))
                                    ? v0[j]
                                    : v0[j] + 1;

            v1[j + 1] = MIN(deletion_cost, MIN(insertion_cost, substitution_cost));
        }

        i32* tmp = v0;
        v0 = v1;
        v1 = tmp;
    }

    i32 res = v0[n];
    free(mem);
    return res;
}

#ifdef RUN_TESTS
TEST_FUNC(levenshtein_distance)
{
    test_assert(str_levenshtein_ignorecase("file", "FILE") == 0);
    test_assert(str_levenshtein_ignorecase("file", "xfile") == 1);
    test_assert(str_levenshtein_ignorecase("file", "xFILE") == 1);
    test_assert(str_levenshtein_ignorecase("kitten", "sitting") == 3);
    test_assert(str_levenshtein_ignorecase("itten", "kitten") == 1);
    test_assert(str_levenshtein_ignorecase("kitten", "kitten") == 0);
    test_assert(str_levenshtein_ignorecase("beer", "bar") == 2);
    test_assert(str_levenshtein_ignorecase("abcdef", "ghijkl") == 6);
    test_assert(str_levenshtein_ignorecase("abcdef", "ghijkl ") == 7);
    test_assert(str_levenshtein_ignorecase("", "levenshtein") == 11);
    test_assert(str_levenshtein_ignorecase("levenshtein", "") == 11);
    test_assert(str_levenshtein_ignorecase("levenshtein", "x") == 11);
    test_assert(str_levenshtein_ignorecase("", "") == 0);
    test_assert(str_levenshtein_ignorecase("\n", "\ntest") == 4);

    return true;
}
#endif

static bool
str_eq_ignore_ws(const char* a, const char* b)
{
    while (true)
    {
        while (IS_WHITESPACE_CHAR(a[0]))
        {
            ++a;
        }
        while (IS_WHITESPACE_CHAR(b[0]))
        {
            ++b;
        }

        if (!a[0] || !b[0])
        {
            return a[0] == b[0];
        }

        if ((a++)[0] != (b++)[0])
        {
            return false;
        }
    }
}

#ifdef RUN_TESTS
TEST_FUNC(str_eq_ignore_ws)
{
    test_assert(str_eq_ignore_ws("hello", "hello"));
    test_assert(str_eq_ignore_ws("", "hello") == false);
    test_assert(str_eq_ignore_ws("h e l l o", "h\ne\nll\to"));
    test_assert(str_eq_ignore_ws("hello", "hell") == false);
    test_assert(str_eq_ignore_ws("\n  \t hello", "hello  "));
    test_assert(str_eq_ignore_ws("\n  \t help", "hello  ") == false);

    return true;
}
#endif

static inline const char*
strdup_len(const char* str, size_t len)
{
    char* res = malloc(len + 1);
    memcpy(res, str, len);
    res[len] = '\0';
    return res;
}

/**
 * Trims str by writing '\0' after the last non-whitespace character and returning
 * the first non-whitespace character.
 */
static inline const char*
str_trim_in_place(char* str)
{
    while (IS_WHITESPACE_CHAR(str[0]))
    {
        ++str;
    }
    char* end = str + strlen(str);
    while (end > str && IS_WHITESPACE_CHAR(end[-1]))
    {
        --end;
    }
    end[0] = '\0';
    return str;
}

/**
 * Non-recursive wildcard string matching function.
 * '*' in the mstr means "match none, one or many arbitrary characters in the str"
 * '?' in the mstr means "match one arbitrary character in the str"
 * Other than that the strings must match exactly.
 *
 * @param str   string that is being matched by mstr. may contain the wildcard characters
 *              but they get treated just like any other character.
 * @param mstr  match string that can contain the wildcard characters '*' and '?'
 * @return      true if str matches the wildcard string mstr, false otherwise
 */
static bool
wildcard_match(const char* str, const char* mstr)
{
    const char* str_bookmark = NULL;
    const char* mstr_bookmark = NULL;

    while (true)
    {
        if (!str[0])
        {
            while (mstr[0] == '*')
            {
                ++mstr;
            }
            return mstr[0] == '\0';
        }
        else if (!mstr[0])
        {
            assert(str[0]);

            if (str_bookmark && str_bookmark[0])
            {
                assert(mstr_bookmark);
                str = ++str_bookmark;
                mstr = mstr_bookmark;
                continue;
            }

            // No more match string but string left -> no match
            return false;
        }
        else if (mstr[0] == '*')
        {
            while (mstr[0] == '*')
            {
                ++mstr;
            }

            // Bookmark and test how much of the str "*" should consume or whether
            // we can find a match at all. This is the crux of the algorithm.

            str_bookmark = str;
            mstr_bookmark = mstr - 1; // We skipped at least one "*" forward
        }
        else if (str[0] == mstr[0] || mstr[0] == '?')
        {
            ++str;
            ++mstr;
        }
        else
        {
            assert(str[0] && mstr[0]);
            assert(str[0] != mstr[0]);
            assert(mstr[0] != '*');

            if (str_bookmark && str_bookmark[0])
            {
                assert(mstr_bookmark);
                str = ++str_bookmark;
                mstr = mstr_bookmark;
                continue;
            }

            // Char mismatch and nothing left to try -> no match
            return false;
        }
    }
}

#ifdef RUN_TESTS

TEST_FUNC(wildcard_matching)
{
    test_assert(wildcard_match("TESTabcTESTdefTESTabcdefg", "*abc*def*g"));
    test_assert(wildcard_match("xyxzzxy", "x***y"));
    test_assert(wildcard_match("xyxzzxy", "x***x") == false);
    test_assert(wildcard_match("xyxzzxy", "*"));
    test_assert(wildcard_match("a12b34c", "a*b*c"));
    test_assert(wildcard_match("daaadabadmanda", "da*da*da*"));
    test_assert(wildcard_match("xy", "***x*****y***"));
    test_assert(wildcard_match("?????", "*"));
    test_assert(wildcard_match("?????", "?????"));
    test_assert(wildcard_match("AAAAA", "??????") == false);
    test_assert(wildcard_match("AAAAA", "AAAAA"));
    test_assert(wildcard_match("AAAAA", "AAAAAA") == false);
    test_assert(wildcard_match("simple/test/simple", "simple/*"));
    test_assert(wildcard_match("", ""));
    test_assert(wildcard_match("*", "") == false);
    test_assert(wildcard_match("**", "*"));
    test_assert(wildcard_match("", "*"));
    test_assert(wildcard_match("", "****"));
    test_assert(wildcard_match("where?", "*?"));
    test_assert(wildcard_match("where?", "where?"));
    test_assert(wildcard_match("/////???", "*?"));
    test_assert(wildcard_match("*********???", "*?"));

    test_assert(wildcard_match("abcccd", "*ccd"));
    test_assert(wildcard_match("mississipissippi", "*issip*ss*"));
    test_assert(wildcard_match("xxxx*zzzzzzzzy*f", "xxxx*zzy*fffff") == false);
    test_assert(wildcard_match("xxxx*zzzzzzzzy*f", "xxx*zzy*f"));
    test_assert(wildcard_match("xxxxzzzzzzzzyf", "xxxx*zzy*fffff") == false);
    test_assert(wildcard_match("xxxxzzzzzzzzyf", "xxxx*zzy*f"));
    test_assert(wildcard_match("xyxyxyzyxyz", "xy*z*xyz"));
    test_assert(wildcard_match("mississippi", "*sip*"));
    test_assert(wildcard_match("xyxyxyxyz", "xy*xyz"));
    test_assert(wildcard_match("mississippi", "mi*sip*"));
    test_assert(wildcard_match("ababac", "*abac*"));
    test_assert(wildcard_match("ababac", "*abac*"));
    test_assert(wildcard_match("aaazz", "a*zz*"));
    test_assert(wildcard_match("a12b12", "*12*23") == false);
    test_assert(wildcard_match("a12b12", "a12b") == false);
    test_assert(wildcard_match("a12b12", "*12*12*"));
    test_assert(wildcard_match("*", "*"));
    test_assert(wildcard_match("a*abab", "a*b"));
    test_assert(wildcard_match("a*r", "a*"));
    test_assert(wildcard_match("a*ar", "a*aar") == false);
    test_assert(wildcard_match("XYXYXYZYXYz", "XY*Z*XYz"));
    test_assert(wildcard_match("missisSIPpi", "*SIP*"));
    test_assert(wildcard_match("mississipPI", "*issip*PI"));
    test_assert(wildcard_match("xyxyxyxyz", "xy*xyz"));
    test_assert(wildcard_match("miSsissippi", "mi*sip*"));
    test_assert(wildcard_match("miSsissippi", "mi*Sip*") == false);
    test_assert(wildcard_match("abAbac", "*Abac*"));
    test_assert(wildcard_match("abAbac", "*Abac*"));
    test_assert(wildcard_match("aAazz", "a*zz*"));
    test_assert(wildcard_match("A12b12", "*12*23") == false);
    test_assert(wildcard_match("a12B12", "*12*12*"));
    test_assert(wildcard_match("oWn", "*oWn*"));
    test_assert(wildcard_match("bLah", "bLah"));
    test_assert(wildcard_match("bLah", "bLaH") == false);
    test_assert(wildcard_match("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab", "a*a*a*a*a*a*aa*aaa*a*a*b"));
    test_assert(wildcard_match("abababababababababababababababababababaacacacacacacacadaeafagahaiajakalaaaaaaaaaaaaaaaaaffafagaagggagaaaaaaaab", "*a*b*ba*ca*a*aa*aaa*fa*ga*b*"));
    test_assert(wildcard_match("abababababababababababababababababababaacacacacacacacadaeafagahaiajakalaaaaaaaaaaaaaaaaaffafagaagggagaaaaaaaab", "*a*b*ba*ca*a*x*aaa*fa*ga*b*") == false);
    test_assert(wildcard_match("abababababababababababababababababababaacacacacacacacadaeafagahaiajakalaaaaaaaaaaaaaaaaaffafagaagggagaaaaaaaab", "*a*b*ba*ca*aaaa*fa*ga*gggg*b*") == false);
    test_assert(wildcard_match("abababababababababababababababababababaacacacacacacacadaeafagahaiajakalaaaaaaaaaaaaaaaaaffafagaagggagaaaaaaaab", "*a*b*ba*ca*aaaa*fa*ga*ggg*b*"));
    test_assert(wildcard_match("aaabbaabbaab", "*aabbaa*a*"));
    test_assert(wildcard_match("a*a*a*a*a*a*a*a*a*a*a*a*a*a*a*a*a*", "a*a*a*a*a*a*a*a*a*a*a*a*a*a*a*a*a*"));
    test_assert(wildcard_match("aaaaaaaaaaaaaaaaa", "*a*a*a*a*a*a*a*a*a*a*a*a*a*a*a*a*a*"));
    test_assert(wildcard_match("aaaaaaaaaaaaaaaa", "*a*a*a*a*a*a*a*a*a*a*a*a*a*a*a*a*a*") == false);
    test_assert(wildcard_match("abc*abcd*abcde*abcdef*abcdefg*abcdefgh*abcdefghi*abcdefghij*abcdefghijk*abcdefghijkl*abcdefghijklm*abcdefghijklmn", "abc*abc*abc*abc*abc*abc*abc*abc*abc*abc*abc*abc*abc*abc*abc*abc*abc*") == false);
    test_assert(wildcard_match("abc*abcd*abcde*abcdef*abcdefg*abcdefgh*abcdefghi*abcdefghij*abcdefghijk*abcdefghijkl*abcdefghijklm*abcdefghijklmn", "abc*abc*abc*abc*abc*abc*abc*abc*abc*abc*abc*abc*"));
    test_assert(wildcard_match("abc*abcd*abcd*abc*abcd", "abc*abc*abc*abc*abc") == false);
    test_assert(wildcard_match("abc*abcd*abcd*abc*abcd*abcd*abc*abcd*abc*abc*abcd", "abc*abc*abc*abc*abc*abc*abc*abc*abc*abc*abcd"));
    test_assert(wildcard_match("abc", "********a********b********c********"));
    test_assert(wildcard_match("********a********b********c********", "abc") == false);
    test_assert(wildcard_match("abc", "********a********b********b********") == false);
    test_assert(wildcard_match("*abc*", "***a*b*c***"));

    // '?' wildcard specific tests:

    test_assert(wildcard_match("xx", "*?"));
    test_assert(wildcard_match("abcdefg", "*?"));
    test_assert(wildcard_match("AAAAA", "?????"));
    test_assert(wildcard_match("aabbccddeeff", "*?"));
    test_assert(wildcard_match("xyxzzxy", "x***x?"));
    test_assert(wildcard_match("a12b34c", "a?b?c") == false);
    test_assert(wildcard_match("bLah", "bL?h"));
    test_assert(wildcard_match("bLaaa", "bLa?") == false);
    test_assert(wildcard_match("bLah", "bLa?"));
    test_assert(wildcard_match("bLaH", "?Lah") == false);
    test_assert(wildcard_match("bLaH", "?LaH"));
    test_assert(wildcard_match("a", "??") == false);
    test_assert(wildcard_match("ab", "?*?"));
    test_assert(wildcard_match("ab", "*?*?*"));
    test_assert(wildcard_match("abc", "?**?*?"));
    test_assert(wildcard_match("abc", "?**?*&?") == false);
    test_assert(wildcard_match("abcd", "?b*??"));
    test_assert(wildcard_match("abcd", "?a*??") == false);
    test_assert(wildcard_match("abcd", "?**?c?"));
    test_assert(wildcard_match("abcd", "?**?d?") == false);
    test_assert(wildcard_match("abcde", "?*b*?*d*?"));
    test_assert(wildcard_match("a", "*?"));
    test_assert(wildcard_match("ab", "*?"));
    test_assert(wildcard_match("abc", "*?"));

    return true;
}

#endif

//########################//
// DYNBUF
//########################//

static DynBuf
buf_create(size_t elem_size, u32 cap)
{
    assert(cap > 0);
    assert(elem_size);
    assert(elem_size <= UINT32_MAX);

    DynBuf res = {0};
    res.elems = malloc(cap * elem_size);
    res.cap = cap;
    res.elem_size = (u32)elem_size;

    return res;
}

static void
buf_destroy(DynBuf* buf)
{
    free(buf->elems);
    memset(buf, 0, sizeof(DynBuf));
}

static inline void*
buf_get(DynBuf* buf, u32 idx)
{
    if (idx >= buf->len)
        IO_FATAL("out of bounds access in dynbuf (idx: %d)", idx);

    return ((u8*)buf->elems) + (idx * buf->elem_size);
}

static void*
buf_append(DynBuf* buf, void* elems, u32 count)
{
    if (buf->len + count > buf->cap)
    {
        u32 new_cap = buf->cap * 2;
        while (new_cap < buf->len + count) { new_cap *= 2; }
        buf->cap = new_cap;
        buf->elems = realloc(buf->elems, buf->cap * buf->elem_size);
    }

    void* res = memcpy(((u8*)buf->elems) + buf->len * buf->elem_size, elems, count * buf->elem_size);
    buf->len += count;

    return res;
}

static inline void*
buf_push(DynBuf* buf, void* elem)
{
    if (buf->len >= buf->cap)
    {
        buf->cap *= 2;
        buf->elems = realloc(buf->elems, buf->cap * buf->elem_size);
    }

    void* res = memcpy(((u8*)buf->elems) + buf->len * buf->elem_size, elem, buf->elem_size);
    ++buf->len;

    return res;
}

//########################//
// ALIGN
//########################//


static inline size_t
align(size_t base, size_t req)
{
    size_t mod = base % req;
    return (mod == 0) ? base : base + (req - mod);
}

#ifdef RUN_TESTS
TEST_FUNC(align)
{
    test_assert(align(12, 12) == 12);
    test_assert(align(24, 12) == 24);
    test_assert(align(11, 12) == 12);
    test_assert(align(23, 12) == 24);
    test_assert(align(1001, 2) == 1002);

    return true;
}
#endif

//########################//
// PAGE ALLOC
//########################//

static PAPage*
page_alloc_page_new(PageAlloc* pa)
{
    size_t elem_data_offset = align(sizeof(PAPage), 8);
    PAPage* res = malloc(elem_data_offset + pa->page_cap * pa->elem_size);
    res->next = NULL;
    res->elems = (u8*)res + elem_data_offset;

    return res;
}

static PageAlloc
page_alloc_create(size_t elem_size, size_t page_cap)
{
    PageAlloc res = {0};
    res.page_cap = page_cap;
    res.elem_size = elem_size;
    res.first_page = res.last_page = page_alloc_page_new(&res);

    return res;
}

static inline void*
page_alloc(PageAlloc* pa)
{
    if (pa->last_page_len >= pa->page_cap)
    {
        pa->last_page->next = page_alloc_page_new(pa);
        pa->last_page = pa->last_page->next;
        pa->last_page_len = 0;
    }

    return (u8*)pa->last_page->elems + pa->elem_size * pa->last_page_len++;
}

static void
page_alloc_free(PageAlloc* pa)
{
    PAPage* page = pa->first_page;
    do
    {
        PAPage* next = page->next;
        free(page);
        page = next;

    } while (page);
    memset(pa, 0, sizeof *pa);
}

//########################//
// TIME
//########################//

static inline u64
time_nano()
{
#if defined(_MSC_VER)
    LARGE_INTEGER li;
    QueryPerformanceCounter(&li);
    u64 res = (u64)li.QuadPart;
#else
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    u64 res = ((u64)ts.tv_sec * NANOSECONDS_IN_SEC) + (u64)ts.tv_nsec;
#endif
    return res;
}

static f32
time_nano_to_sec(u64 nano)
{
#if defined(_MSC_VER)
    LARGE_INTEGER li_freq;

#ifndef NDEBUG
    bool query_freq_works =
#endif
        QueryPerformanceFrequency(&li_freq);
    assert(query_freq_works);

    f64 freq = (f64)li_freq.QuadPart;
#else
    f64 freq = (f64)NANOSECONDS_IN_SEC;
#endif

    return (f32)((f64)nano / freq);
}

static f64
time_nano_to_msec(u64 nano)
{
#if defined(_MSC_VER)
    LARGE_INTEGER li_freq;

#ifndef NDEBUG
    bool query_freq_works =
#endif
        QueryPerformanceFrequency(&li_freq);
    assert(query_freq_works);

    f64 freq = (f64)li_freq.QuadPart / 1000.0;
#else
    f64 freq = (f64)NANOSECONDS_IN_SEC / 1000.0;
#endif

    return (f64)nano / freq;
}

static inline f32
time_sec_since(u64 nano)
{
    return time_nano_to_sec(time_nano() - nano);
}

//########################//
// HASH
//########################//

static inline u64
hash_fnv1a64_null_terminated(const char* data)
{
    u64 hash = FNV_OFFSET_BASIS;
    while (*data)
    {
        hash = hash ^ (u64)(*data++);
        hash = hash * FNV64_PRIME;
    }

    return hash;
}

static inline u64
hash_fnv1a64(const char* data, size_t len)
{
    u64 hash = FNV_OFFSET_BASIS;
    for (size_t i = 0; i < len; ++i)
    {
        hash = hash ^ (u64)(*data++);
        hash = hash * FNV64_PRIME;
    }

    return hash;
}

static inline u64
hash_fnv1a64_continue(const char* data, size_t len, u64 hash)
{
    for (size_t i = 0; i < len; ++i)
    {
        hash = hash ^ (u64)(*data++);
        hash = hash * FNV64_PRIME;
    }

    return hash;
}

static inline u32
hash_fnv1a32_null_terminated(const char* data)
{
    u32 hash = FNV32_OFFSET_BASIS;
    while (*data)
    {
        hash = hash ^ (u32)(*data++);
        hash = hash * FNV32_PRIME;
    }

    return hash;
}

static inline u32
hash_fnv1a32(const char* data, size_t len)
{
    u32 hash = FNV32_OFFSET_BASIS;
    for (size_t i = 0; i < len; ++i)
    {
        hash = hash ^ (u32)(*data++);
        hash = hash * FNV32_PRIME;
    }

    return hash;
}

static inline u32
hash_fnv1a32_continue(const char* data, size_t len, u32 hash)
{
    for (size_t i = 0; i < len; ++i)
    {
        hash = hash ^ (u32)(*data++);
        hash = hash * FNV32_PRIME;
    }

    return hash;
}

static inline u64
hash64str(const char* str)
{
    return hash_fnv1a64_null_terminated(str);
}

static inline u32
hash32str(const char* str)
{
    return hash_fnv1a32_null_terminated(str);
}

static inline u64
hash64(const char* str, size_t len)
{
    return hash_fnv1a64(str, len);
}

static inline u32
hash32(const char* str, size_t len)
{
    return hash_fnv1a32(str, len);
}

static inline u64
hash_ptr(void* ptr)
{
    u64 x = (u64)ptr;
    x *= 0xff51afd7ed558ccd;
    x ^= x >> 32;
    return x;
}

//########################//
// DICT
//########################//

static inline i32
popcount(u32 x)
{
    return __builtin_popcount(x);
}

static Dict
dict_create(u32 capacity)
{
    if (popcount(capacity) != 1)
        IO_FATAL("non-power-of-two in dict initialization");

    capacity = MAX(capacity, 32);

    Dict res;
    res.num = 0;
    res.cap = capacity;
    res.keys = calloc(res.cap, sizeof(const char*) + sizeof(void*));
    res.values = (void**)(res.keys + res.cap);

    return res;
}

static void
dict_free_values(Dict* map)
{
    for (u32 i = 0; i < map->cap; i++)
    {
        if (map->keys[i])
        {
            free((void*)map->keys[i]);
            free(map->values[i]);
        }
    }
    free(map->keys);
    memset(map, 0, sizeof(Dict));
}

static void
dict_free(Dict* map)
{
    for (u32 i = 0; i < map->cap; i++)
    {
        if (map->keys[i])
            free((void*)map->keys[i]);
    }
    free(map->keys);
    memset(map, 0, sizeof(Dict));
}

static void
dict_grow(Dict* map)
{
    assert(map->cap < UINT32_MAX / 2);

    u32 new_cap = 2 * map->cap;

    Dict new_map;
    new_map.num = map->num;
    new_map.cap = new_cap;
    new_map.keys = calloc(new_map.cap, sizeof(const char*) + sizeof(void*));
    new_map.values = (void**)(new_map.keys + new_map.cap);

    for (u32 i = 0; i < map->cap; i++)
    {
        if (map->keys[i])
        {
            u64 hash = hash64str(map->keys[i]);
            u32 index = hash & (new_map.cap - 1);
            while (true)
            {
                if (new_map.keys[index] == NULL)
                {
                    new_map.keys[index] = map->keys[i];
                    new_map.values[index] = map->values[i];
                    break;
                }

                index = (index + 1) & (new_map.cap - 1);
            }
        }
    }
    free(map->keys);
    *map = new_map;
}

static void
dict_put_hashed(Dict* map, const char* key, void* value, u64 hash)
{
    if (2*map->num >= map->cap)
        dict_grow(map);

    u32 mask = map->cap - 1;
    u32 index = hash & mask;
    while (true)
    {
        if (map->keys[index] == NULL)
        {
            map->keys[index] = STRDUP(key);
            map->values[index] = value;
            ++map->num;
            return;
        }
        else if (strcmp(map->keys[index], key) == 0)
        {
            map->values[index] = value;
            return;
        }

        index = (index + 1) & mask;
    }
}

static void**
dict_get_hashed(Dict* map, const char* key, u64 hash)
{
    u32 mask = map->cap - 1;
    u32 index = hash & mask;
    while (true)
    {
        if (map->keys[index] == NULL)
            break;
        else if (strcmp(map->keys[index], key) == 0)
            return map->values + index;

        index = (index + 1) & mask;
    }

    return NULL;
}

static void
dict_put(Dict* map, const char* key, void* value)
{
    assert(key);

    u32 mask = map->cap - 1;
    u32 index = hash64str(key) & mask;
    while (true)
    {
        if (map->keys[index] == NULL)
        {
            map->keys[index] = STRDUP(key);
            map->values[index] = value;
            ++map->num;
            break;
        }
        else if (strcmp(map->keys[index], key) == 0)
        {
            map->values[index] = value;
            break;
        }

        index = (index + 1) & mask;
    }

    if (2*map->num >= map->cap)
        dict_grow(map);
}

static void
dict_putn(Dict* map, const char* key, size_t keylen, void* value)
{
    assert(key && keylen > 0);

    u32 mask = map->cap - 1;
    u32 index = hash64(key, keylen) & mask;
    while (true)
    {
        if (map->keys[index] == NULL)
        {
            map->keys[index] = strdup_len(key, keylen);
            map->values[index] = value;
            ++map->num;
            break;
        }
        else if (strlen(map->keys[index]) == keylen &&
                 strncmp(map->keys[index], key, keylen) == 0)
        {
            map->values[index] = value;
            break;
        }

        index = (index + 1) & mask;
    }

    if (2*map->num >= map->cap)
        dict_grow(map);
}

static void*
dict_get(Dict* map, const char* key)
{
    assert(key);
    u64 hash = hash64str(key);
    void** res = dict_get_hashed(map, key, hash);
    return res ? *res : NULL;
}

static void*
dict_getn(Dict* map, const char* key, size_t keylen)
{
    assert(key && keylen > 0);

    u32 mask = map->cap - 1;
    u32 index = hash64(key, keylen) & mask;
    while (true)
    {
        if (map->keys[index] == NULL)
            break;
        else if (strlen(map->keys[index]) == keylen &&
                 strncmp(map->keys[index], key, keylen) == 0)
            return map->values[index];

        index = (index + 1) & mask;
    }

    return NULL;
}

static DictEntry
dict_get_entry(Dict* map, const char* key)
{
    assert(key);

    u32 mask = map->cap - 1;
    u32 index = hash64str(key) & mask;
    while (true)
    {
        if (map->keys[index] == NULL)
        {
            return (DictEntry){
                .value      = NULL,
                .idx        = index,
                .key        = key,
                .keylen     = 0,
                .present    = false
            };
        }
        else if (strcmp(map->keys[index], key) == 0)
        {
            return (DictEntry){
                .value      = map->values[index],
                .idx        = index,
                .key        = map->keys[index],
                .keylen     = 0,
                .present    = true
            };
        }

        index = (index + 1) & mask;
    }

    INVALID_CODE_PATH();
}

static DictEntry
dict_get_entryn(Dict* map, const char* key, size_t keylen)
{
    assert(key && keylen > 0);

    u32 mask = map->cap - 1;
    u32 index = hash64(key, keylen) & mask;
    while (true)
    {
        if (map->keys[index] == NULL)
        {
            return (DictEntry){
                .value      = NULL,
                .idx        = index,
                .key        = key,
                .keylen     = keylen,
                .present    = false
            };
        }
        else if (strlen(map->keys[index]) == keylen &&
                 strncmp(map->keys[index], key, keylen) == 0)
        {
            return (DictEntry){
                .value      = map->values[index],
                .idx        = index,
                .key        = map->keys[index],
                .keylen     = 0,
                .present    = true
            };
        }

        index = (index + 1) & mask;
    }

    INVALID_CODE_PATH();
}

static void
dict_apply_entry(Dict* map, DictEntry* entry)
{
    assert(entry->key);

    if (entry->present)
    {
        map->values[entry->idx] = entry->value;
    }
    else
    {
        if (2*map->num >= map->cap)
        {
            dict_grow(map);
            if (entry->keylen > 0)
                dict_putn(map, entry->key, entry->keylen, entry->value);
            else
                dict_put(map, entry->key, entry->value);
        }
        else
        {
            ++map->num;
            map->values[entry->idx] = entry->value;
            if (entry->keylen > 0)
            {
                map->keys[entry->idx] = strdup_len(entry->key, entry->keylen);
            }
            else
            {
                map->keys[entry->idx] = STRDUP(entry->key);
            }
        }
    }
}

#ifdef RUN_TESTS

TEST_FUNC(dict)
{
    u64 random_seed = 0x1234567890ABCDEF;
    /* u64 random_seed = time_nano(); */

    Dict dict = dict_create(2);

    for (i32 i = 0; i < 64; ++i)
        dict_put(&dict, "key1", (void*)((u64)i));
    test_assert(dict.num == 1);
    test_assert(dict.cap == 32);

    dict_put(&dict, "key1", (void*)1234ull);
    test_assert((u64)dict_get(&dict, "key1") == 1234ull);

    test_assert(dict_get(&dict, "key2") == NULL);
    dict_put(&dict, "key2", (void*)5678ull);
    test_assert((u64)dict_get(&dict, "key2") == 5678ull);
    test_assert((u64)dict_getn(&dict, "key1", 4) == 1234ull);

    test_assert(dict.num == 2);
    test_assert(dict.cap == 32);

    DictEntry entry = dict_get_entryn(&dict, "key2", 4);
    test_assert(entry.present);
    test_assert(entry.keylen == 0); // Null terminated

    entry.value = (void*)1337ull;
    dict_apply_entry(&dict, &entry);

    test_assert((u64)dict_get(&dict, "key2") == 1337ull);

    // Larger N random key testing

    size_t num = 1000;
    size_t unique_num = 0;
    Array_CString keys = array_cstring_create(num);
    u64 rng = random_seed;
    for (u32 i = 0; i < num; ++i)
    {
        size_t slen = (size_t)rndi32between(&rng, 2, 12);
        char* s = malloc(slen + 1);
        for (u32 j = 0; j < slen; ++j)
        {
            s[j] = (char)rndi32between(&rng, 'a', 'z' + 1);
        }
        s[slen] = '\0';

        DictEntry entry = dict_get_entry(&dict, s);
        if (!entry.present)
        {
            ++unique_num;
        }

        entry.value = (void*)(u64)(i + 1);
        dict_apply_entry(&dict, &entry);

        array_cstring_push(&keys, s);
    }

    size_t num_collisions = num - unique_num;
    test_assert(num_collisions && "tweak num and random_seed so the test contains key collisions");

    for (u32 i = 0; i < keys.len; ++i)
    {
        const char* key = array_cstring_get_safe(&keys, i);

        u64 val = (u64)dict_get(&dict, key);
        if (val != (u64)(i + 1))
        {
            // Value was overwritten with last occurence of key.
            // Find it and retrieve/compare its associated value.

            bool collision_found = false;
            for (u32 j = keys.len - 1; j > i; --j)
            {
                const char* coll_key = array_cstring_get_safe(&keys, j);
                if (strcmp(coll_key, key) == 0)
                {
                    test_assert((u64)dict_get(&dict, coll_key) == (u64)(j + 1));
                    collision_found = true;
                    break;
                }
            }
            test_assert(collision_found);
        }

        free((void*)key);
    }

    test_assert(dict.num == 2 + unique_num);
    test_assert(dict.cap >= 2 * dict.num);

    array_cstring_free(&keys);

    // Original keys still intact after many new keys have been added?

    test_assert((u64)dict_getn(&dict, "key1", 4) == 1234ull);
    test_assert((u64)dict_get(&dict, "key2") == 1337ull);

    dict_free(&dict);

    return true;
}

TEST_FUNC(dict2)
{
    Dict dict = dict_create(4);

    dict_put(&dict, "addToNav", (void*)1234ull);
    dict_put(&dict, "abc", (void*)'c');
    dict_put(&dict, "ab", (void*)'b');
    dict_put(&dict, "a", (void*)'a');

    test_assert((u64)dict_getn(&dict, "a", 1) == (u64)'a');
    test_assert((u64)dict_getn(&dict, "ab", 2) == (u64)'b');
    test_assert((u64)dict_getn(&dict, "abc", 3) == (u64)'c');
    test_assert((u64)dict_getn(&dict, "addToNav", 8) == 1234ull);

    dict_free(&dict);

    return true;
}

#endif

//########################//
// STRING INTERN TABLE
//########################//

static InternTable
interntable_create(u32 init_capacity)
{
    InternTable res = {0};
    res.strings = dict_create(2 * init_capacity);

    return res;
}

static InternedString
interntable_intern_stringn(InternTable* table, const char* str, size_t strlen)
{
    DictEntry entry = dict_get_entryn(&table->strings, str, strlen);
    if (entry.present)
    {
        return entry.value;
    }
    else
    {
        InternedString res = strdup_len(str, strlen);
        entry.value = (void*)res;
        dict_apply_entry(&table->strings, &entry);

        return res;
    }
}

static InternedString
interntable_intern_string(InternTable* table, const char* str)
{
    DictEntry entry = dict_get_entry(&table->strings, str);
    if (entry.present)
    {
        return entry.value;
    }
    else
    {
        InternedString res = STRDUP(str);
        entry.value = (void*)res;
        dict_apply_entry(&table->strings, &entry);

        return res;
    }
}

static u32
interntable_num_interns(InternTable* table)
{
    return table->strings.num;
}

static void
interntable_free(InternTable* table)
{
    dict_free_values(&table->strings);
}

#ifdef RUN_TESTS

TEST_FUNC(string_interning)
{
    u64 random_seed = 0x1234567890ABCDEF;

    InternTable table = interntable_create(4);

    const char* test = "test";
    InternedString res = interntable_intern_string(&table, test);

    test_assert(res != test); // A newly interned string always gets allocated
    test_assert(interntable_intern_string(&table, test) == res);

    for (i32 i = 0; i < 100; ++i)
    {
        const char* test_dup = STRDUP(test);
        InternedString s = interntable_intern_string(&table, test);
        test_assert(s == res);
        free((void*)test_dup);

        char buf[10];
        strcpy(buf, test);
        InternedString sl = interntable_intern_stringn(&table, test, strlen(buf));

        test_assert(sl == res);
    }
    test_assert(interntable_num_interns(&table) == 1);

    {
        size_t num = 1000;
        Array_CString interns = array_cstring_create(num);
        u64 rng = random_seed;
        for (u32 i = 0; i < num; ++i)
        {
            size_t slen = (size_t)rndi32between(&rng, 2, 100);
            char* s = malloc(slen + 1);
            for (u32 j = 0; j < slen; ++j)
            {
                s[j] = (char)rndi32between(&rng, 'a', 'z' + 1);
            }
            s[slen] = '\0';

            if (s[0] & 1)
                array_cstring_push(&interns, interntable_intern_string(&table, s));
            else
                array_cstring_push(&interns, interntable_intern_stringn(&table, s, slen));

            free((void*)s);
        }

        rng = random_seed;
        for (u32 i = 0; i < num; ++i)
        {
            size_t slen = (size_t)rndi32between(&rng, 2, 100);
            char* s = malloc(slen + 1);
            for (u32 j = 0; j < slen; ++j)
            {
                s[j] = (char)rndi32between(&rng, 'a', 'z' + 1);
            }
            s[slen] = '\0';

            InternedString intern = array_cstring_get_safe(&interns, i);
            test_assert(intern == interntable_intern_string(&table, s));

            free((void*)s);
        }

        array_cstring_free(&interns);
    }

    interntable_free(&table);
    return true;
}

#endif
