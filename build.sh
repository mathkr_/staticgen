#!/bin/bash

ERROR='
    -Wall -Wextra -pedantic
    -Wuninitialized -Wsign-conversion -Winit-self
    -Wno-unused-function -Wno-missing-braces
'
FLAGS="-std=c11 -D_GNU_SOURCE"
INCLUDE="-Ithirdparty"
LINK=""
TIMESTAMP=`date +%Y%m%d.%H%M%S`

### VERSION ###
SEMANTIC_VERSION="0.2.0+${TIMESTAMP}"
PROGRAM_NAME="staticgen"

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    TARGET_OS="linux"
    TARGET_EXTENSION=""
elif [[ "$OSTYPE" == "darwin"* ]]; then
    TARGET_OS="osx"
    TARGET_EXTENSION=""
elif [[ "$OSTYPE" == "cygwin" ]] || [[ "$OSTYPE" == "msys" ]] || [[ "$OSTYPE" == "win32" ]]; then
    TARGET_OS="windows"
    TARGET_EXTENSION=".exe"
    FLAGS="${FLAGS} -D_CRT_SECURE_NO_WARNINGS"
else
    echo "Error: Unknown operating system in build script."
    exit 1
fi

if [[ "$1" == "test" ]] || [[ "$2" == "test" ]]; then
    RUN_TESTS=true
elif  [[ "$1" == "profile" ]] || [[ "$2" == "profile" ]]; then
    FLAGS="${FLAGS} -DRUN_PROFILING"
fi

# Don't enable warnings as errors on release builds for the sake of people
# using source distributions
if [[ "$1" != "release" ]]; then
    ERROR="-Werror ${ERROR}"
fi

if [[ "$1" == "release" ]]; then
    OPTIM="-O3 -DNDEBUG"
    if [[ $TARGET_OS != "windows" ]]; then
        LINK="${LINK} -Wl,-strip-all" # NOTE(mk): strip binary
    fi
elif [[ "$1" == "debug" ]]; then
    OPTIM="-O0 -g"
    SEMANTIC_VERSION="${SEMANTIC_VERSION}.debug"
elif [[ "$1" == "sanitize-memory" ]]; then
    OPTIM="-O0 -g -fno-omit-frame-pointer -fno-optimize-sibling-calls -fsanitize=memory"
    SEMANTIC_VERSION="${SEMANTIC_VERSION}.sanitize"
elif [[ "$1" == "sanitize-thread" ]]; then
    OPTIM="-O0 -g -fno-omit-frame-pointer -fno-optimize-sibling-calls -fsanitize=thread"
    SEMANTIC_VERSION="${SEMANTIC_VERSION}.sanitize"
else
    OPTIM="-O0 -g -fsanitize=address -fno-omit-frame-pointer -fno-optimize-sibling-calls -fsanitize=undefined"
    SEMANTIC_VERSION="${SEMANTIC_VERSION}.sanitize"
fi
echo "Compiling with: ${OPTIM}"

TARGET_ARCH="x86-64"
FLAGS="${FLAGS} -DPROGRAM_NAME=\"${PROGRAM_NAME}\" -DSEMANTIC_VERSION=\"${SEMANTIC_VERSION}\""

if [[ "$RUN_TESTS" = true ]]; then
    echo "Compiling tests (-DRUN_TESTS)"
    FLAGS="${FLAGS} -DRUN_TESTS"
fi

EXECUTABLE="${PROGRAM_NAME}_${TARGET_OS}_${TARGET_ARCH}_${SEMANTIC_VERSION}${TARGET_EXTENSION}"

mkdir -p target/

clang ${FLAGS} ${ERROR} ${OPTIM} src/main.c -o target/${PROGRAM_NAME}${TARGET_EXTENSION} ${INCLUDE} ${LINK}
COMPILE_EXIT_STATUS=$?

if [[ "$1" == "release" ]] && [[ "${COMPILE_EXIT_STATUS}" == "0" ]]; then
    cp -v target/${PROGRAM_NAME}${TARGET_EXTENSION} target/${EXECUTABLE}
fi

if [[ "${COMPILE_EXIT_STATUS}" == "0" ]] && [[ "$RUN_TESTS" = true ]]; then
    eval target/${PROGRAM_NAME}${TARGET_EXTENSION}
    TEST_EXIT_STATUS=$?
    NUM_TESTS=`grep -v "#define" src/*.[c,h] | grep "TEST_FUNC(" | wc -l`
    if [[ "${TEST_EXIT_STATUS}" != "${NUM_TESTS}" ]]; then
        echo ""
        echo "Missing RUN_TEST statements in test main."
        echo "${TEST_EXIT_STATUS} tests run but ${NUM_TESTS} found in repository."
    fi
    exit "0"
fi

exit "${COMPILE_EXIT_STATUS}"
