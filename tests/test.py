import subprocess
import difflib
import time

class col:
    OK      = '\033[92m'
    FAIL    = '\033[91m'
    EM      = '\033[1m\033[95m'
    RESET   = '\033[0m'

    @classmethod
    def ok(cls, string):
        return cls.OK + string + cls.RESET

    @classmethod
    def fail(cls, string):
        return cls.FAIL + string + cls.RESET

    @classmethod
    def em(cls, string):
        return cls.EM + string + cls.RESET

EXE = 'target/staticgen.exe'

def run(test_prefix,
        input_dir,
        input_files = ['index.html'],
        expect_failure = False,
        result_files = ['index.html'],
        result_dir = 'out/',
        options = ['--clean-out-dir', '--no-static-files']):

    command = [EXE] + options + list(map(lambda f: input_dir + test_prefix + '_' + f, input_files))
    p = subprocess.run(command, stdout=subprocess.PIPE, universal_newlines=True)

    print('|| TEST:', col.em(test_prefix))
    print('|| CMD:', ' '.join(command))
    print('|| SHOULD FAIL:', expect_failure)
    retc = col.ok(str(p.returncode)) if expect_failure == (p.returncode != 0) else col.fail(str(p.returncode))
    print('|| RETURNCODE:', retc)
    print('|| OUTPUT:')
    for line in p.stdout.splitlines():
        print('||    ' + line)

    test_failed = False
    if expect_failure:
        if p.returncode == 0:
            test_failed = True
    elif p.returncode != 0:
        test_failed = True
    else:
        for result_file in result_files:
            result_path = result_dir + result_file
            control_path = input_dir + test_prefix + '_control_' + result_file
            try:
                with open(result_path, 'r') as fresult, open(control_path, 'r') as fcontrol:
                    result = fresult.read()
                    control = fcontrol.read()
                    match = (result == control)
                    print('|| ' + result_path + ' MATCHES CONTROL:', match)
                    if not match:
                        print('|| DIFF:')
                        for line in difflib.context_diff(control.splitlines(),
                                                         result.splitlines(),
                                                         fromfile=control_path,
                                                         tofile=result_path,
                                                         lineterm=''):
                            print('|| ' + line)
                        test_failed = True
            except IOError as e:
                print('|| ' + (col.fail('IOERROR({0})') + ': {1} - {2}').format(e.errno, e.filename, e.strerror))
                test_failed = True
                break;

    if test_failed:
        print('|| ' + col.fail('TEST FAILED'))
    else:
        print('|| ' + col.ok('TEST OK'))
    print('')

class Tester:
    def __init__(self):
        self.num_run = 0
        self.num_failed = 0
        self.start_time = time.time()

    def summarize(self):
        print('SUMMARY:')
        print('ran {0} tests in {1} seconds'.format(self.num_run, round(time.time() - self.start_time, 3)))
        print(col.ok('passed:'), self.num_run - self.num_failed)
        print(col.fail('failed:'), self.num_failed)

    def run(self,
            test_prefix,
            input_dir,
            input_files = ['index.html'],
            expect_failure = False,
            result_files = ['index.html'],
            result_dir = 'out/',
            options = ['--clean-out-dir', '--no-static-files']):

        command = [EXE] + options + list(map(lambda f: input_dir + test_prefix + '_' + f, input_files))
        p = subprocess.run(command, stdout=subprocess.PIPE, universal_newlines=True)

        print('|| TEST(' + str(self.num_run) + '):', col.em(test_prefix))
        print('|| CMD:', ' '.join(command))
        print('|| SHOULD FAIL:', expect_failure)
        retc = col.ok(str(p.returncode)) if expect_failure == (p.returncode != 0) else col.fail(str(p.returncode))
        print('|| RETURNCODE:', retc)
        print('|| OUTPUT:')
        for line in p.stdout.splitlines():
            print('||    ' + line)

        test_failed = False
        if expect_failure:
            if p.returncode == 0:
                test_failed = True
        elif p.returncode != 0:
            test_failed = True
        else:
            for result_file in result_files:
                result_path = result_dir + result_file
                control_path = input_dir + test_prefix + '_control_' + result_file
                try:
                    with open(result_path, 'r') as fresult, open(control_path, 'r') as fcontrol:
                        result = fresult.read()
                        control = fcontrol.read()
                        match = (result == control)
                        print('|| ' + result_path + ' MATCHES CONTROL:', match)
                        if not match:
                            print('|| DIFF:')
                            for line in difflib.context_diff(control.splitlines(),
                                                             result.splitlines(),
                                                             fromfile=control_path,
                                                             tofile=result_path,
                                                             lineterm=''):
                                print('|| ' + line)
                            test_failed = True
                except IOError as e:
                    print('|| ' + (col.fail('IOERROR({0})') + ': {1} - {2}').format(e.errno, e.filename, e.strerror))
                    test_failed = True
                    break;

        self.num_run += 1
        if test_failed:
            self.num_failed += 1
            print('|| ' + col.fail('TEST FAILED'))
        else:
            print('|| ' + col.ok('TEST OK'))
        print('')
