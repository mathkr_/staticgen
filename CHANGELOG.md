## v0.2.0 (2020/06/22)
Ready to build first test site.

Build includes:
- Templating (templates, instantiations, parameters)
- Referencing top level instantiations
- Multiple input files
- Output folder and static file folder
- Markdown conversion
- Order independent top-level symbols

## v0.1.0 (2020/06/04)
First versioned build
